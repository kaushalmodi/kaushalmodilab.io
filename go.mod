module gitlab.com/kaushalmodi/kaushalmodi.gitlab.io

go 1.16

require (
	github.com/kaushalmodi/hugo-debugprint v0.1.0 // indirect
	gitlab.com/kaushalmodi/hugo-theme-refined v0.14.5 // indirect
)

// replace gitlab.com/kaushalmodi/hugo-theme-refined => ../hugo-theme-refined
// replace github.com/kaushalmodi/hugo-search-fuse-js => ../hugo-search-fuse-js
// replace github.com/kaushalmodi/hugo-atom-feed => ../hugo-atom-feed
// replace github.com/kaushalmodi/hugo-jf2 => ../hugo-jf2
