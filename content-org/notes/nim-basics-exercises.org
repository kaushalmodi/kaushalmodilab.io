#+setupfile: ../scripter-setupfile.org

#+title: Nim Basics Exercises
#+date: 2018-06-13

#+hugo_auto_set_lastmod: t

#+hugo_base_dir: ../../
#+hugo_section: notes
#+hugo_bundle: nim-basics-exercises
#+export_file_name: index

#+hugo_categories: programming
#+hugo_tags: nim examples exercise basics

#+hugo_custom_front_matter: :versions '((nim . "0.18.1, git hash: cd65ef0056"))

#+startup: shrink
#+options: toc:2

#+begin_description
Solved exercises from the [[https://narimiran.github.io/nim-basics/][Nim Basics tutorial]].
#+end_description

* Basic data types
** Basic data types Problems {{{n(basic)}}} and {{{n(basic)}}}
#+begin_src nim
let age = 100
echo "Age of ", age, " years in days = ", age*365
echo "Is age of ", age, " divisible by 3? ", (age mod 3) == 0
#+end_src

#+RESULTS:
: Age of 100 years in days = 36500
: Is age of 100 divisible by 3? false

** Basic data types Problem {{{n(basic)}}}
#+begin_src nim
let htCm = 184.4 # cm
echo htCm, " cm = ", htCm/2.54, " inches"
#+end_src

#+RESULTS:
: 184.4 cm = 72.5984251968504 inches

** Basic data types Problem {{{n(basic)}}}
#+begin_src nim
let pipeDia = 3/8 # inches
echo pipeDia, " inches = ", pipeDia*2.54, " cm"
#+end_src

#+RESULTS:
: 0.375 inches = 0.9525 cm

** Basic data types Problem {{{n(basic)}}}
#+begin_src nim
let
  fName = "foo"
  lName = "bar"
var
  fullName = fName & " " & lName
echo fullName
#+end_src

#+RESULTS:
: foo bar

** Basic data types Problem {{{n(basic)}}}
Alice earns $400 every 15 days. Bob earns $3.14 per hour and works 8
hours a day, 7 days a week. After 30 days, has Alice earned more than
Bob?
#+begin_src nim
let
  alicePayPerDay = 400/15
  bobPayPerDay = 3.14*8
  alice30DayPay = alicePayPerDay*30
  bob30DayPay = bobPayPerDay*30
echo "Alice's 30 day pay = ", alice30DayPay
echo "Bob's 30 day pay = ", bob30DayPay
echo "  Has Alice earned more than Bob after 30 days? ", alice30DayPay > bob30DayPay
#+end_src

#+RESULTS:
: Alice's 30 day pay = 800.0
: Bob's 30 day pay = 753.6
:   Has Alice earned more than Bob after 30 days? true

* Loops
** Loops Problem {{{n(loops)}}}
[[https://en.wikipedia.org/wiki/Collatz_conjecture][Collatz conjecture]] is a popular mathematical problem with simple
rules.
- First pick a number.
- If it is odd, multiply it by three and add one;
- if it is even, divide it by two.
- Repeat this procedure until you arrive at one.

E.g. 5 → odd → 3*5 + 1 = 16 → even → 16 / 2 = 8 → even → 4 → 2 → 1 →
end!

Pick an integer (as a mutable variable) and create a loop which will
print every step of the Collatz conjecture.

#+begin_src nim
var num = 43
while num != 1:
  var isOdd = (num mod 2) == 1
  if isOdd:
    echo num, " is odd"
    num *= 3
    inc num
  else:
    echo num, " is even"
    num = int(num/2)
echo num
#+end_src

#+RESULTS:
#+begin_example
43 is odd
130 is even
65 is odd
196 is even
98 is even
49 is odd
148 is even
74 is even
37 is odd
112 is even
56 is even
28 is even
14 is even
7 is odd
22 is even
11 is odd
34 is even
17 is odd
52 is even
26 is even
13 is odd
40 is even
20 is even
10 is even
5 is odd
16 is even
8 is even
4 is even
2 is even
1
#+end_example

** Loops Problem {{{n(loops)}}}
Create an immutable variable containing your full name. Write a
for-loop which will iterate through that string and print only the
vowels (a, e, i, o, u).

#+begin_src nim
let name = "Xerxes Yancy Wolfeschlegelsteinhausenbergerdorff"
for c in name:
  if c in {'a', 'e', 'i', 'o', 'u'}:
    echo c
#+end_src

#+RESULTS:
#+begin_example
e
e
a
o
e
e
e
e
i
a
u
e
e
e
o
#+end_example

** Loops Problem {{{n(loops)}}}
Fizz buzz is a kids game sometimes used to test basic programming
knowledge. We count numbers from one upwards. If a number is divisible
by 3 replace it with fizz, if it is divisible by 5 replace it with
buzz, and if a number is divisible by 15 (both 3 and 5) replace it
with fizzbuzz. First few rounds would look like this: 1, 2, fizz, 4,
buzz, fizz, 7. ..

Create a program which will print first 30 rounds of Fizz buzz.

#+begin_src nim
for i in 1 .. 30:
  var str = $i
  if (i mod 3) == 0:
    str = "fizz"
    if (i mod 5) == 0:
      str.add("buzz")
  elif (i mod 5) == 0:
    str = "buzz"
  echo str
#+end_src

#+RESULTS:
#+begin_example
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz
16
17
fizz
19
buzz
fizz
22
23
fizz
buzz
26
fizz
28
29
fizzbuzz
#+end_example

/Interestingly, I blogged about [[https://scripter.co/nim-fizz-buzz-test/][FizzBuzz in Nim]] very
recently./ :smile:
** Loops Problem {{{n(loops)}}}
In the previous exercises you have converted inches to centimeters,
and vice versa. Create a conversion table with multiple values. For
example, the table might look like this:

#+begin_example
in	| cm
----------------
1	| 2.54
4	| 10.16
7	| 17.78
10	| 25.4
13	| 33.02
16	| 40.64
19	| 48.26
#+end_example

#+begin_src nim
import strformat
let maxVal = 20
echo fmt"""{"in":<4}| cm"""
echo "----------------"
for valInch in countUp(1, 20, 3):
  var valCm = float(valInch) * 2.54
  echo fmt"""{valInch:<4}| {valCm}"""
#+end_src

#+RESULTS:
: in  | cm
: ----------------
: 1   | 2.54
: 4   | 10.16
: 7   | 17.78
: 10  | 25.4
: 13  | 33.02
: 16  | 40.64
: 19  | 48.26

* Containers
* Procedures
* Interacting with user input
