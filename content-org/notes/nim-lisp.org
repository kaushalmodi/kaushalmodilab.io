#+setupfile: ../scripter-setupfile.org

#+title: Nim List Processing
#+date: 2018-07-03

#+startup: shrink

#+hugo_auto_set_lastmod: t

#+hugo_base_dir: ../../
#+hugo_section: notes
#+hugo_bundle: nim-lisp
#+export_file_name: index

#+hugo_categories: programming
#+hugo_tags: nim sequtils library "list-processing"

#+hugo_custom_front_matter: :versions '((nim . "ab47a870bc"))

#+begin_description
Examples of using various procs and templates for processing "lists"
(Nim sequences and arrays) from the *sequtils* Nim standard library.
#+end_description

The [[https://nim-lang.org/docs/sequtils.html][~sequtils~ library]] provides procs and templates to deal with
"lists", or rather *sequences* and *arrays* in Nim.

#+begin_note
Procs like ~map~ and ~filter~ pair really well with [[file:./nim.org::#anonymous-procedures][Anonymous
Procedures]], [[file:./nim.org::#do-notation][Do Notation]] and the syntactic sugar [[file:./nim.org::#lambda][~=>~]].
#+end_note

#+property: header-args:nim :exports both :results output :import sequtils,sugar
/The *sequtils* and *sugar* (*future* module if using Nim 0.18.0 and
older) modules are assumed to be imported before running all of the
below code snippets./

* concat
Return concatenation of input sequences.
#+begin_src nim
let
  s1 = @[1, 2, 3]
  s2 = @[4, 5]
  s3 = @[6, 7]
  total = concat(s1, s2, s3)
echo total
#+end_src

#+RESULTS:
: @[1, 2, 3, 4, 5, 6, 7]

* count
Return the number of occurrences of an item in the input sequence or
array.
#+begin_src nim
let
  s1 = @[1, 2, 2, 3, 2, 4, 2]
  c1 = s1.count(2)
  s2 = ["a", "c", "b", "z", "a", "b", "a"]
  c2 = s2.count("a")
echo c1
echo c2
#+end_src

#+RESULTS:
: 4
: 3

* Repeat Procs
** cycle
Return a sequence containing repetitions of the input sequence or
array.
#+begin_src nim
let
  s = @[1, 2, 3]
  total = s.cycle(3)
echo total
#+end_src

#+RESULTS:
: @[1, 2, 3, 1, 2, 3, 1, 2, 3]

** repeat
Return a sequence containing repetitions of the specified item.
#+begin_src nim
let
  s1 = repeat(5, 3)
  s2 = repeat("xyz", 5)
echo s1
echo s2
#+end_src

#+RESULTS:
: @[5, 5, 5]
: @["xyz", "xyz", "xyz", "xyz", "xyz"]

* TODO deduplicate
* TODO zip
* TODO distribute
* map
Return a sequence with the results of a proc applied to every item in
the input sequence or array.
#+begin_src nim
let
  a = @[1, 2, 3, 4]
  b1 = map(a, proc(x: int): string = $x)
  b2 = map(a) do (x: int) -> string: $x
  b3 = a.map(x => $x)
echo b1
echo b2
echo b3
#+end_src

#+RESULTS:
: @["1", "2", "3", "4"]
: @["1", "2", "3", "4"]
: @["1", "2", "3", "4"]

* apply
Like ~map~, ~apply~ applies the input proc to every item in the input
sequence or array, but that input sequence/array is modified *in
place*.

So the input sequence/array needs to be declared as *var*.
#+begin_src nim
var a = @["1", "2", "3", "4"]
echo a
apply(a, proc(x: var string) = x &= "a")
echo a
apply(a) do (x: var string): x &= "b"
echo a
a.apply(x => x & "c") # Here, x is not mutable, but the end result is the same.
echo a
#+end_src

#+RESULTS:
: @["1", "2", "3", "4"]
: @["1a", "2a", "3a", "4a"]
: @["1ab", "2ab", "3ab", "4ab"]
: @["1abc", "2abc", "3abc", "4abc"]

* TODO keepIf
* TODO delete
* TODO insert
* TODO all
* TODO any
* TODO filter
#+begin_src nim
let
  colors = @["red", "yellow", "black", "green", "blue", "brown"]
  f1 = filter(colors, proc(x: string): bool = x.len < 6)
  f2 = filter(colors) do (x: string) -> bool: x.len > 5
  f3 = colors.filter(x => x[0] == 'b')
echo f1
echo f2
echo f3
#+end_src

#+RESULTS:
: @["red", "black", "green", "blue", "brown"]
: @["yellow"]
: @["black", "blue", "brown"]

* TODO mapLiterals
* TODO filterIt
* TODO keepItIf
* TODO allIt
* TODO anyIt
* TODO toSeq
* TODO foldl
* TODO foldr
* TODO mapIt
* TODO applyIt
* TODO newSeqWith

* Reference
- [[https://nim-lang.org/docs/sequtils.html][~sequtils~ Documentation]]
- [[./nim.org][More Nim notes]]
