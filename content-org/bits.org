#+hugo_base_dir: ../
#+hugo_section: bits

#+setupfile: ./scripter-setupfile.org

#+bibliography: cite/bib/refs.bib
#+cite_export: csl cite/csl/apa.csl

#+startup: inlineimages

* DONE PlantUML version                                    :plantuml:version:
CLOSED: [2018-04-03 Tue 17:53]
:PROPERTIES:
:EXPORT_FILE_NAME: plantuml-version
:EXPORT_HUGO_CUSTOM_FRONT_MATTER: :versions '((plantuml . "1.2022.1"))
:END:
#+begin_description
Finding PlantUML version.
#+end_description

{{{update(<2022-02-22 Tue>,Add another method to get the PlantUML
version\, using ~version~ in the UML code.)}}}
** Using PlantUML code
#+name: code__plantuml_version
#+caption: Print PlantUML version
#+begin_src plantuml :file images/plantuml-version.svg :exports both
@startuml
version
@enduml
#+end_src

#+name: fig__plantuml_version
#+caption: PlantUML Version Output
#+RESULTS: code__plantuml_version
[[file:images/plantuml-version.svg]]
** From the command line
#+begin_src shell
java -jar /path/to/plantuml.jar -version
#+end_src

That will give an output like:
#+begin_example
PlantUML version 1.2022.1 (Tue Feb 01 13:19:58 EST 2022)
(GPL source distribution)
Java Runtime: OpenJDK Runtime Environment
JVM: OpenJDK 64-Bit Server VM
Default Encoding: UTF-8
Language: en
Country: US

PLANTUML_LIMIT_SIZE: 4096

Dot version: dot - graphviz version 2.30.1 (20180420.1509)
Installation seems OK. File generation OK
#+end_example
** Reference
[[https://plantuml.com/faq][PlantUML FAQ]]
