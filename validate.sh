#!/usr/bin/env bash
# Time-stamp: <2022-06-01 12:14:34 kmodi>

rm -rf public
hugo

# https://github.com/validator/validator
# https://github.com/svenkreiss/html5validator <- pip install --user html5validator
# Ignores:
# 1. google4a938eaf9bbacbcd : Google verification file - https://www.google.com/webmasters/verification
# 2. 'notes/plantuml.*Attribute.*title.*not allowed' : Error due to a PlantUML generated SVG with a hyperlink - cannot fix that.
html5validator --ignore 'google4a938eaf9bbacbcd' \
               --ignore-re 'notes/plantuml.*Attribute.*title.*not allowed' \
               --root public/ > validate.log

# java -jar vnu.jar --skip-non-html --errors-only public/

echo "See validate.log"
