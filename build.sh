#!/usr/bin/env bash

# Wrapper script for Netlify/CI hugo builds

set -euo pipefail # http://redsymbol.net/articles/unofficial-bash-strict-mode
IFS=$' \n\t'

# Initialize variables
debug=0

BRANCH=${BRANCH:-'main'} # This variable is set by Netlify.

audit_dir="./audit/"

hugo_cmd=("hugo")

while [ $# -gt 0 ]
do
    case "$1" in
        "-D"|"--debug" ) debug=1;;
        * ) hugo_cmd+=("$1");;
    esac
    shift # expose next argument
done

validate () {
    # Ignores:
    # 1. google4a938eaf9bbacbcd : Google verification file - https://www.google.com/webmasters/verification
    # 2. 'notes/plantuml.*Attribute.*title.*not allowed' : Error due to a PlantUML generated SVG with a hyperlink - cannot fix that.
    if ! html5validator --ignore 'google4a938eaf9bbacbcd' \
         --ignore-re 'notes/plantuml.*Attribute.*title.*not allowed' \
         --root "${audit_dir}"
    then
        echo "FAIL: HTML5 Validation."
        exit 1
    fi
    echo "PASS: HTML5 Validation"
}

site_audit () {
    # https://discourse.gohugo.io/t/audit-your-published-site-for-problems/35184
    if ! HUGO_MINIFY_TDEWOLFF_HTML_KEEPCOMMENTS=true \
         HUGO_ENABLEMISSINGTRANSLATIONPLACEHOLDERS=true \
         hugo --destination "${audit_dir}"
    then
        echo "FAIL: hugo run failed."
        exit 1
    fi

    validate

    if grep -inorE "<\!-- raw HTML omitted -->|ZgotmplZ|\[i18n\]|\(<nil>\)|(&lt;nil&gt;)|hahahugo" "${audit_dir}"
    then
        echo "FAIL: Site audit. Review the contents of ${audit_dir}."
        exit 1
    fi

    echo "PASS: Site audit"
    rm -rf "${audit_dir}"

    echo ""
}

gen_site () {
    # Primary deployment URLs for Netlify:
    # https://docs.netlify.com/configure-builds/environment-variables/#deploy-urls-and-metadata
    # If your Netlify site name is "foo" (i.e. you have
    # https://app.netlify.com/sites/foo), and your git repo branch is
    # "bar", Netlify auto-deploys your branch at
    # "https://bar--foo.netlify.com" (regardless of your setting a
    # custom domain for your site). The DEPLOY_PRIME_URL env var would
    # be auto-set to "https://bar--foo.netlify.com" in this case.

    # Ignore DEPLOY_PRIME_URL for main and master branch commits.
    if [[ ("${BRANCH}" != "master") && ("${BRANCH}" != "main")]]
    then
        if [[ -n ${DEPLOY_PRIME_URL+x} ]]
        then
            echo -e "DEPLOY_PRIME_URL = ${DEPLOY_PRIME_URL}\n"
            hugo_cmd+=("--baseURL" "${DEPLOY_PRIME_URL}/") # Add the trailing forward slash to Hugo BaseURL
        fi
    fi

    echo "Running: '${hugo_cmd[*]}' .."
    eval "${hugo_cmd[*]}"
    echo ""
}

post_process_site () {
    echo 'Post Processing: Adding rel="noopener" for target="_blank" links ..'
    # Open new tabs using `rel="noopener"` to improve performance and prevent
    # security vulnerabilities.
    # https://developers.google.com/web/tools/lighthouse/audits/noopener
    # - Find all .html files in public/.
    # - In those files, then filter down to only the lines that have an <a..>
    #   tag with *only* href and target properties.  This is to prevent false
    #   search/replace.
    # - Then replace «target="_blank"» with «target="_blank" rel="noopener"»
    #   only in those lines.
    find ./public -name "*.html" -exec sed -r -i '/<a href=[^ ]+ target="?_blank"?>/ s/target=("?)_blank\1/\0 rel=\1noopener\1/g' {} +
    echo ""
}

main () {
    if [[ ${debug} -eq 1 ]]
    then
        echo "Debug mode"
    fi

    echo "[1/3] Auditing generated site .."
    site_audit

    echo "[2/3] Generating site .."
    gen_site

    echo "[3/3] Post-processing the site .."
    post_process_site

    echo "Done!"
}

main
