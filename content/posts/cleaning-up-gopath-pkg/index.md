+++
title = "Cleaning up ${GOPATH}/pkg/"
author = ["Kaushal Modi"]
description = """
  Use `go clean -modcache` to clean up all the old auto-downloaded Go
  modules in `${GOPATH}/pkg/`. That's all you need to know. Rest of the
  post gives just the history of how I got there.
  """
date = 2022-06-25T10:48:00-04:00
tags = ["golang", "100DaysToOffload"]
categories = ["programming"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  go = "1.18.3"
[syndication]
  mastodon = 108538715949214320
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [The `$GOPATH` directory](#the-gopath-directory)
- [The disk space issue](#the-disk-space-issue)
- [Write-protected `${GOPATH}/pkg/`](#write-protected-gopath-pkg)
- [Cleaning up "modcache"](#cleaning-up-modcache)
- [Wrapping up](#wrapping-up)
- [References](#references)

</div>
<!--endtoc-->


As I mentioned in the post description above, the solution to clean up
the auto-populated `${GOPATH}/pkg/` is to run `go clean
-modcache`. While the solution is simple, it wasn't easy for me to
discover, and so I am writing this short piece to make it a bit more
discoverable for others like me.


## The `$GOPATH` directory {#the-gopath-directory}

The `GOPATH` environment variable lists places to look for Go code.

If you haven't set this variable and if the `${HOME}/go/` directory
isn't used to contain the Go distribution, `$GOPATH` defaults to that
path.

When you run `go get` to install any Go package,

-   `${GOPATH}/pkg/` gets populated with all the Go module dependencies,
-   .. and all package's executables if any get installed to `${GOPATH}/bin/`.

<div class="note">

I think that if the user has set the `$GOMODCACHE` environment
variable, that directory would get populated with Go module downloads
instead. But for my use, I am sticking with only the `$GOPATH` for
simplicity.

</div>

To confirm the value of `GOPATH` used by Go, see {{< highlight shell "hl_inline=true" >}}go env | rg GOPATH{{< /highlight >}}.


## The disk space issue {#the-disk-space-issue}

I have been happily installing and building Go apps like Hugo using
`go get` or `go build`, and all these installations would end up in
the `$GOPATH`. But over time, I noticed that the disk space used by
`${GOPATH}/pkg/` just kept on creeping up.

Today I happened to notice that this directory was taking up roughly
**4 GB** of my disk space! I started analyzing why it was taking up so
much space using my favorite tool for this purpose -- [ncdu](https://dev.yorhel.nl/ncdu).

Here's a snapshot showing disk usage by one of the sub-directories
under `${GOPATH}/pkg/`, which shows the problem --- Over time, I had
accumulated multiple versions of multiple packages!

<a id="figure--go-pkg-disk-usage"></a>

{{< figure src="go-pkg-disk-usage.png" caption="<span class=\"figure-number\">Figure 1: </span>Snapshot of _ncdu_ showing disk usage for a `${GOPATH}/pkg/` directory" >}}


## Write-protected `${GOPATH}/pkg/` {#write-protected-gopath-pkg}

I identified the problem. And I knew that I just needed to delete all
the old packages.

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;But Go wouldn't allow deleting those `pkg/` directories!<br />

</div>

I had been searching a solution to this on and off, but didn't have
much success, mainly attributed to the short and generic name of the
"Go" language, and the fact that I didn't know what the
`${GOPATH}/pkg/` directory was called.


## Cleaning up "modcache" {#cleaning-up-modcache}

Today, I finally had success when I searched for this magic phrase:
_golang "cannot remove" "pkg/mod"_ .. and [Golang Issues # 27161](https://github.com/golang/go/issues/27161#issuecomment-415213240) was
the first search result!

The solution was so simple, but so difficult to look for ..

<a id="code-snippet--go-clean-modcache"></a>
```shell
go clean -modcache
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--go-clean-modcache">Code Snippet 1</a>:</span>
  Command to delete contents in <code>${GOPATH}/pkg/</code> or the Go <i>modcache</i>
</div>

From that issue, I also learned that the `${GOPATH}/pkg/` directory is
the default "modcache" or the cache directory for holding all the
installed Go modules.


## Wrapping up {#wrapping-up}

Knowing that the stuff I was trying to delete is called _modcache_,
this of course works ..

<a id="code-snippet--go-help-clean"></a>
```shell
go help clean | rg modcache -A 2
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--go-help-clean">Code Snippet 2</a>:</span>
  Getting help with cleaning up the <i>modcache</i> from <code>go help</code>
</div>

The issue referenced above gets resolved when a [`-modcacherw` switch
gets added](https://github.com/golang/go/issues/27161#issuecomment-625899357) to the `go build` command. I see that switch when I run
`go help build`. _But that switch is not available for `go get`?
.. because I don't see it when I run `go help get`._

I don't understand why Go decided to make this so complicated by
taking away the _write_ access from the user who installed the Go
package!

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;At least `go clean -modcache` accomplishes what I want .. _sigh_<br />

</div>


## References {#references}

-   [`go` Documentation -- `GOPATH` environment variable](https://pkg.go.dev/cmd/go#hdr-GOPATH_environment_variable)
-   [`go` Documentation -- `go help clean`](https://pkg.go.dev/cmd/go#hdr-Remove_object_files_and_cached_files)
-   [Go Wiki -- Setting `GOPATH`](https://github.com/golang/go/wiki/SettingGOPATH)
-   [Golang Issue # 27161](https://github.com/golang/go/issues/27161)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
