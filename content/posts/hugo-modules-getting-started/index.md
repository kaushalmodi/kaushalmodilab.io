+++
title = "Hugo Modules: Getting Started"
author = ["Kaushal Modi"]
description = "What are Hugo Modules and should you convert your Hugo site to be one?"
date = 2022-02-24T02:21:00-05:00
series = ["Hugo Modules"]
tags = ["module", "100DaysToOffload"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  go = "1.17.7"
  hugo = "0.92.0"
[syndication]
  mastodon = 107851791774935886
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Why use Hugo Modules?](#why-use-hugo-modules)
- [How to use Hugo Modules?](#how-to-use-hugo-modules)
    - [<span class="section-num">1</span> Install a recent version of `go`](#install-a-recent-version-of-go)
    - [<span class="section-num">2</span> Convert your Hugo site to a _Hugo Module_](#convert-to-hugo-module)
- [Example: `hugo mod init`](#example-hugo-mod-init)
- [Closing](#closing)
- [References](#references)

</div>
<!--endtoc-->

<div class="org-center">

The short answer to the second question is _yes_!

</div>

The **Hugo Modules** feature
{{% sidenote %}}
This feature was added to Hugo back in [July 2019](https://github.com/gohugoio/hugo/releases/tag/v0.56.0) and it has become
more stable and feature-rich with time.
{{% /sidenote %}} allows collecting different pieces of your Hugo site source from
different repositories. Here each "piece" is a _module_. Those modules
do not have to be just themes --- they can even be parts of your site
content.

From the [documentation](https://gohugo.io/hugo-modules/):

> A module can be your main project or a smaller module providing one or
> more of the 7 component types defined in Hugo: **static**, **content**,
> **layouts**, **data**, **assets**, **i18n**, and **archetypes**. You can combine
> modules in any combination you like, and even mount directories from
> non-Hugo projects, forming a big, virtual union file system.

I find the Hugo Modules feature to be
<mark>kind of like [Git Submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules)</mark> , or like a symbolic link aggregation scheme like [GNU Stow](https://www.gnu.org/software/stow/) but version
controlled.

<div class="note">

You need to install `go` in order to use Hugo Modules.

</div>

So you might wonder why you would want to go through that trouble ..


## Why use Hugo Modules? {#why-use-hugo-modules}

Separation of reusable components
: This benefit is same as that of
    using _git submodules_ for managing themes. You might have separate
    repos for [adding ATOM feed](https://github.com/kaushalmodi/hugo-atom-feed), another for adding [search](https://github.com/kaushalmodi/hugo-search-fuse-js) which you
    would want to reuse on multiple sites, and you integrate those in
    your main site repo as submodules. Hugo modules allows you to do the
    same, and then more (recursive module updates, printing dependency
    graphs, etc.).

Mounts
: Once a site repo is a Hugo module, it can start using the
    [**mounts** feature](https://gohugo.io/hugo-modules/configuration/#module-config-mounts).

    Let's call the main Hugo site repository the <span class="org-radio" id="org-radio--self-module">self module</span>.
    _Mounts_ are analogous to creating symbolic links
{{% sidenote %}}
    If you know what GNU Stow does, it's kind of like that.
    {{% /sidenote %}} from any file or directory in the [self module](#org-radio--self-module) to **any** file or
    directory in one or more of the imported _modules_ (including the
    [self module](#org-radio--self-module)).

    As an example, you might have a `README.md` in the root of your git
    repository. Using _mounts_, you can tell Hugo to use that same file
    as if it were `content/_index.md`
{{% sidenote %}}
    You can find the full example in [github.com/bep/portable-hugo-links](https://github.com/bep/portable-hugo-links/blob/aa096af0d46e5fc8126b0afaedac557470128cf1/config.toml#L12-L14).
    {{% /sidenote %}} , and now the same `README.md` serves as the content page for your
    site's home page! :exploding_head:


## How to use Hugo Modules? {#how-to-use-hugo-modules}


### <span class="section-num">1</span> Install a recent version of `go` {#install-a-recent-version-of-go}

The _Hugo Modules_ feature is powered by [Go Modules](https://github.com/golang/go/wiki/Modules). So even if you
don't develop anything in `go`, you need to have a recent version of
`go` installed (version 1.12
{{% sidenote %}}
If you are using Netlify to deploy your website, go to the _Deploy
Settings_ ➔ _Environment_, and set the `GO_VERSION` environment
variable to 1.12 or newer.
{{% /sidenote %}} or newer).

<div class="note">

As of <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-02-24 Thu&gt;</span></span>, the latest stable version of `go` is
_1.17.7_. Download it from <https://go.dev/dl/>
{{% sidenote %}}
Also see [Installing go toolchain]({{< relref "installing-go-toolchain" >}}).
{{% /sidenote %}} .

</div>


### <span class="section-num">2</span> Convert your Hugo site to a _Hugo Module_ {#convert-to-hugo-module}

For your Hugo site to be able to use themes or theme components (like
ATOM feed support, etc.), or even to use the _Mounts_ feature,
<mark>your Hugo site itself needs to become a Hugo Module</mark> .

Here's how you initialize your site repo to be a Hugo Module. The
following command assumes that you Hugo site repo lives at
`https://github.com/USER/PROJECT`.

<a id="code-snippet--hugo-mod-init-example"></a>
```shell
# cd /path/to/your/site/repo/
hugo mod init github.com/USER/PROJECT
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--hugo-mod-init-example">Code Snippet 1</a>:</span>
  Initializing your site repo to become a Hugo Module
</div>

Here, `github.com/USER/PROJECT` is your Go/Hugo <span class="org-radio" id="org-radio--module-name">module name</span>.
This name does not need to be your git remote's URL. It could be any
globally unique string you can think of.

<div class="note">

Note that "https​://" should not be included in the _[module name](#org-radio--module-name)_.

</div>

If all went well, a `go.mod` file will be created that will look like
this:

<a id="code-snippet--example-go-mod"></a>
```text
module github.com/USER/PROJECT

go 1.17
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--example-go-mod">Code Snippet 2</a>:</span>
  Example <code>go.mod</code>
</div>

Right now, the `go.mod` only contains your site's [module name](#org-radio--module-name) and the
`go` version used to create it. Once other modules are added as
dependencies, they will be added in this same file
{{% sidenote %}}
Coming back to the _git submodules_ analogy, `go.mod` is similar to
the `.gitmodules` file.
{{% /sidenote %}} .

Also, once other module dependencies are added, a `go.sum` file would
also be created containing the checksums of all your module
dependencies.

If you are managing your Hugo themes via _git submodules_, don't
worry! Converting your site to a Hugo Module will not break anything.


## Example: `hugo mod init` {#example-hugo-mod-init}

I have a <abbr aria-label="Minimum Working Example" tabindex=0>MWE</abbr> repository called
[`hugo-mwe`](https://gitlab.com/hugo-mwe/hugo-mwe) to conduct small Hugo-related experiments, or play with a
new feature, or attempt to reproduce a bug that I'd want to
report. You can clone it and quickly try out the `hugo mod init`
command.

<a id="code-snippet--hugo-mod-init-hugo-mwe"></a>
```shell
git clone https://gitlab.com/hugo-mwe/hugo-mwe
cd hugo-mwe
hugo mod init gitlab.com/$USER/hugo-mwe
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--hugo-mod-init-hugo-mwe">Code Snippet 3</a>:</span>
  Full example of initializing a <i>Hugo Module</i>
</div>


## Closing {#closing}

In this post, I have given a brief introduction to what _Hugo Modules_
are, and how you can convert your Hugo site into one.  In the [next
post]({{< relref "hugo-modules-importing-a-theme" >}}), I go through how to import themes as _modules_.


## References {#references}

-   [Hugo Modules documentation](https://gohugo.io/hugo-modules/)
-   [Hugo Forum -- Hugo Modules for "dummies"](https://discourse.gohugo.io/t/hugo-modules-for-dummies/20758)
-   [Hugo Modules: Everything you need to know!](https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
