+++
title = "Hugo Modules: Importing a Theme"
author = ["Kaushal Modi"]
description = "A brief guide on how to install Hugo themes using Hugo Modules."
date = 2022-05-26T16:26:00-04:00
series = ["Hugo Modules"]
tags = ["module", "100DaysToOffload", "theme", "component"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  hugo = "v0.99.1"
[syndication]
  mastodon = 108370239242540227
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [<span class="section-num">1</span> Clean up the old way of setting a theme](#clean-up-the-old-way-of-setting-a-theme)
- [<span class="section-num">2</span> Import the "theme" module](#import-the-theme-module)
    - [Quick Example](#module-imports-example)
- [<span class="section-num">3</span> `hugo mod tidy`](#hugo-mod-tidy)
- [<span class="section-num">4</span> Updating the theme](#updating-the-theme)
- [Dependency Graph](#dependency-graph)
- [Building your Hugo site on a server](#building-your-hugo-site-on-a-server)
- [In a nutshell](#in-a-nutshell)
- [References](#references)

</div>
<!--endtoc-->


Hello! You are reading this post because you are probably interested
in the Hugo Modules feature and are considering to import a Hugo
Module as a theme.

**Step 0** for that approach is to make your site repo a Hugo Module. If
your site already is, then it would have a `go.mod` file in the repo
root. If you don't have the `go.mod` file, check out the previous post
[Hugo Modules: Getting Started]({{< relref "hugo-modules-getting-started" >}}) first :bangbang:

<div class="note">

If you don't have a `go.mod` file for your site repo, and you still
decide to continue with the next steps, don't complain if you see
errors like _module "foo" not found; either add it as a Hugo Module or
store it in "&lt;your site repo&gt;/themes".: module does not exist_. ---
[speaking from experience](https://discourse.gohugo.io/t/hugo-mod-init-fails-to-create-go-mod-if-hugo-detects-an-error-prematurely-in-site-config-toml/36687) :wink:.

</div>

With that out of the way, here are the next steps ..


## <span class="section-num">1</span> Clean up the old way of setting a theme {#clean-up-the-old-way-of-setting-a-theme}

If you are upgrading your Hugo site to switch from the legacy method
of using themes (i.e using the `theme` variable in the site config
{{% sidenote %}}
In my posts, you may have seen me use the _Site Config_ term or
`config.toml` -- They mean the same thing.
{{% /sidenote %}} ), you need to clean that up.

1.  Remove the `theme` variable from your site config.
2.  Remove the `themes` directory, or move it out of your Hugo site
    repo.
    -   If you were cloning a theme developed by someone else in there,
        you can just remove this directory.
    -   If you are maintaining your own theme in that directory, move it
        out of your site repo and {{< titleref "Hugo Modules: Getting Started#convert-to-hugo-module" " convert it to a Hugo Module " >}}.


## <span class="section-num">2</span> Import the "theme" module {#import-the-theme-module}

The _theme_ is quoted in this title, because the concept of a Hugo
"theme" is a bit old now (<span class="timestamp-wrapper"><span class="timestamp">&lt;2022-05-26 Thu&gt;</span></span>) and that has been
superseded with the concept of "modules".

The main difference between a theme and a generic _Hugo Module_ is
that the former will allow you to build your site entirely, while the
latter might implement only some modular features like enabling the
ATOM feed, or adding a search to your website.

<div class="note">

I am mentioning this again for convenience, from [the previous post in
this series]({{< relref "hugo-modules-getting-started" >}}):

_A module can be your main project or a smaller module providing one
or more of the 7 component types defined in Hugo: **static**, **content**,
**layouts**, **data**, **assets**, **i18n**, and **archetypes**. You can combine
modules in any combination you like, and even mount directories from
non-Hugo projects, forming a big, virtual union file system._

</div>

A theme will need to have the "layout" component. Additionally, it
might have the "assets", "static", and other components too.

Importing a module as a theme will typically look like this in your
site config:

```toml
[module]
  [[module.imports]]
    path = "URL of the theme's git remote *without* the 'https://' part"
```

The **path** here would be something like
`github.com/USER/THEME-REPO-NAME` or
`gitlab.com/USER/THEME-REPO-NAME`.

Note
: It's possible to take <span class="underline">any</span> Hugo theme git repo and import
    that as a Hugo Module even if that repo isn't actually one
    i.e. doesn't have a `go.mod`. But it's recommended that the theme be
    a proper Hugo Module so that you have better dependency tracking
    between your site and the theme.


### Quick Example {#module-imports-example}

Follow these steps if you want to try out how this Hugo Module based
theme importing

<div class="note">

As a reminder, you {{< titleref "Hugo Modules: Getting Started#install-a-recent-version-of-go" " need to have Go installed " >}}
on your system.

</div>

1.  Create a temporary directory somewhere and `cd` to it.
2.  Initialize your site as a Hugo Module: `hugo mod init foo` (yeah,
    type that out literally --- it will work)
3.  Create a `config.toml` file with the below content. It imports the
    [`hugo-mwe-theme`](https://gitlab.com/kaushalmodi/hugo-mwe-theme)
{{% sidenote %}}
    `hugo-mwe-theme` is a minimal Hugo theme that I use to quickly try
    out some new feature in Hugo or to create a _minimal working
    example_ to reproduce a bug.
    {{% /sidenote %}} theme.
    <a id="code-snippet--theme-module-import-example"></a>
    ```toml
    [module]
      [[module.imports]]
        path = "gitlab.com/kaushalmodi/hugo-mwe-theme"
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--theme-module-import-example">Code Snippet 1</a>:</span>
      Example of importing a Hugo module as a theme in <code>config.toml</code>
    </div>
4.  Create `content/hello.md`. This step is optional and is only so
    that your test site as some content.
    ```md
    +++
    title = "Hello"
    +++
    Hey!
    ```

That's it! Now run the Hugo server (`hugo server`) and look at your
site running on localhost .. while thinking in disbelief.. _just how
easy all of this was!_ :smiley:.

-   Did you need to manually clone any theme? **No**
-   Would you need to deal with the `.gitmodules` file? **No**


## <span class="section-num">3</span> `hugo mod tidy` {#hugo-mod-tidy}

Finally, run [`hugo mod tidy`](https://gohugo.io/commands/hugo_mod_tidy/) to clean up the `go.mod` and
update/generate the `go.sum` file. These files will track the module
dependencies for your site.

-   The `go.mod` contains the direct module dependencies for your site.
-   The `go.sum` contains the versions and hashes of all the direct **and
    indirect** dependencies
{{% sidenote %}}
    Just as you added a theme as a Hugo Module to your site, it's
    possible that that theme is depending on other Hugo Modules (like
    the ones I mentioned earlier: ATOM feeds, search, etc.).
    {{% /sidenote %}} for your site.

<div class="note">

You would need to commit the `go.mod` and `go.sum`[^fn:1] files if you
build and deploy your website on a remote server or a [CI/CD](https://en.wikipedia.org/wiki/CI/CD) system.

</div>

If you ran the [Quick Example](#module-imports-example), you will see this (as of
<span class="timestamp-wrapper"><span class="timestamp">&lt;2022-05-26 Thu&gt;</span></span>) in your `go.mod`:

```text
module foo

go 1.18

require gitlab.com/kaushalmodi/hugo-mwe-theme v0.1.1 // indirect
```

.. and this in your `go.sum`:

```text
gitlab.com/kaushalmodi/hugo-mwe-theme v0.1.1 h1:FyTp43CJRpBfoHyWnwQFx//cipgP6xQ9/uucj+qjj1U=
gitlab.com/kaushalmodi/hugo-mwe-theme v0.1.1/go.mod h1:vvq0r/SfKMbiPbyqL4YottSOkpCkBSosqGRm82aDNrU=
```


## <span class="section-num">4</span> Updating the theme {#updating-the-theme}

Here are some common ways to update the theme module going forward:

| Command                 | Description                                                          |
|-------------------------|----------------------------------------------------------------------|
| `hugo mod get -u`       | Update only the modules that your site directly depends on.          |
| `hugo mod get -u ./...` | Update the modules that your site depends on in a recursive fashion. |

Additionally, you might or might not need these, but I am documenting
them here for completeness:

| Command                                | Description                                                                                                                     |
|----------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `hugo mod get -u <module path>`        | Update only the specified module[^fn:2] to the latest version. Example: `hugo mod get -u gitlab.com/kaushalmodi/hugo-mwe-theme` |
| `hugo mod get <module path>@<git ref>` | Update a module to the specified git tag or commit. Example: `hugo mod get gitlab.com/kaushalmodi/hugo-mwe-theme@v0.1.1`        |


## Dependency Graph {#dependency-graph}

If you have a theme added as a Hugo Module, which depends on other
Hugo Modules, it's often helpful to know the dependency graph. You can
do that by running:

```shell
hugo mod graph
```

For the above [Quick Example](#module-imports-example), you will see just this one line because
that theme does not depend on other modules:

```text
foo gitlab.com/kaushalmodi/hugo-mwe-theme@v0.1.1
```


## Building your Hugo site on a server {#building-your-hugo-site-on-a-server}

Alright, so you are able to build your site locally after switching to
using themes as modules, great!

Now, if you build and deploy your site on a remote server like Netlify
or Vercel, you need to ensure that you have a recent version of Go
installed in their environment too.

I deploy this website using Netlify, and so I know how to do that
there --- Set the `GO_VERSION` environment variable to a recent
version like **1.18** in the [Environment variables](https://docs.netlify.com/configure-builds/environment-variables/) section in Netlify
_Build &amp; deploy_ settings.


## In a nutshell {#in-a-nutshell}

1.  **First** convert your Hugo site to a Hugo module.
2.  Then replace the `theme` in your site config with a module import.

Enjoy! :champagne:


## References {#references}

-   [Hugo Modules documentation](https://gohugo.io/hugo-modules/use-modules/)
-   [Hugo Modules: Everything you need to know!](https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/)

[^fn:1]: It is recommended to _git commit_ the `go.sum` along with your
    site's `go.mod`. From the [Go Modules documention](https://github.com/golang/go/wiki/Modules#releasing-modules-all-versions): _Ensure your
    `go.sum` file is committed along with your `go.mod` file. See [FAQ
    below](https://github.com/golang/go/wiki/Modules#should-i-commit-my-gosum-file-as-well-as-my-gomod-file) for more details and rationale._
[^fn:2]: Trust me.. once you get a hang of the Hugo Module system, your
    site will have more than one!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
