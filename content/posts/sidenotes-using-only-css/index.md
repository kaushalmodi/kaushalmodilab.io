+++
title = "Sidenotes using only CSS"
author = ["Kaushal Modi"]
description = """
  Through the Mastodon-verse, one day I somehow landed on the amazing
  website <https://takeonrules.com/> by Jeremy Friesen and I got motivated
  to learn about sidenotes once again.

  This post describes my fresh attempt at getting the sidenotes to work.
  """
date = 2022-02-03T01:27:00-05:00
series = ["Sidenotes"]
tags = ["sidenotes", "css", "100DaysToOffload"]
categories = ["web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  mastodon = 107732702830961962
  twitter = ""
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [HTML elements for sidenotes](#html-elements-for-sidenotes)
- [Basic CSS](#basic-css)
- [Sidenote CSS](#sidenote-css)
    - [CSS for mobile view or narrow viewport](#css-for-mobile-view-or-narrow-viewport)
- [Sidenote Counter CSS](#sidenote-counter-css)
- [Things to improve](#things-to-improve)
- [Conclusion](#conclusion)
- [More References](#more-references)

</div>
<!--endtoc-->

I had dabbled into sidenotes CSS about three years back when I came
across [this blog post](https://fransdejonge.com/wp-content/uploads/2010/01/sidenotes.html) by Frans de Jonge, but that attempt just stayed
in a git branch and never panned out.  This time, I was more
determined
{{% sidenote %}}
You need that kind of determination when you are working with CSS :smile:
{{% /sidenote %}} and I got to work by looking online for resources on _"sidenotes using
CSS"_, and my first stop was Kenneth Friedman's blog post on [Marginal
Notes](https://kennethfriedman.org/thoughts/2019/marginal-notes/).

I liked how the author introduced the basic steps for implementing
marginal notes---that you need to (i) restrict the width of the body
text so that you can fix the sidenotes, (ii) put the notes in a
special tag like `<aside>`, and (iii)<span class="org-target" id="org-target--kenneth-point-three"></span> add CSS
to push the `<aside>` tag content outside of the body text.

I really liked it because point (i) was already implemented on this
website.

I implemented the later two points using the examples on that blog
post, examples from few [more](#org-target--more-sidenotes-references) blog posts and then a bit more of
fiddling with CSS on my own.


## HTML elements for sidenotes {#html-elements-for-sidenotes}

Regarding point (ii) above, I later realized that using the `<aside>`
tag was a wrong choice for sidenotes. Hugo (or rather its Markdown
parser _Goldmark_) auto-wraps these tags in `<p>` and so we get forced
paragraph breaks around them.

That problem did not show up immediately. But once I started to get
the CSS sidenote counters
{{% sidenote %}}
_sidenote counters_ allow you to easily find the sidenotes
corresponding to the those counter numbers referenced in the body
text. This is really helpful when you have a lot of sidenotes bunched
together.
{{% /sidenote %}} to work, I saw that the reference counter numbers in the main text
would always jump to the next paragraph!  After few minutes .. OK
.. after a lot **more** minutes, after reviewing the generated HTML, and
comparing with the CSS seen on [various](https://codepen.io/dredmorbius/details/OVmBaZ) [sites](https://takeonrules.com/about/), I realized that the
`<aside>` tag was the culprit!

Later, I find this [another blog post](https://danilafe.com/blog/sidenotes/) which confirmed what I concluded
above:

> Markdown rendering generates `<p>` tags. According the to spec, `<p>`
> tags cannot have a block element inside them. When you try to put a
> block element, such as `<div>` inside `<p>`, the browser will
> automatically close the `<p>` tag, breaking the rest of the page. So,
> even though conceptually (and visually) the content of the sidenote is
> a block, it has to be inside an inline element.

<mark>The solution was to use an inline HTML element `<small>` or
`<span>` for the sidenote content.</mark> <a id="code-snippet--sidenote-html-template"></a>
```html
<span class="sidenote-number">
  <small class="sidenote">
    sidenote content
  </small>
</span>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-html-template">Code Snippet 1</a>:</span>
  Sidenote HTML template
</div>

The <span class="org-radio" id="org-radio--sidenote-element">sidenote element</span> that's referenced few times in this post is
the `<small>` HTML element with `sidenote` _class_.


## Basic CSS {#basic-css}

Now to point [(iii)](#org-target--kenneth-point-three) from Kenneth's blog ..

Here are my some key takeaways from his blog post:

1.  Use `float: right;`
{{% sidenote %}}
    I used `float: left;` because I have the Table of Contents in the
    right margin. I am going down the untrodden path of putting
    sidenotes in the left margin.
    {{% /sidenote %}} to move the sidenote content to stick to the right side of its
    parent HTML element.
2.  Use `width: 20vw;`
{{% sidenote %}}
    "1vw" = 1% of the width of the _viewport_, and "viewport" is the
    browser window size.
    {{% /sidenote %}} to limit the width of the sidenotes in the margin. We don't want
    the sidenotes to overflow into the body text.
3.  Use `margin-right: -22vw;`
{{% sidenote %}}
    I used `margin-left` instead.
    {{% /sidenote %}} to shift the whole sidenotes containing HTML element to outside the
    body.
4.  And finally, do **not** use the `<aside>` HTML element to contain the
    sidenote content---Use an inline element like `<small>` instead.

Now I'll dive into the details of the CSS code. Please bear with me
because I am not a web developer. These notes are mainly to document
the CSS for myself and to share what I learned. So I would welcome any
suggestions and corrections.


## Sidenote CSS {#sidenote-css}

This is the basic CSS that puts the [sidenote element](#org-radio--sidenote-element) in the margin to
the left of its container element.

<a id="code-snippet--sidenote-css-wide-viewport"></a>
```css { linenos=true, anchorlinenos=true, lineanchors=org-coderef--1387e6 }
.sidenote {
    font-size: 80%;
    position: relative;
}
/* Wide viewport */
@media (min-width: 1400px) {
    .sidenote {
        float: left;
        clear: left;
        margin-left: -23vw;
        text-align: right;

        top: -3rem;
        width: 20vw;
        margin-top: 1rem;
    }
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-css-wide-viewport">Code Snippet 2</a>:</span>
  Sidenote CSS for normal or wide viewport
</div>

-   [Line 2](#org-coderef--1387e6-2) : Sidenote content font size is set to be
    slighter smaller than the default body font size.
-   [Line 3](#org-coderef--1387e6-3) : The `position` specified [later using `top`](#org-coderef--1387e6-13)
    is _relative_ to the position of the element.
-   [Line 6](#org-coderef--1387e6-6) : For this website, when the browser window is
    wider than 1400px, it's considered as _wide viewport_. The sidenotes
    are shown in the margin only for wide viewports.
-   [Line 8](#org-coderef--1387e6-8) : The whole [sidenote element](#org-radio--sidenote-element) is floated to the
    left of the parent container.
-   [Line 9](#org-coderef--1387e6-9) : This is necessary to prevent adjacent sidenotes
    from overlapping. [This blog post](https://dev.to/neshaz/explanation-of-the-css-clear-float-property-examples-5hd2) puts it nicely:

    > If an element can fit in the horizontal space next to the floated
    > elements, _it will_. Unless you apply the `clear` property to that
    > element in the
<mark>same direction as the float.</mark> Then the element will move below the floated elements.
-   [Line 10](#org-coderef--1387e6-10) : The `float` moves the [sidenote element](#org-radio--sidenote-element) to
    the left most side of the container. But the margin is still further
    left to that container's left border. So the `margin-left: -<val>;`
    shifts the [sidenote element](#org-radio--sidenote-element) `<val>` units
<mark>further left to the container's left border.</mark>
-   [Line 11](#org-coderef--1387e6-11) : As the sidenote is floating in the left
    margin, I wanted the text to the aligned towards
{{% sidenote %}}
    Towards the right ⇶ in _wide_ viewport, but it's left-aligned in
    _narrow_ viewport as you'll see in the CSS snippet for narrow
    viewport.
    {{% /sidenote %}} the body text.
-   [Line 14](#org-coderef--1387e6-14) : The `width` limits the width of the "sidenotes
    column" that gets created in the left margin.
-   [Line 15](#org-coderef--1387e6-15) : The `margin_top: 1rem;` is just an
    embellishment tweak that inserts a _1rem_ space between adjacent
    sidenotes if they happen to get packed too close vertically.


### CSS for mobile view or narrow viewport {#css-for-mobile-view-or-narrow-viewport}

On a narrow viewport like a phone, we cannot afford to display a wide
margin just for showing the sidenotes. So I interleave the sidenotes
within the body, but with some indentation on the left ([line
9 below](#org-coderef--9b2ea7-9)).

As the sidenotes are within the body, they are still floated to the
left, but now the text is aligned to the [left](#org-coderef--9b2ea7-5) instead of right. The
[width](#org-coderef--9b2ea7-7) is set to 100% so that the text following the sidenote gets
pushed to below it.
{{% sidenote %}}
This part feels like a hack. So if someone can suggest a canonical way
to deal with this, please let me know.
{{% /sidenote %}} <a id="code-snippet--sidenote-css-narrow-viewport"></a>
```css { linenos=true, anchorlinenos=true, lineanchors=org-coderef--9b2ea7 }
/* Narrow viewport */
@media (max-width: 1400px) {
    .sidenote {
        float: left;
        text-align: left;

        width: 100%;
        margin: 1rem 0;
        padding-left: 15%;
    }
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-css-narrow-viewport">Code Snippet 3</a>:</span>
  Sidenote CSS for mobile or narrow viewport
</div>


## Sidenote Counter CSS {#sidenote-counter-css}

The sidenote counters are implemented using just CSS.
{{% sidenote %}}
Thanks to [this codepen](https://codepen.io/dredmorbius/details/OVmBaZ) by a user _dredmorbius_ and the CSS on
_takeonrules.com_!
{{% /sidenote %}}

Let's look at the sidenote HTML once again, but this time with some
annotation to help understand how the counter number placement works
in the body text and next to the sidenote in the margin.

<a id="code-snippet--sidenote-html-annotated"></a>
```html
<span class="sidenote-number">❶.sidenote-number  INCREMENT COUNTER
  <small class="sidenote">❷.sidenote::before     COUNTER IN MARGIN
    sidenote content
  </small>❸.sidenote-number::after               REF COUNTER IN BODY
</span>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-html-annotated">Code Snippet 4</a>:</span>
  Sidenote HTML with annotation
</div>

The sidenote counters need to be [reset](#org-coderef--1a233c-3) in the `body` element so that
they always begin from 1 on each page.

From the [above annotated HTML](#code-snippet--sidenote-html-annotated), ❶ is the `.sidenote-number` element
that wraps both of the sidenote counter locations: one in the body
which acts as sidenote reference, and another in the margin next to
the sidenote. So the counter is [incremented](#org-coderef--1a233c-6) only once at each
`.sidenote-number` element.

❷ is the point where the [`.sidenote::before`](#org-coderef--1a233c-9) CSS rule will render the
counter number _before_ the sidenote content. As the `.sidenote`
element is pushed into the margin, this counter number will also be
pushed out along with the sidenote.

And finally, ❸ is the point that still remains at its original place
within the main body. This is where the refernce counter value gets
rendered. The [`.sidenote-number::after`](#org-coderef--1a233c-17) CSS rule is responsible for
this.

<a id="code-snippet--sidenote-counter-css-1"></a>
```css { linenos=true, anchorlinenos=true, lineanchors=org-coderef--1a233c }
/* Sidenote counter */
body {
    counter-reset: sidenote-counter;
}
.sidenote-number {
    counter-increment: sidenote-counter;
}
/* Counter before the sidenote in the margin. */
.sidenote::before {
    content: counter(sidenote-counter)".";
    position: relative;
    vertical-align: baseline;
    font-size: 0.9em;
    font-weight: bold;
}
/* Counter in the main body. */
.sidenote-number::after {
    content: counter(sidenote-counter);
    vertical-align: super;
    font-size: 0.7em;
    font-weight: bold;
    margin-right: 0.5rem;
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-counter-css-1">Code Snippet 5</a>:</span>
  Sidenote Counter CSS
</div>

This final CSS snippet is responsible for highlighting the
corresponding sidenote in the margin when mouse is hovered over a
sidenote reference counter number in the body.

<a id="code-snippet--sidenote-css-highlight"></a>
```css
@media (min-width: 1400px) {
    /* Highlight the sidenote when mouse hovers on the sidenote number in body. */
    .sidenote-number:hover .sidenote {
        background-color: yellow;
    }
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-css-highlight">Code Snippet 6</a>:</span>
  Sidenote CSS Highlight
</div>


## Things to improve {#things-to-improve}

While [floating the sidenotes to the left](#org-coderef--1387e6-8) moves them into the left
margin, that aligns the left border of all the sidenotes to the left
side of the page.

What I really wanted to do is to:

1.  Move the sidenotes into the left margin.
2.  Align the
<mark>right border</mark> of all the sidenotes in a straight line with the left border of the
    body text.[^fn:1]


## Conclusion {#conclusion}

That said, I am quite happy with the way the sidenotes and the
counters turned out.


## More References {#more-references}

<span class="org-target" id="org-target--more-sidenotes-references"></span> Apart from the references already linked
in this post, here are few other references for creating sidenotes
using CSS that I came across during my research:

-   [Sidenotes without javascript with accessibility coding](https://www.kooslooijesteijn.net/blog/sidenotes-without-js)
-   [gwern.net -- Sidenotes](https://www.gwern.net/Sidenotes)

[^fn:1]: I already tried changing [`float: left;`](#org-coderef--1387e6-8) to `float: right;` and
    [`margin-left: -23vw;`](#org-coderef--1387e6-10) to `margin-right: 50vw;` but that has an
    undesirable effect what I don't understand why---Now the sidenotes
    occupy the entire vertical space, even the part in the body.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
