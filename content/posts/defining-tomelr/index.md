+++
title = "Defining tomelr – A library for converting Lisp expressions to TOML"
author = ["Kaushal Modi"]
description = """
  Creating a specification for an Emacs-Lisp library to convert Lisp
  data expressions into easy-to-read TOML strings.
  """
date = 2022-04-28T00:08:00-04:00
tags = ["toml", "100DaysToOffload"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108207920996038729
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Using `json-encode` as reference](#using-json-encode-as-reference)
- [Mapping scalar data to TOML](#mapping-scalar-data-to-toml)
    - [About `:false`](#about-false)
- [Mapping lists to TOML](#mapping-lists-to-toml)
- [Mapping lists of lists to TOML](#mapping-lists-of-lists-to-toml)
- [Mapping other object types](#mapping-other-object-types)
- [Closing](#closing)

</div>
<!--endtoc-->

`ox-hugo` has some custom code that generates [TOML](https://toml.io/en/)
{{% sidenote %}}
I :heart: TOML. As the makers of this config format put it.. "it's a
format _for humans_"! --- No need to deal with indentations, weird
syntax for multi-line strings, no prohibition on adding comments to
the config, or dealing with careful placement of commas and braces.
{{% /sidenote %}} for [Hugo](https://gohugo.io/) front-matter, based on the Org keywords and other meta-data
that the user sets in their Org file. But this TOML generation code is
not generic enough for any TOML object type.

As I am [writing up a definition](https://github.com/kaushalmodi/ox-hugo/pull/504#issuecomment-1093592230) on how to export `:LOGBOOK:` [drawers](https://orgmode.org/manual/Drawers.html "Emacs Lisp: (info \"(org) Drawers\")"),
I felt it's a good time to polish the whole **Lisp data → TOML**
conversion code, and may be package that into a separate library.

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It's kind of an ambitious project --- I am calling it [tom​**el**​r](https://github.com/kaushalmodi/tomelr) :sparkles:<br />

</div>

It's a big undertaking to create a generic library for this kind of
data format conversion. But even before I start coding or think if I
can complete this project, I need to _spec_
{{% sidenote %}}
I am not sure if it's a widely used verb, but _to spec_ means _to
write a specification for something_.
{{% /sidenote %}} it. I need to understand early-on how the _S-exp_ (Symbolic lisp
expression) would needed to look for each kind of generated TOML
object --- scalars, lists, lists of lists, maps, lists of maps, etc.


## Using `json-encode` as reference {#using-json-encode-as-reference}

The aim of the `tomelr` library is to take some `(lisp data)` and
convert that TOML. But I did not want to invent my own _lisp data
convention_ for this! So I decided to stick with the lisp expression
conventions understood by the `json-encode` function from the Emacs
core library `json.el`.

Credit for the `json.el` idea goes to [this tweet](https://twitter.com/pdcawley/status/1519007598896369664) by [Piers Cawley](https://twitter.com/pdcawley):

> I'd suggest that
{{% sidenote %}}
> By "that", he's referring to adding support for exporting front-matter
> to JSON in `ox-hugo`.
> {{% /sidenote %}} , since emacs has built in JSON support these days, you
> don't really have to worry about the commas and braces, just build the
> s-exp you want and export, but it's you that's writing the code and
> I'm just delighted that it exists.
>
> Thank you for your efforts.

I am not sold on adding support of yet another front-matter format to
`ox-hugo`. I might not use `json.el` for that, but it definitely
helped me a lot with coming up with this library's spec :laughing:.


## Mapping scalar data to TOML {#mapping-scalar-data-to-toml}

Figuring out the Lisp representation for scalar (plain key-value
pairs) TOML objects was easy. `json.el` helped figure out how to deal
with _nil_ and _false_ values.

<a id="table--mapping-scalar-lisp-to-toml"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--mapping-scalar-lisp-to-toml">Table 1</a>:</span>
  Mapping of <i>scalar</i> Lisp data to TOML
</div>

| Lisp S-exp                   | TOML                    |
|------------------------------|-------------------------|
| `'((int_key . 123))`         | `int_key = 123`         |
| `'((float_key . 1.23))`      | `float_key = 1.23`      |
| `'((date_key . 2022-04-27))` | `date_key = 2022-04-27` |
| `'((bool_key . t))`          | `bool_key = true`       |
| `'((any_key . nil))`         | _(key removed in TOML)_ |
| `'((bool_key . :false))`     | `bool_key = false`      |


### About `:false` {#about-false}

`json.el` defines a variable `json-false` that's set to the value
`:json-false`. This is because in JSON, the _null_ value is different
from the boolean `false` value.

-   _nil_ in Lisp → _null_ in JSON
-   `:json-false` in Lisp → `false` in JSON

Inspired by that decision of `json.el`, I am thinking of using
`:false` as the special value that will set the equivalent TOML value
to boolean `false`.

<div class="note">

TOML does not define a _null_ value, but if the Lisp value is _nil_,
that key will simply not be translated to TOML.

</div>


## Mapping lists to TOML {#mapping-lists-to-toml}

Mapping lists was simple.. because in Lisp, a list value of course
looks like {{< highlight emacs-lisp "hl_inline=true" >}}'((foo . (1 2 3 4 5))){{< /highlight >}}
:smiley:.

<a id="table--mapping-list-lisp-to-toml"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--mapping-list-lisp-to-toml">Table 2</a>:</span>
  Mapping of <i>list</i> Lisp data to TOML
</div>

| Lisp S-exp                       | TOML                          |
|----------------------------------|-------------------------------|
| `'((list_key1 . (1 2 3)))`       | `list_key1 = [1, 2, 3]`       |
| `'((list_key2 . ("a" "b" "c")))` | `list_key2 = ["a", "b", "c"]` |


## Mapping lists of lists to TOML {#mapping-lists-of-lists-to-toml}

<a id="table--mapping-list-of-list-lisp-to-toml"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--mapping-list-of-list-lisp-to-toml">Table 3</a>:</span>
  Mapping of <i>list of list</i> Lisp data to TOML
</div>

| Lisp S-exp                     | TOML                           |
|--------------------------------|--------------------------------|
| `'((lol_key . [(1 2) (3 4)]))` | `lol_key = [ [1, 2], [3, 4] ]` |

I was going to use {{< highlight emacs-lisp "hl_inline=true" >}}'((lol_key . ((1 2) (3 4)))){{< /highlight >}} as the reference Lisp expression for {{< highlight toml "hl_inline=true" >}}lol_key = [ [1, 2], [3, 4] ]{{< /highlight >}}. But I found out that
`json-encode` throws an error if you pass it that expression! I don't
understand the reason for that error, and so I have [asked for help](https://lists.gnu.org/r/help-gnu-emacs/2022-04/msg00240.html) on
the _help-gnu-emacs_ mailing list.

But while that question gets resolved, I wanted to move forward with
the spec definition. After some trial-and-error and
reverse-engineering `json.el`
{{% sidenote %}}
I knew how I wanted TOML to look. So I used [an online JSON/TOML
converter](https://toolkit.site/format.html) to convert that TOML snippet to JSON, and then used
`json-read` to convert JSON to Lisp expression.
{{% /sidenote %}} , I learned that _list of list_ data needs to be represented using a [Vector type](https://www.gnu.org/software/emacs/manual/html_node/elisp/Vector-Type.html "Emacs Lisp: (info \"(elisp) Vector Type\")"), and so {{< highlight emacs-lisp "hl_inline=true" >}}'((lol_key . [(1 2) (3 4)])){{< /highlight >}} would be the correct expression -- _Notice the use
of square brackets instead of parentheses for the outer vector_.


## Mapping other object types {#mapping-other-object-types}

Once I figured out how to map the above data types, mapping Lisp data
to _TOML Tables_ aka _dictionaries_ and _arrays of Tables_ was a
breeze
{{% sidenote %}}
Of course, the _breeze_ is referring to the ease of writing the spec
for these :laughing:. Implementation-wise, the _tables_, _arrays of
tables_, and the especially **nested** variants of those are going to be
the most challenging.
{{% /sidenote %}} .


## Closing {#closing}

It was fun coming up with an initial draft of the specification for
this library. My next steps would be to gradually add TOML generator
functions (as I find time) to this library, along with [ERT tests]({{< relref "quick-intro-to-emacs-lisp-regression-testing" >}})!
Eventually, I will remove the existing TOML generation code from
`ox-hugo` and depend on this library.

Getting back to [my plan](https://github.com/kaushalmodi/ox-hugo/pull/504#issuecomment-1093592230) for exporting `:LOGBOOK:` drawers in
`ox-hugo`, based on this spec, I will need to construct this date in
Emacs-Lisp:

<a id="code-snippet--org-logbook-lisp"></a>
```emacs-lisp
(org_logbook . (((timestamp . 2022-04-08T14:53:00-04:00)
                 (note . "This note addition prompt shows up on typing the `C-c C-z` binding.\nSee [org#Drawers](https://www.gnu.org/software/emacs/manual/html_mono/org.html#Drawers)."))
                ((timestamp . 2018-09-06T11:45:00-04:00)
                 (note . "Another note **bold** _italics_."))
                ((timestamp . 2018-09-06T11:37:00-04:00)
                 (note . "A note `mono`."))))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--org-logbook-lisp">Code Snippet 1</a>:</span>
  Example data from a <code>:LOGBOOK:</code> drawer in Lisp format
</div>

.. will translate to this in TOML:

<a id="code-snippet--org-logbook-toml"></a>
```toml
[[org_logbook]]
  timestamp = 2022-04-08T14:53:00-04:00
  note = """This note addition prompt shows up on typing the `C-c C-z` binding.
See [org#Drawers](https://www.gnu.org/software/emacs/manual/html_mono/org.html#Drawers)."""
[[org_logbook]]
  timestamp = 2018-09-06T11:45:00-04:00
  note = """Another note **bold** _italics_."""
[[org_logbook]]
  timestamp = 2018-09-06T11:37:00-04:00
  note = """A note `mono`."""
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--org-logbook-toml">Code Snippet 2</a>:</span>
  Same example data from the <code>:LOGBOOK:</code> drawer translated to TOML format
</div>

---

Check out the below link where I have documented the equivalent
expressions between Lisp, TOML and JSON for all the object types.

:point_right: [tom​**el**​r Specification](https://github.com/kaushalmodi/tomelr/blob/main/README.org)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
