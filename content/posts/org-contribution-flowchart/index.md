+++
title = "Org Contribution Flow-chart"
author = ["Kaushal Modi"]
description = """
  A handy flow-chart if you are confused about when commits happen to
  Org `bugfix` branch vs `main` branch, what ends up in the GNU Elpa
  version, and so on.
  """
date = 2018-03-06T00:23:00-05:00
images = ["flowchart-cropped.png"]
lastmod = 2022-02-22T00:00:00-05:00
tags = ["plantuml", "flow-chart", "contribution", "development"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  twitter = 970896341999194112
[versions]
  plantuml = "1.2022.1"
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-02-22T00:00:00-05:00
      note = """
  The old `maint` branch is now called the `bugfix` branch, and the
  `master` branch is now called `main`.  Also now stable releases of Org
  happen on GNU Elpa ([ref](https://list.orgmode.org/87blb3epey.fsf@gnu.org/)).
  """
+++

I have often seen questions and confusion about why certain fixes or
features added to Org mode did not show up in its update on GNU Elpa.

Below flow chart is an attempt to answer all such questions, and also
my first attempt at creating one using **PlantUML** (_legacy syntax_)
{{% sidenote %}}
See the [legacy](http://plantuml.com/activity-diagram-legacy) vs [new (beta)](http://plantuml.com/activity-diagram-beta) PlantUML syntax for activity
diagrams. When I tried the new (beta) syntax, it did not allow using
the "labels".. see the `as bugfix`, `as main` syntax in the [flowchart
source code](#org-target--org-contrib-plantuml-source-code).
{{% /sidenote %}} .

<a id="figure--org-contribution-flow-chart"></a>

{{< figure src="flowchart.svg" caption="<span class=\"figure-number\">Figure 1: </span>Flow of a commit in Org mode repo from `bugfix` branch to `main` branch to Emacs repo" >}}

<span class="org-target" id="org-target--org-contrib-plantuml-source-code"></span>

<details>
<summary>PlantUML source code</summary>
<div class="details">

```plantuml
(*) --> "My Org commit"

--> "Discuss on Org mailing list"

if "Bug fix commit?" then
  -->[yes] "Commit to Org **bugfix**" as bugfix
else
  ->[no] if "**bugfix** compatible doc fix commit?" then
    -->[yes] bugfix
  else
    ->[no] "Commit to Org **main**" as main
  endif
endif

bugfix --> "Merge to Org **main**"
  --> main
bugfix --> "Wait till next Monday"
  --> "Push to GNU Elpa" as push
bugfix --> "Short maturity time"
  --> "Cut a minor release" as minor_release

minor_release --> ===RELEASE===
minor_release --> main

main --> "**Long maturity time**"
  --> "Cut a major release" as major_release

major_release --> ===RELEASE===
major_release --> "Merge to Org **bugfix**"
  --> bugfix

===RELEASE=== --> push
===RELEASE=== --> "Squash and commit to Emacs release or main branch"
  --> (*)

push --> (*)
```

Few notes on PlantUML syntax
: | Syntax      | Output           |
    |-------------|------------------|
    | `-​-​>`       | Vertical arrow   |
    | `-​>`        | Horizontal arrow |
    | `(*) -​-​>`   | Start point      |
    | `-​-​> (*)`   | End point        |
</div>
</details>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
