+++
title = "Saving Python pip dependencies"
author = ["Kaushal Modi"]
description = """
  How I generated the Python dependencies file `requirements.txt` so
  that Netlify can install and run the HTML5 Validator before deploying
  this site.
  """
date = 2022-06-14T00:38:00-04:00
series = ["HTML5 Validator"]
tags = ["html", "validator", "python", "pip", "100DaysToOffload", "netlify"]
categories = ["web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  python = "3.7.3"
[syndication]
  mastodon = 108474078058318625
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [<span class="section-num">1</span> Create a _virtual environment_](#create-a-virtual-environment)
- [<span class="section-num">2</span> Activate the _virtual environment_](#activate-the-virtual-environment)
- [<span class="section-num">3</span> Install the packages using `pip`](#install-the-packages-using-pip)
- [<span class="section-num">4</span> Generate `requirements.txt`](#generate-requirements-dot-txt)
- [Netlify deployment](#netlify-deployment)
- [Summary](#summary)

</div>
<!--endtoc-->


Recently I learned about the Python tool `html5validator` tool and
used it to run HTML5 validation on local HTML files. You can read more
about that in the [Offline HTML5 Validator]({{< relref "offline-html5-validator" >}}) post.

But then I wondered .. "Wouldn't it be awesome if I run this
validation step **each time** after Hugo generates this site on [Netlify](https://netlify.com),
but <span class="underline">before</span> it gets deployed?". That curiosity led me to [Netlify's
documentation on Python dependencies](https://docs.netlify.com/configure-builds/manage-dependencies/#python-dependencies), and I learned that Netlify will
run `pip install` to install the dependencies in `requirements.txt`
present in the repository's base directory.

<span class="org-target" id="netlify-pip-freeze"></span> I followed the `pip freeze > requirements.txt`
step in that documentation but that ended up listing **all** my `pip`
installed packages in that file! I needed the `requirements.txt` to
include the dependencies only for `html5validator`. This post was born
in my quest to achieve that.

The solution to this problem was to first create a Python _virtual
environment_ in my site directory, and _then_ do all the `pip`
operations in there. I learned about this _virtual environment_ step
from [this Python Pandemonium post](https://medium.com/python-pandemonium/better-python-dependency-and-package-management-b5d8ea29dff1).

The [**venv** documentation](https://docs.python.org/3/library/venv.html) has a lot of details --- I'll just list the
key steps in this post.


## <span class="section-num">1</span> Create a _virtual environment_ {#create-a-virtual-environment}

_cd_ to your site directory and run the below command create a
virtualenv directory named `pyenv` in there.

```shell
python3 -m venv pyenv
```

<div class="note">

As we are creating this _virtual environment_ just for the sake of
creating a `requirements.txt`, we don't need to commit this
directory to _git_.

</div>


## <span class="section-num">2</span> Activate the _virtual environment_ {#activate-the-virtual-environment}

The virtualenv directory `pyenv/bin/` will have multiple flavors of
shell scripts to activate your virtualenv. I am using
`activate.csh` here as my shell is _tcsh_ :face_with_rolling_eyes:.

```shell
source pyenv/bin/activate.csh
```

Just to emphasize, it is important to use the **`source`** command here,
and not just call the shell script directly.

Once you activate this virtualenv, you should see something like
_[pyenv]_ in the shell prompt.


## <span class="section-num">3</span> Install the packages using `pip` {#install-the-packages-using-pip}

Install all the project-specific packages. In this case it was just
this:

```shell
pip install html5validator
```

The package (and its dependencies) will be installed in
`pyenv/lib/python3.7/site-packages/`. Here, the `python3.7/`
sub-directory name will match the version of Python you have
installed.


## <span class="section-num">4</span> Generate `requirements.txt` {#generate-requirements-dot-txt}

Finally, we run the `pip freeze` command [mentioned](#netlify-pip-freeze) in Netlify docs,
but with the **`--local`** switch:

```shell
pip freeze --local > requirements.txt
```

<div class="note">

The `--local` option ensures that globally-installed packages are not
listed even if the virtualenv has global access.

</div>

Here's the `requirements.txt` created by that command:

<a id="code-snippet--netlify-requirements.txt"></a>
```text
html5validator==0.4.2
PyYAML==6.0
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--netlify-requirements.txt">Code Snippet 1</a>:</span>
  The <code>requirements.txt</code> for Netlify
</div>

Now, I could have just manually typed `html5validator==0.4.2` in a
`requirements.txt` and committed that, and that would work too. But
then, I wouldn't have learned how to create a project-specific _pip
dependency file_ :smile:.


## Netlify deployment {#netlify-deployment}

The `html5validator` has a dependency on Java 8. Luckily the Netlify
environment already comes with that installed. So the only extra setup
needed to make this package work on Netlify was to set the
`PYTHON_VERSION` environment variable to **3.8**
{{% sidenote %}}
The version number should be one of the values listed in [Netlify's
_included software_ list](https://github.com/netlify/build-image/blob/focal/included_software.md).
{{% /sidenote %}} .


## Summary {#summary}

The numbered headings in this post already summarize the steps needed
to create a project-specific `requirements.txt`.

With these in place:

<div class="verse">

:white_check_mark: `requirements.txt` committed in site directory root<br />
:white_check_mark: Netlify environment variable `PYTHON_VERSION` set to 3.8<br />

</div>

my Netlify deployment script now does HTML5 Validation along with few
other checks before this site gets deployed :tada:.

Here's the [full `build.sh` script](https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io/-/blob/master/build.sh).

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
