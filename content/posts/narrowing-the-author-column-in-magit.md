+++
title = "Narrowing the Author column in Magit"
author = ["Kaushal Modi"]
date = 2017-12-18T16:36:00-05:00
lastmod = 2018-08-11T00:00:00-04:00
tags = ["magit", "git", "org", "log"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2018-08-11T00:00:00-04:00
      note = """
  Update for the change in `magit-log-format-margin` signature (Magit
  20180809.1716).
  """
+++

The Org and [Worg](https://orgmode.org/worg/) git repos can be committed to only via _ssh_ protocol
(and not _https_), and for technical reasons, I cannot commit to those
repos via _ssh_ from my primary development machine. So I ended up
with a flow that involves pushing commits to those repos using my
Nexus 6p phone.

[Magit](https://magit.vc/) is how I _git_, and I love to primarily work from the
_&lowast;magit-log&lowast;_ buffer (`M-x magit-status`, <kbd>l</kbd> <kbd>b</kbd>). But the default
column widths were not optimal on a Nexus 6p 5.7&quot; screen. And that's
what inspired this tweak, which looks great on a regular desktop
monitor too.

<!--more-->

Before I jump to the code, you can see in the below figure how the
author and commit age columns took up roughly half the screen width on
my phone before the tweak (left), and how much easier it is to read
the commit messages after the tweak (right).

{{< figure src="/images/magit_author_column.png" caption="<span class=\"figure-number\">Figure 1: </span>Org-mode Worg commit log : Before (Left), After (Right)" link="/images/magit_author_column.png" >}}

So the Magit Log columns tweaks are basically:

1.  Abbreviate the authors' first names to just their initials.
2.  Abbreviate the commit ages to 1-character time units.

To implement those tweaks, I started digging through `magit-log.el`
and found that I would need to customize (i) the `magit-log-margin`
variable and (ii) `magit-log-format-margin` function.

-   Customizing `magit-log-margin` variable allows me to use the
    abbreviated age instead of the verbose age strings (minute→m,
    hour→h, day→d, week→w, month→M, year→Y). It's also where I specify
    how wide I want the author column to be.. I can now reduce that
    column width from the default value of 18 to 11 as I am abbreviating
    the author name too.
-   Reviewing the `magit-log.el` code, I realized that I also need to
    advise the `magit-log-format-margin` function to implement the
    author name abbreviation.

In the below code snippet, I am using [`use-package`](https://github.com/jwiegley/use-package)[^fn:1] to customize
that variable and function in `magit-log`.

```emacs-lisp
(use-package magit-log
  :init
  (progn
    ;; (setq magit-log-margin '(t age magit-log-margin-width t 18)) ;Default value
    (setq magit-log-margin '(t age-abbreviated magit-log-margin-width :author 11)))
  :config
  (progn
    ;; Abbreviate author name. I added this so that I can view Magit log without
    ;; too much commit message truncation even on narrow screens (like on phone).
    (defun modi/magit-log--abbreviate-author (&rest args)
      "The first arg is AUTHOR, abbreviate it.
First Last  -> F Last
First.Last  -> F Last
Last, First -> F Last
First       -> First (no change).

It is assumed that the author has only one or two names."
      ;; ARGS               -> '((REV AUTHOR DATE))
      ;; (car ARGS)         -> '(REV AUTHOR DATE)
      ;; (nth 1 (car ARGS)) -> AUTHOR
      (let* ((author (nth 1 (car args)))
             (author-abbr (if (string-match-p "," author)
                              ;; Last, First -> F Last
                              (replace-regexp-in-string "\\(.*?\\), *\\(.\\).*" "\\2 \\1" author)
                            ;; First Last -> F Last
                            (replace-regexp-in-string "\\(.\\).*?[. ]+\\(.*\\)" "\\1 \\2" author))))
        (setf (nth 1 (car args)) author-abbr))
      (car args))                       ;'(REV AUTHOR-ABBR DATE)
    (advice-add 'magit-log-format-margin :filter-args #'modi/magit-log--abbreviate-author)))
```

**NOTES**

-   I need to set the `magit-log-margin` value in the `:init` block
    because that variable dynamically sets many other variables in
    `magit-log` at the time of loading. So we cannot wait for the whole
    `magit-log` to load before setting that variable. I wouldn't need to
    worry about this if I were using the Emacs _Customize_ interface to
    set this variable.
-   I use `modi/magit-log--abbreviate-author` to add a `:filter-args`
    advice to `magit-log-format-margin`. The signature of the advised
    function is `(magit-log-format-margin REV AUTHOR DATE)` (as of
    _Magit 20180809.1716_). So the advice basically replaces the
    `AUTHOR` arg with its abbreviated form as explained in the
    `modi/magit-log--abbreviate-author` doc-string.

[^fn:1]: Feel free to ask for more explanation in comments in the case
    you don't use `use-package`.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
