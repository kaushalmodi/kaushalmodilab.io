+++
title = """
  "Hello World" for SV/C++ DPI-C integration
  """
author = ["Kaushal Modi"]
description = """
  A little example demonstrating calling a C++ written function in
  SystemVerilog.
  """
date = 2019-04-26T16:16:00-04:00
tags = ["dpi-c", "cpp", "cadence", "xcelium"]
categories = ["systemverilog"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  gcc = "7.2.0"
  xlm = "18.09-s06"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [C++ Header](#c-plus-plus-header)
- [C++ Source Code](#c-plus-plus-source-code)
- [Creating shared object (`.so`)](#creating-shared-object--dot-so)
    - [Verifying that the `.so` actually contains that exported function](#verifying-that-the-dot-so-actually-contains-that-exported-function)
- [SystemVerilog test bench](#systemverilog-test-bench)
- [Result](#result)

</div>
<!--endtoc-->

Let's say we want to call a C++ function named `hello_from_cpp` in
SystemVerilog, and for simplicity, let's say that this is a _void_
returning function and takes no arguments.


## C++ Header {#c-plus-plus-header}

So that function signature would look like:

```cpp
void hello_from_cpp();
```

We need to **export** that function from the C++ compiled library. So we
need to prefix that signature with the `extern` keyword and wrap it
with `extern "C"` as shown below, in `libdpi.h`:

<a id="code-snippet--libdpi-h"></a>
```cpp
// libdpi.h
#ifdef __cplusplus
extern "C" {
#endif

  extern void hello_from_cpp();

#ifdef __cplusplus
}
#endif
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--libdpi-h">Code Snippet 1</a>:</span>
  C++ Header file marking the <code>hello_from_cpp</code> function as <b>exportable</b>
</div>


## C++ Source Code {#c-plus-plus-source-code}

And here's the implementation of that `hello_from_cpp` function in
C++:

<a id="code-snippet--libdpi-cpp"></a>
```cpp
// libdpi.cpp
#include <iostream>
using namespace std;

#include "libdpi.h"

void
hello_from_cpp() {
  cout << "Hello from C++!\n";
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--libdpi-cpp">Code Snippet 2</a>:</span>
  Implementation of the <code>hello_from_cpp</code> function in C++
</div>


## Creating shared object (`.so`) {#creating-shared-object--dot-so}

Cadence Xcelium uses a `libdpi.so` by default as an "SV/DPI Lib"
(`-sv_lib` switch) if it is present in the compilation directory. So
we will simply compile the above to a `libdpi.so`.

<a id="code-snippet--libdpi-cpp-build-steps"></a>
```shell
g++ -c -fPIC libdpi.cpp -o libdpi.o
g++ -shared -Wl,-soname,libdpi.so -o libdpi.so libdpi.o
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--libdpi-cpp-build-steps">Code Snippet 3</a>:</span>
  <code>g++</code> commands to compile <code>libdpi.cpp</code> into the shared object <code>libdpi.so</code>
</div>


### Verifying that the `.so` actually contains that exported function {#verifying-that-the-dot-so-actually-contains-that-exported-function}

This is a [wonderful SO answer](https://stackoverflow.com/a/67985/1219634) that taught me the existence of a CLI
GNU development tool called `nm`. From its _man_ page, this utility nm
list symbols from object files.

Here, I need to know which symbols from the text/code section got
exported to the `libdpi.so`. When I did `nm libdpi.so`, it listed
about two dozen symbols, most of which were gibberish to me. But the
symbol that I cared about: `hello_from_cpp` has a **T** before it.

[`man nm`](http://man7.org/linux/man-pages/man1/nm.1.html) says this about that **T**:

```text
"T"
"t" The symbol is in the text (code) section.
```

So after kind of understanding that, I do:

```shell
nm libdpi.so | rg '\bT\b'
```

and I get:

```text { hl_lines=["3"] }
0000000000000888 T _fini
0000000000000698 T _init
00000000000007cc T hello_from_cpp
```


## SystemVerilog test bench {#systemverilog-test-bench}

With the `libdpi.so` object containing the exported `hello_from_cpp`
ready, we just need to _DPI-C import_ it into the SystemVerilog test
bench.

<a id="code-snippet--libdpi-cpp-sv-tn"></a>
```systemverilog
program top;

  import "DPI-C" function void hello_from_cpp();

  initial begin
    hello_from_cpp();
    $finish;
  end

endprogram : top
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--libdpi-cpp-sv-tn">Code Snippet 4</a>:</span>
  SystemVerilog test bench importing function from C++
</div>


## Result {#result}

And running:

```text
xrun -64bit tb.sv
```

gives:

```text
xcelium> run
Hello from C++!
```

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
