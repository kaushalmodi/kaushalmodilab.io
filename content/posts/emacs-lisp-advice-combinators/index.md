+++
title = "Emacs Lisp: Advice Combinators"
author = ["Kaushal Modi"]
description = "My diagrammatic take on summarizing all the Emacs advice combinators."
date = 2022-06-17T21:30:00-04:00
lastmod = 2022-06-18T08:51:00-04:00
tags = ["100DaysToOffload", "advice", "plantuml"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108495986908506062
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-06-18T08:51:00-04:00
      note = """
  Improved the diagrams for `:before-while`, `:before-until`,
  `:after-while` and `:after-until`. Thanks ****@shom**** for the [feedback](https://fosstodon.org/@shom/108496962414711439).
  """
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Overview on using advices](#overview-on-using-advices)
- [<span class="section-num">1</span> `:before`](#advice-combinators--before)
- [<span class="section-num">2</span> `:after`](#advice-combinators--after)
- [<span class="section-num">3</span> `:override`](#advice-combinators--override)
- [<span class="section-num">4</span> `:around`](#advice-combinators--around)
- [<span class="section-num">5</span> `:before-while`](#advice-combinators--before-while)
- [<span class="section-num">6</span> `:before-until`](#advice-combinators--before-until)
- [<span class="section-num">7</span> `:after-while`](#advice-combinators--after-while)
- [<span class="section-num">8</span> `:after-until`](#advice-combinators--after-until)
- [<span class="section-num">9</span> `:filter-args`](#advice-combinators--filter-args)
- [<span class="section-num">10</span> `:filter-return`](#advice-combinators--filter-return)
- [Summary](#summary)
- [References](#advice-combinators--references)

</div>
<!--endtoc-->


If you have read some of my earlier posts, you would know that I
really enjoy using the Emacs Advice system :smiley:.

The "advice" feature lets you add to the existing definition of a
function, by _advising the function_.  This is a cleaner method than
redefining the whole function, because it's easier to debug and if you
don't need it, you can just _remove_ the advice.

<div class="note">

You can jump to the [References section](#advice-combinators--references) below if you need to look at
the related sections in the Emacs Lisp Manual.

</div>


## Overview on using advices {#overview-on-using-advices}

I do not plan to write a tutorial on how to write advices in
Emacs-Lisp, but here's a 3-second primer:

To add an advice
: {{< highlight emacs-lisp "hl_inline=true" >}}(advice-add 'original-fn <combinator> #'advising-fn){{< /highlight >}}

To remove an advice
: {{< highlight emacs-lisp "hl_inline=true" >}}(advice-remove 'original-fn #'advising-fn){{< /highlight >}}

This article attempts to briefly describe different ways of advising a
function, using 10 different _combinators_. If you have never used
advices in your Emacs config, don't worry. I am hopeful that the
diagrams in this post and the examples linked for some of the
combinators in the Summary section makes this concept a bit easier to
assimilate.

Diagram Legend
: -   Initial black circle: Original Fn input arguments
    -   Yellow box: Original Fn
    -   Gray box: Advising Fn
    -   Black circle inside a white circle: Return values


## <span class="section-num">1</span> `:before` {#advice-combinators--before}

<a id="figure--advice-combinators-before"></a>

{{< figure src="before.svg" caption="<span class=\"figure-number\">Figure 1: </span>**:before** advice" >}}


## <span class="section-num">2</span> `:after` {#advice-combinators--after}

<a id="figure--advice-combinators-after"></a>

{{< figure src="after.svg" caption="<span class=\"figure-number\">Figure 2: </span>**:after** advice" >}}


## <span class="section-num">3</span> `:override` {#advice-combinators--override}

<a id="figure--advice-combinators-override"></a>

{{< figure src="override.svg" caption="<span class=\"figure-number\">Figure 3: </span>**:override** advice" >}}


## <span class="section-num">4</span> `:around` {#advice-combinators--around}

<a id="figure--advice-combinators-around"></a>

{{< figure src="around.svg" caption="<span class=\"figure-number\">Figure 4: </span>**:around** advice" >}}


## <span class="section-num">5</span> `:before-while` {#advice-combinators--before-while}

<a id="figure--advice-combinators-before-while"></a>

{{< figure src="before-while.svg" caption="<span class=\"figure-number\">Figure 5: </span>**:before-while** advice" >}}


## <span class="section-num">6</span> `:before-until` {#advice-combinators--before-until}

<a id="figure--advice-combinators-before-until"></a>

{{< figure src="before-until.svg" caption="<span class=\"figure-number\">Figure 6: </span>**:before-until** advice" >}}


## <span class="section-num">7</span> `:after-while` {#advice-combinators--after-while}

<a id="figure--advice-combinators-after-while"></a>

{{< figure src="after-while.svg" caption="<span class=\"figure-number\">Figure 7: </span>**:after-while** advice" >}}


## <span class="section-num">8</span> `:after-until` {#advice-combinators--after-until}

<a id="figure--advice-combinators-after-until"></a>

{{< figure src="after-until.svg" caption="<span class=\"figure-number\">Figure 8: </span>**:after-until** advice" >}}


## <span class="section-num">9</span> `:filter-args` {#advice-combinators--filter-args}

<a id="figure--advice-combinators-filter-args"></a>

{{< figure src="filter-args.svg" caption="<span class=\"figure-number\">Figure 9: </span>**:filter-args** advice" >}}


## <span class="section-num">10</span> `:filter-return` {#advice-combinators--filter-return}

<a id="figure--advice-combinators-filter-return"></a>

{{< figure src="filter-return.svg" caption="<span class=\"figure-number\">Figure 10: </span>**:filter-return** advice" >}}


## Summary {#summary}

Here's a concise summary of what each advice combinator does. For
brevity, the _advising function_ is called _A_ and the _original
function_ is called _O_.

<div class="note">

Once you click on any of the example posts, search for `advice-add` on
that page to find the code example.

</div>

<a id="table--advice-combinator-summary"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--advice-combinator-summary">Table 1</a>:</span>
  Summary of what each advice combinator means
</div>

| Combinator       | Description                                                                  | Example                                                                                                                         |
|------------------|------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|
| `:before`        | _A_ is called before _O_. _O_ args and return values are not modified.       |                                                                                                                                 |
| `:after`         | _A_ is called after _O_. _O_ args and return values are not modified.        |                                                                                                                                 |
| `:override`      | _A_ is called in lieu of _O_. _A_ gets the same args as _O_.                 | [Zero HTML Validation Errors!]({{< relref "zero-html-validation-errors" >}})                                                    |
| `:around`        | _A_ is called in lieu of _O_. _A_ gets _O_ fn + _O_ args as args.            | [Using Emacs advice to silence messages from functions]({{< relref "using-emacs-advice-to-silence-messages-from-functions" >}}) |
| `:before-while`  | _A_ is called first. If it returns non-nil, _O_ is called.                   |                                                                                                                                 |
| `:before-until`  | _A_ is called first. If it returns nil, _O_ is called.                       | [Org: Show only Post subtree headings]({{< relref "org-show-only-post-subtree-headings" >}})                                    |
| `:after-while`   | _O_ is called first. If it returns non-nil, _A_ is called.                   |                                                                                                                                 |
| `:after-until`   | _O_ is called first. If it returns nil, _A_ is called.                       |                                                                                                                                 |
| `:filter-args`   | _A_ is called first. _O_ is called next with return value from _A_ as input. | [Narrowing the Author column in Magit]({{< relref "narrowing-the-author-column-in-magit" >}})                                   |
| `:filter-return` | _O_ is called first. _A_ is called next with return value from _O_ as input. | [Zero HTML Validation Errors!]({{< relref "zero-html-validation-errors" >}})                                                    |

If you have any feedback on how these diagrams can be made easier to
understand, please let me know.


## References {#advice-combinators--references}

-   [Elisp Info: Advising Functions](https://www.gnu.org/software/emacs/manual/html_node/elisp/Advising-Functions.html "Emacs Lisp: (info \"(elisp) Advising Functions\")")
    -   [Elisp Info: Advice Combinators](https://www.gnu.org/software/emacs/manual/html_node/elisp/Advice-Combinators.html "Emacs Lisp: (info \"(elisp) Advice Combinators\")")

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
