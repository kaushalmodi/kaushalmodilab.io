+++
title = "Sidenotes using ox-hugo"
author = ["Kaushal Modi"]
description = "Adding sidenotes using a Hugo shortcode and ox-hugo."
date = 2022-02-05T18:14:00-05:00
series = ["Sidenotes"]
tags = ["sidenotes", "100DaysToOffload", "ox-hugo", "shortcode", "special-block"]
categories = ["emacs", "org", "hugo", "web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  mastodon = 107748030708167778
  twitter = ""
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Hugo shortcode](#hugo-shortcode)
- [Org mode special block](#org-mode-special-block)
- [Summary](#summary)
    - [One-time configuration](#one-time-configuration)
    - [Org special block: `sidenote`](#org-special-block-sidenote)

</div>
<!--endtoc-->

The main focus of my [ previous post ]({{< relref "sidenotes-using-only-css" >}}) was the CSS for sidenotes. I also
showed the companion HTML wrapper needed for that CSS to work. Here it
is again for convenience:

<a id="code-snippet--sidenote-html-example"></a>
```html
<span class="sidenote-number">
  <small class="sidenote">
    sidenote content
  </small>
</span>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-html-example">Code Snippet 1</a>:</span>
  Sidenote HTML example
</div>


## Hugo shortcode {#hugo-shortcode}

Now .. You might be thinking that it's not too practical to type that
thing for each new sidenote. And you are right! That's where we use
the [Hugo Shortcode](https://gohugo.io/content-management/shortcodes/) feature.
{{% sidenote %}}
_Hugo Shortcodes_ are like Org Macros that are typically used to
insert some HTML template around the user entered content.
{{% /sidenote %}} If you look at the [above snippet](#code-snippet--sidenote-html-example), the outer `span` and `small` tags
are just HTML boilerplate that the user will need to type mechanically
around each sidenote. So the shortcodes are perfect for solving this
annoyance.

If we design a _paired_ shortcode for this, the above HTML example
will change to below:

<a id="code-snippet--sidenote-shortcode-use"></a>
```md
{{%/* sidenote */%}}
sidenote content
{{%/* /sidenote */%}}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-shortcode-use">Code Snippet 2</a>:</span>
  Example use of the <code>sidenote</code> shortcode
</div>

As you can see, this shortcode
{{% sidenote %}}
This is a markdown-rendering shortcode i.e. a shortcode with `{{​%
.. %}}` instead of `{{​< .. >}}`. So the insides of this shortcode
will passed on to the Hugo Markdown renderer.
{{% /sidenote %}} _now_ enables the user to
<mark>focus on content writing</mark> than on writing boilerplate HTML. And the definition of that shortcode
is pretty simple as well:

<a id="code-snippet--sidenote-shortcode"></a>
```html
<span class="sidenote-number"><small class="sidenote">{{ .Inner }}</small></span>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-shortcode">Code Snippet 3</a>:</span>
  "sidenote.html" Hugo shortcode
</div>

The only Hugo-templating syntax used here is `{{ .Inner }}`. You can
read more about it [here](https://gohugo.io/templates/shortcode-templates/#inner), but the _tl;dr_ is that the `.Inner` holds
whatever content the user entered _inside_ the `{{​% sidenote %}}` and
`{{​% /sidenote %}}` tags.

Once you save [that shortcode snippet](#code-snippet--sidenote-shortcode) to a `layouts/sidenote.html` file
in your Hugo site's directory, you are ready to use it.

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:stop_sign: But wait! :stop_sign:<br />

</div>


## Org mode special block {#org-mode-special-block}

It would be a shame to populate the Org source of this blog with
Hugo-specific shortcodes when I am using [ox-hugo](https://ox-hugo.scripter.co/) :smile:.

So instead, we just use the plain-old Org special blocks and tell
_ox-hugo_ that this "sidenote" special block be exported as a
markdown-rendering Hugo shortcode. I do that by just placing this at
the top of my Org file
{{% sidenote %}}
See the [Hugo Paired Shortcodes](https://ox-hugo.scripter.co/doc/shortcodes/#hugo-paired-shortcodes) section in the _ox-hugo_ manual to learn
more about this.
{{% /sidenote %}} :

<a id="code-snippet--sidenote-hugo-paired-shortcode"></a>
```org
#+hugo_paired_shortcodes: %sidenote
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-hugo-paired-shortcode">Code Snippet 4</a>:</span>
  <code>#+hugo_paired_shortcodes</code> keyword for specifying "shortcode" special blocks
</div>

.. and then writing a "sidenote" type Org special block:

<a id="code-snippet--sidenote-special-block-use"></a>
```org
abc
#+begin_sidenote
example sidenote content
#+end_sidenote
def
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-special-block-use">Code Snippet 5</a>:</span>
  Example use of the <code>sidenote</code> special block
</div>

Normally, Org mode would insert a paragraph break where any Org block
(like that _sidenote_ special block) is used. So above would render
as:

<div style="color:green;" class="green">

abc

{{% sidenote %}}
example sidenote content
{{% /sidenote %}}

def

</div>

But I am pretty sure that nobody would render sidenote references in
their main text as above---instead, below is more _normal_ :smile:.

<div style="color:green;" class="green">

abc
{{% sidenote %}}
example sidenote content
{{% /sidenote %}} def

</div>

In that _normal_ example of the sidenote, _ox-hugo_ trims the whitespace
around the _sidenote_ block. That is configured by customizing the
`org-hugo-special-block-type-properties` variable:

<a id="code-snippet--sidenote-whitespace-trimming"></a>
```emacs-lisp
(with-eval-after-load 'ox-hugo
  (add-to-list 'org-hugo-special-block-type-properties '("sidenote" . (:trim-pre t :trim-post t))))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sidenote-whitespace-trimming">Code Snippet 6</a>:</span>
  Whitespace trimming enabled by default for <code>sidenote</code> special blocks
</div>

You can read more about this _whitespace trimming_ feature in the
[ox-hugo manual](https://ox-hugo.scripter.co/doc/org-special-blocks/#whitespace-trimming).


## Summary {#summary}


### One-time configuration {#one-time-configuration}

1.  Create and save a "sidenote" Hugo shortcode as shown [here](#code-snippet--sidenote-shortcode).
2.  Configure _ox-hugo_ to export "sidenote" special block as a
    markdown-rendering shortcode.
3.  Configure whitespace to be trimmed around those blocks using
    `org-hugo-special-block-type-properties` customization variable.


### Org special block: `sidenote` {#org-special-block-sidenote}

Now just use the `#+begin_sidenote` .. `#+end_sidenote` Org special
blocks wherever you need sidenotes.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
