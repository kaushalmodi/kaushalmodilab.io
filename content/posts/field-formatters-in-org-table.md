+++
title = "Field Formatters in Org table"
author = ["Kaushal Modi"]
description = "Using field-formatters to format results of Org table formulae."
date = 2018-03-05T17:40:00-05:00
tags = ["table", "field-formatter"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Using `printf` style `%0.1f` formatter](#using-printf-style-0-dot-1f-formatter)
- [Using _Calc_ `f1` formatter](#using-calc-f1-formatter)

</div>
<!--endtoc-->

If you have a simple Org table like:

```org
| 0.5 |
| 1.5 |
|-----|
|     |
```

And you want the _A3_ cell to contain the sum of _A1_ and _A2_ cells,
you would add this at the bottom on that table and evaluate it (<kbd>C-c
C-c</kbd>).

```org
#+tblfm: @>$1=vsum(@1..@>>)
```

That formula reads as: "Set the value of the cell is last row
(`@>`)[^fn:1], first column (`$1`) be equal to the sum of all cells in
the same column from row 1 (`@1`) to second-to-the-last row (`@>>`)."

But then you end up with an odd-looking `2.` instead of `2.0` in the
result cell:

```org { hl_lines=["4"] }
| 0.5 |
| 1.5 |
|-----|
|  2. |
#+tblfm: @>$1=vsum(@1..@>>)
```

So I had [posted](https://lists.gnu.org/r/emacs-orgmode/2016-04/msg00581.html) a question on the Org mailing list to understand if
this was a bug --- _it was not_.


## Using `printf` style `%0.1f` formatter {#using-printf-style-0-dot-1f-formatter}

Thanks to the reply from Thierry Banel to that question, one of the
solutions is to use _field formatters_, like in `printf` in C (and
many other languages):

```org { hl_lines=["4","5"] }
| 0.5 |
| 1.5 |
|-----|
| 2.0 |
#+tblfm: @>$1=vsum(@1..@>>);%0.1f
```


## Using _Calc_ `f1` formatter {#using-calc-f1-formatter}

Another solution, also from Thierry, was to use the _Calc_ `f1`
formatter instead of `%0.1f` (and similarly `f5` instead of `%0.5f`).

```org { hl_lines=["4","5"] }
| 0.5 |
| 1.5 |
|-----|
| 2.0 |
#+tblfm: @>$1=vsum(@1..@>>);f1
```

Note
: While the `%.1f` format is handy for those who are used to
    `printf()` syntax, note that Calc unlimited precision
    numbers are converted to double floats before applying
    `%.1f`. Whereas `f1` operates on Calc numbers without
    conversion.

[^fn:1]: See <a href="https://orgmode.org/manual/References.html"><abbr title="Read the same section within Emacs by doing 'C-h i g (org) References'">(org) References</abbr></a> for more information on those field references.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
