+++
title = "Hugo: Leaf and Branch Bundles"
author = ["Kaushal Modi"]
description = "Content organization for Hugo posts/articles using _Page Bundles_."
date = 2018-03-06T12:40:00-05:00
images = ["flowchart-cropped.png"]
lastmod = 2022-03-11T00:00:00-05:00
tags = ["page-bundles", "leaf", "branch", "flow-chart"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  twitter = 971463098577182722
[versions]
  plantuml = "1.2018.2"
  hugo = "v0.94.1"
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-03-11T00:00:00-05:00
      note = """
  Fix the `term` and `taxonomy` page Kind references updated as of [Hugo
  v0.73.0](https://github.com/gohugoio/hugo/releases/tag/v0.73.0).
  """
    [[logbook._toplevel.notes]]
      timestamp = 2019-01-04T00:00:00-05:00
      note = "Use the Hugo v0.53 introduced `site` var in examples."
    [[logbook._toplevel.notes]]
      timestamp = 2018-03-23T00:00:00-04:00
      note = "Clarify the leaf vs branch decision making section."
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Quick Summary](#quick-summary)
- [Leaf Bundle](#leaf-bundle)
    - [Examples of Leaf Bundle organization](#examples-of-leaf-bundle-organization)
    - [Headless Bundle](#headless-bundle)
- [Branch Bundle](#branch-bundle)
    - [Examples of Branch Bundle organization](#examples-of-branch-bundle-organization)
    - [Branch Bundle vs Regular Sections](#branch-bundle-vs-regular-sections)
- [So, which bundle should I use?](#so-which-bundle-should-i-use)
- [Upgrading to Hugo v0.32?](#upgrading-to-hugo-v0-dot-32)
- [Examples](#examples)
- [Further Reading](#further-reading)

</div>
<!--endtoc-->


Hugo v0.32 introduced a new feature called **Page Bundles** ([1](https://gohugo.io/content-management/organization/#page-bundles), [2](https://gohugo.io/news/0.32-relnotes/)), as a
way to organize the content files. It is useful for cases where a page
or section's content needs to be split into multiple content pages for
convenience, or has associated attachments like documents or images.

A Page Bundle can be one of two types:

-   Leaf :leaves: Bundle
-   Branch :herb: Bundle

<div class="note">

The _leaf_ and _branch_ _nodes_ from [Data Structure _Trees_](https://en.wikipedia.org/wiki/Tree_(data_structure)) serve as a
great analogy to help relate with the Hugo _bundles_.

> Leaf
> : A node with no children.
>
> Branch
> : A node with at least one child.

</div>


## Quick Summary {#quick-summary}

Here's a quick summary of the differences between these bundle types ---
Details follow in the sections after.

<a id="table--leaf-vs-branch"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--leaf-vs-branch">Table 1</a>:</span>
  Leaf Bundle vs Branch Bundle
</div>

|                                      | Leaf Bundle                                              | Branch Bundle                                                                                                                                                                                                      |
|--------------------------------------|----------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Usage                                | Collection of content and attachments for single pages   | Collection of attachments for section pages (home page, sections, `taxonomy` list pages, `term` list pages)                                                                                                        |
| Index file name                      | `index.md` [^fn:1]                                       | `_index.md` [^fn:1]                                                                                                                                                                                                |
| Allowed Resources                    | Page and non-page (like images, pdf, etc) types          | Only non-page (like images, pdf, etc.) types                                                                                                                                                                       |
| Where can the Resources live?        | At any directory level within the leaf bundle directory. | Only in the directory level **of** the branch bundle directory i.e. the directory containing the `_index.md` ([ref](https://discourse.gohugo.io/t/question-about-content-folder-structure/11822/4?u=kaushalmodi)). |
| Layout type                          | `single`                                                 | `list`                                                                                                                                                                                                             |
| Nesting                              | Does not allow nesting of more bundles under it          | Allows nesting of leaf or branch bundles under it                                                                                                                                                                  |
| Example                              | `content/posts/my-post/index.md`                         | `content/posts/_index.md`                                                                                                                                                                                          |
| Content from non-index page files .. | Accessed only as page resources                          | Accessed only as regular pages                                                                                                                                                                                     |

<div class="note">

[Hugo v0.73.0](https://github.com/gohugoio/hugo/releases/tag/v0.73.0) made a much needed fix --- It finally renamed `taxonomy`
Page Kind to `term`, and `taxonomyTerm` to `taxonomy`. Now if you have
a `tags = ["abc"]` front-matter, _tags_ is the **taxonomy** and _abc_ is
a **term**.

</div>


## Leaf Bundle {#leaf-bundle}

A _Leaf Bundle_ is a directory at any hierarchy within the `content/`
directory, that contains an **`index.md`** file.


### Examples of Leaf Bundle organization {#examples-of-leaf-bundle-organization}

```text
content/
├── about
│   └── index.md
├── posts
│   ├── my-post
│   │   ├── content1.md
│   │   ├── content2.md
│   │   ├── image1.jpg
│   │   ├── image2.png
│   │   └── index.md
│   └── my-another-post
│       └── index.md
└── another-section
    ├── ..
    └── not-a-leaf-bundle
        ├── ..
        └── another-leaf-bundle
            └── index.md
```

In the above example `content/` directory, there are four leaf
bundles:

about
: This leaf bundle is at the root level (directly under
    `content` directory) and has only the `index.md`.

my-post
: This leaf bundle has the `index.md`, two other content
    Markdown files and two image files. The content from the
    two non-index `.md` files can be accessed **only as page
    resources, not as regular files** i.e. those non-index
    `.md` files will not have their own permalinks or HTML
    files in `public/`.

my-another-post
: This leaf bundle has only the `index.md`.

another-leaf-bundle
: This leaf bundle is nested under couple of
    directories. This bundle also has only the `index.md`.

<div class="note">

The hierarchy depth at which a leaf bundle is created does not matter,
as long as it is not inside another **leaf** bundle.

</div>


### Headless Bundle {#headless-bundle}

A headless bundle is a bundle that is configured to **not** get
published anywhere:

-   It will have no `Permalink` and no rendered HTML in `public/`.
-   It will not be part of `site.RegularPages`, etc.

But you can get it by `site.GetPage`. Here is an example:

```go-html-template
{{ $headless := site.GetPage "page" "some-headless-bundle" }}
{{ $reusablePages := $headless.Resources.Match "author*" }}
<h2>Authors</h2>
{{ range $reusablePages }}
    <h3>{{ .Title }}</h3>
    {{ .Content }}
{{ end }}
```

A leaf bundle can be made headless by adding below in the Front Matter
(in the `index.md`):

```toml
headless = true
```

<div class="note">

Only leaf bundles can be made headless.

</div>

Example uses of headless page bundles:

-   Shared media galleries
-   Reusable page content "snippets"


## Branch Bundle {#branch-bundle}

A _Branch Bundle_ is a directory at any hierarchy within the
`content/` directory, that contains an **`_index.md`** file.  This
`_index.md` can also be directly under the `content/` directory, to
set the Home page content and/or front-matter variables.

By default, Hugo considers the **first directory level** under
`content/` as a **section**. But a branch bundle can be used to create a
_section_ at any hierarchy.


### Examples of Branch Bundle organization {#examples-of-branch-bundle-organization}

```text
content/
├── _index.md
├── branch-bundle-2
│   ├── branch-content1.md
│   ├── branch-content2.md
│   ├── image1.jpg
│   ├── image2.png
│   └── _index.md
└── branch-bundle-3
    ├── _index.md
    └── a-leaf-bundle
        └── index.md
```

In the above example `content/` directory, there are three branch
bundles (and a leaf bundle):

Home page as branch bundle
: The home page content is organized as a
    branch bundle, using `_index.md` directly in the `content/`
    directory. It consists of two other branch bundles.

`branch-bundle-2`
: This branch bundle has the `_index.md`, two
    other content Markdown files and two image files.  The content
    from the two non-index `.md` files can be accessed **only as
    regular pages, not as page resources**. Branch bundles cannot have
    page resources, so any non-index Hugo recognized content file in
    a branch bundle can be accessed like any regular page (just like
    in any section.. as.. a _branch bundle_ **is** a _section_ too).

`branch-bundle-3`
: This branch bundle has the `_index.md` and a
    nested leaf bundle.

<div class="note">

The hierarchy depth at which a branch bundle is created does not
matter.

</div>


### Branch Bundle vs Regular Sections {#branch-bundle-vs-regular-sections}

Branch Bundle section
: Any directory in `content/` that contains
    the `_index.md` file.

Regular Section
: Any **first-level directory** in `content/`, and
    also home page and `taxonomy` list page .. that is **not** already a
    Branch Bundle.

<a id="table--branch-bundle-section-vs-regular-section"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--branch-bundle-section-vs-regular-section">Table 2</a>:</span>
  Branch Bundle section vs Regular section
</div>

|                                 | Branch Bundle Section | Regular Section |
|---------------------------------|-----------------------|-----------------|
| `.Kind`                         | `"section"`           | `"section"`     |
| `.File.Path`                    | `/path/to/_index.md`  | `""`            |
| Layout type                     | `list`                | `list`          |
| Can contain non-page Resources? | Yes                   | No              |
| Can contain page Resources?     | No                    | No              |

<div class="note">

Branch Bundle is basically a Section with non-page Resources.

</div>

Here's an example snippet that can be used in a `list` template to
distinguish between the two:

```go-html-template
{{ if (eq .Kind "section") }}                          <!-- Section -->
    {{ if (eq .File.Path "") }} <!-- Regular section -->
        <h1>Posts in ‘{{ .Dir | default .Section }}’</h1>
    {{ else }}                  <!-- Branch bundle section -->
        <h1>{{ .Title }}</h1>
        <h2>Posts in ‘{{ .Dir | default .Section }}’</h2>
    {{ end }}
{{ end }}
```


## So, which bundle should I use? {#so-which-bundle-should-i-use}

In summary:

-   If it's a _regular_ page or a _single_ page where you write your
    content **yourself**, create a _leaf bundle_.

    <mark>Page Bundles of `page` [_Kind_](https://gohugo.io/templates/section-templates/#page-kinds) are always _leaf bundles_.. and vice versa.</mark>
-   Otherwise, if it's a _list_ page which Hugo generates for you,
    create a _branch bundle_. Such pages typically are a list of
    _regular_ pages or even other _list_ pages. Pages of `home`,
    `section`, `taxonomy` and `term` _Kind_ are always _branch bundles_.

<a id="figure--bundle-decision"></a>

{{< figure src="flowchart.svg" caption="<span class=\"figure-number\">Figure 1: </span>Do I need a leaf bundle, or branch bundle, or none?" >}}


## Upgrading to Hugo v0.32? {#upgrading-to-hugo-v0-dot-32}

All of this boils down to these few caution points if you are
upgrading Hugo from a pre-0.32 version to 0.32 or a newer version:

-   If a directory `foo` has an `index.md` (leaf bundle), that file will
    be the content file for that `foo` _Regular_ Page.
-   If a directory `foo` has an `_index.md` (branch bundle), that file
    will be the content file for that `foo` _Section_ Page, and _all
    other_[^fn:2] `.md` files in the same or deeper hierarchy levels
    under `foo/` will become _Regular_ Pages under that `foo` section.


## Examples {#examples}

Of course, a post like this is not complete without examples, so here
they are:

<a id="table--leaf-branch-bundle-examples"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--leaf-branch-bundle-examples">Table 3</a>:</span>
  Leaf and Branch bundle examples
</div>

| Example                                                             | Markdown source                                                                                                                                                                                                                                                                                                    | HTML output                                                                                                   |
|---------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| **This page** (leaf bundle)                                         | [`content/posts/hugo-leaf-and-branch-bundles/`](https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io/tree/master/content/posts/hugo-leaf-and-branch-bundles)                                                                                                                                                       | _duh!_                                                                                                        |
| Leaf and branch bundle examples from `ox-hugo` test site            | [`content/bundles/`](https://github.com/kaushalmodi/ox-hugo/tree/master/test/site/content/bundles)                                                                                                                                                                                                                 | [Bundles](https://ox-hugo.scripter.co/test/bundles/)                                                          |
| Headless (leaf) bundle example from the same                        | [content page using headless bundle](https://raw.githubusercontent.com/kaushalmodi/ox-hugo/master/test/site/content/posts/page-using-headless-page-bundle.md) + [Layout fetching headless bundle pages](https://github.com/kaushalmodi/ox-hugo/blob/master/test/site/layouts/_default/headless-bundle-single.html) | [Page using the Headless Page Bundle](https://ox-hugo.scripter.co/test/posts/page-using-headless-page-bundle) |
| Branch bundles used for `home`, `taxonomy`, and `term` _Kind_ pages | [Hugo _Sandbox_ `content/` dir](https://gitlab.com/kaushalmodi/hugo-sandbox/tree/master/content)                                                                                                                                                                                                                   | [Hugo Sandbox](https://hugo-sandbox.netlify.com/)                                                             |


## Further Reading {#further-reading}

I suggest reading the below pages in the Hugo documentation, as they
complement the _Page Bundles_ concept:

-   [Content Organization](https://gohugo.io/content-management/organization/)
-   [Page Resources](https://gohugo.io/content-management/page-resources/)
-   [Image Processing](https://gohugo.io/content-management/image-processing/)

[^fn:1]: The `.md` extension for `index.md`, `_index.md`, and all other
    content files in this post is just an example. The extension can be
    `.html` or any of any valid MIME type recognized by Hugo.
[^fn:2]: It's a qualified "all other" --- That does not count the content
    files further nested in leaf and branch bundles in that `foo` section.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
