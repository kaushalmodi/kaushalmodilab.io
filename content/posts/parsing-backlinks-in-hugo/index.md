+++
title = "Parsing Backlinks in Hugo"
author = ["Kaushal Modi"]
description = """
  A Hugo partial to parse all the _backlinks_ to any post from the same
  Hugo-generated website.
  """
date = 2022-04-20T09:50:00-04:00
tags = ["partials", "backlinks", "100DaysToOffload"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
mathjax = true
[versions]
  hugo = "v0.97.0"
[syndication]
  mastodon = 108164931549722594
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Code](#code)
    - [<span class="section-num">1</span> Create a `backlinks.html` _partial_](#create-a-backlinks-dot-html-partial)
    - [<span class="section-num">2</span> Use the partial](#use-the-partial)
- [Features and Improvements](#features-and-improvements)
- [Closing](#closing)

</div>
<!--endtoc-->

If a post is referred to in other posts, then all those posts are
creating _backlinks_ to that first post.

As of writing this (<span class="timestamp-wrapper"><span class="timestamp">&lt;2022-04-19 Tue&gt;</span></span>), Hugo doesn't have a built-in
way to generate a list of such backlinks, though there's an open issue
([# 8077](https://github.com/gohugoio/hugo/issues/8077)) to track this feature request.

One way to gather a list of backlinks to a post is to find out that
post's relative or absolute _permalink_, and search for the
occurrences of that link in all the other posts on the published
site. The author of [seds.nl: Export org-roam backlinks with Gohugo](https://seds.nl/notes/export_org_roam_backlinks_with_gohugo/),
Ben Mezger, uses this approach in his solution for creating backlinks
in that post.

In this post, I am expanding upon that solution and refactoring it bit
to fit my needs.


## Code {#code}

Without further ado, here is my version of the _partial_:


### <span class="section-num">1</span> Create a `backlinks.html` _partial_ {#create-a-backlinks-dot-html-partial}

Save this partial to your site repo as
`layouts/partials/backlinks.html`.

<a id="code-snippet--backlinks-partial"></a>
```go-html-template { linenos=true, anchorlinenos=true, lineanchors=org-coderef--e7ca1c }
{{ $backlinks := slice }}
{{ $path_base := .page.File.ContentBaseName }}
{{ $path_base_re := printf `["/(]%s["/)]` $path_base }}

{{ range where site.RegularPages "RelPermalink" "ne" .page.RelPermalink }}
    {{ if (findRE $path_base_re .RawContent 1) }}
        {{ $backlinks = $backlinks | append . }}
    {{ end }}
{{ end }}

{{ with $backlinks }}
    <section class="backlinks">
        {{ printf "%s" ($.heading | default "<h2>Backlinks</h2>") | safeHTML }}
        <nav>
            <ul>
                {{ range . }}
                    <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
                {{ end }}
            </ul>
        </nav>
    </section>
{{ end }}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--backlinks-partial">Code Snippet 1</a>:</span>
  <code>backlinks.html</code> Hugo partial
</div>


### <span class="section-num">2</span> Use the partial {#use-the-partial}

Add a call to this partial in your "single" layout's template file,
which is typically `layouts/_default/single.html`.

<a id="code-snippet--call-backlinks-partial"></a>
```go-html-template
{{ partial "backlinks.html" (dict "page" .) }}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--call-backlinks-partial">Code Snippet 2</a>:</span>
  Calling the <code>backlinks.html</code> partial
</div>


## Features and Improvements {#features-and-improvements}

1.  :sparkles: The partial now accepts a `dict` or a dictionary with
    keys `page` and `heading`.
    -   The `page` key is required to pass the page context from where
        the partial is called.
    -   The `heading` key is optional. This can be used by the user to
        set the "Backlinks" heading differently. For example,
        {{< highlight go-html-template "hl_inline=true" >}}{{ partial "backlinks.html" (dict "page" . "heading" "<h4>Links to this note</h4>") }}{{< /highlight >}}.
2.  :bug: [Line 2](#org-coderef--e7ca1c-2): `.File.BaseFileName` would be
    just "index" for all the [_Leaf Bundles_]({{< relref "hugo-leaf-and-branch-bundles" >}})
{{% sidenote %}}
    If you follow this link and scroll to the bottom of that page, you
    will see a "Backlinks" section auto-generated with the help of this
    partial. This post will be linked there because I just referenced
    that post here.
    {{% /sidenote %}} , and I use them **heavily**! Using `.File.ContentBaseName` fixes
    this problem. See [:open_book: Hugo File Variables](https://gohugo.io/variables/files/) for more
    info. [Credit: [_@sjgknight_](https://github.com/benmezger/blog/issues/8#issuecomment-894750079)]
3.  :bug: [Line 3](#org-coderef--e7ca1c-3): Reduce false matches for
    backlinks by making the regular expression a bit stricter. Now it
    will match only if the derived `$path_base` variable is found
    wrapped in characters like **`"`**, **`/`**, **`(`** or **`)`**.
    -   If `$path_base` is 'hello', I don't want its mere reference like
        in this line to create a backlink on that 'hello' post!
    -   Instead, a match will happen only if something like **`"hello"`**
        (as in {{< highlight go-html-template "hl_inline=true" >}}{{</* relref "hello" */>}}{{< /highlight >}}), or **`/hello"`** (as in {{< highlight go-html-template "hl_inline=true" >}}{{</* relref "/posts/hello" */>}}{{< /highlight >}}), or **`/hello)`** (as in
        {{< highlight md "hl_inline=true" >}}[Hello](/hello){{< /highlight >}}) is found in the raw
        Markdown content (`.RawContent`).
4.  :zap: [Line 5](#org-coderef--e7ca1c-5): Look for backlinks only in
    `site.RegularPages`. See [:open_book: About site Pages variables](https://gohugo.io/variables/site/#sitepages-compared-to-pages) for
    more info. `site.AllPages` includes **all** pages.. even the list
    pages like section and taxonomy pages which, I believe, won't
    contain backlinks.
5.  :zap: The `findRE` in [line 6](#org-coderef--e7ca1c-6) is slightly optimized
    by quitting the search immediately as soon as the first match is
    found.
6.  :lipstick: Rest of the changes are just using a different style of
    coding using Hugo templates and creating a different structure in
    HTML.


## Closing {#closing}

This partial works great for this site --- adds only a few hundred
_milliseconds_ to the build time.

But it's not an efficient solution. The partial is called in the
_single_ template where it searching for backlinks to the current page
in **all** other regular pages, and the _single_ template is evaluated
for **all** the regular pages. So its [\\(O\\) notation](https://en.wikipedia.org/wiki/Big_O_notation) will be close to
\\(O(n^2)\\) where \\(n\\) is the number of regular pages.

I have only about a hundred regular pages at the moment, but I can see
this partial taking a major chunk of the build time
{{% sidenote %}}
The `hugo --templateMetrics --templateMetricsHints` command prints a
table listing all the partials used in the build and how much time
each of them took. See [:open_book: Hugo Build Performance](https://gohugo.io/troubleshooting/build-performance/) for more
info.
{{% /sidenote %}} as the number of pages increase.

A built-in support for backlinks from Hugo ([# 8077](https://github.com/gohugoio/hugo/issues/8077)) would really help
in this performance department.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
