+++
title = "Downloading Nim"
author = ["Kaushal Modi"]
description = """
  Download and "installing" Nim using just `curl` and `tar`.
  """
date = 2022-05-10T10:45:00-04:00
tags = ["nim", "installing", "100DaysToOffload"]
categories = ["programming"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  nim = "1.6.6"
  OS = "CentOS 7.6.1810"
[syndication]
  mastodon = 108278224465407303
+++

Today I saw [this toot by user _@hyperlinkyourheart_](https://mastodon.art/@hyperlinkyourheart/108277257649997561) regarding
installing Nim:

> Not a great experience so far though - choosenim is broken on Ubuntu
> 22.04 based systems ..

..  and that inspired this quick post. I would encourage the user
posting that to bring up that issue on the [Nim Forum](https://forum.nim-lang.org/), but here's a
quick stop-gap solution to install Nim using just `curl` and `tar`.

1.  Copy and save the below script somewhere, let's say as
    `~/scripts/download_nim.sh`.
<mark>Update the `nim_install_dir` variable
    in there to your choice.</mark> That is set to {{< highlight bash "hl_inline=true" >}}${HOME}/nim{{< /highlight >}} by default.
    <a id="code-snippet--nim-download"></a>
    ```shell
    #!/usr/bin/env bash

    # Running this script will download and extract nim installation to ~/nim/${nim_version}.
    # To uninstall nim, just remove the ~/nim directory.

    set -euo pipefail # http://redsymbol.net/articles/unofficial-bash-strict-mode
    IFS=$'\n\t'

    nim_version="1.6.6"
    nim_archive_url="https://nim-lang.org/download/nim-${nim_version}-linux_x64.tar.xz"
    nim_install_dir="${HOME}/nim"

    tmp_dir="/tmp/${USER}"
    nim_download_dir="${tmp_dir}/nim-${nim_version}"

    nim_version_dir="${nim_install_dir}/${nim_version}"

    mkdir -p "${tmp_dir}"
    cd "${tmp_dir}" || exit
    echo "Downloading nim archive from ${nim_archive_url} .."
    curl -RLs "${nim_archive_url}" -o "nim.tar.xz"
    tar xf nim.tar.xz # Extracts to ${nim_download_dir}.

    if [[ -d "${nim_version_dir}" ]]
    then
        rm -rf "${nim_version_dir}"
    fi
    mkdir -p "${nim_version_dir}/doc"

    cd "${nim_version_dir}" || exit
    cp -fP "${nim_download_dir}"/doc/*.css ./doc/. # Required for 'nim doc ..' to work
    cp -rfP "${nim_download_dir}"/bin .
    cp -rfP "${nim_download_dir}"/lib .
    cp -rfP "${nim_download_dir}"/compiler . # Required for 'nimterop' package
    cp -rfP "${nim_download_dir}"/config .

    cd "${nim_install_dir}" || exit
    find . -name "bin" -type l -delete
    ln -fs "${nim_version_dir}"/bin ./bin

    echo "Finished downloading $("${nim_version_dir}"/bin/nim -v | head -n 1)"
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--nim-download">Code Snippet 1</a>:</span>
      Nim download script
    </div>
2.  Make the script an executable: {{< highlight shell "hl_inline=true" >}}chmod +x ~/scripts/download_nim.sh{{< /highlight >}}
3.  Run `~/scripts/download_nim.sh`.. This will download the `nim`
    compiler and its standard libraries (totaling to only 40MB!) to the
    path set in {{< highlight bash "hl_inline=true" >}}${nim_install_dir}{{< /highlight >}}.
4.  Make sure that the path in {{< highlight bash "hl_inline=true" >}}${nim_install_dir}{{< /highlight >}} is added to your **PATH**.
5.  Run `nim --version` to check the installation.
    ```text
    Nim Compiler Version 1.6.6 [Linux: amd64]
    Compiled at 2022-05-05
    Copyright (c) 2006-2021 by Andreas Rumpf

    git hash: 0565a70eab02122ce278b98181c7d1170870865c
    active boot switches: -d:release
    ```

Now head over to <https://nim-lang.org/learn.html> to learn this
awesome :heart: language!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
