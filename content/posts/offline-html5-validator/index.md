+++
title = "Offline HTML5 Validator"
author = ["Kaushal Modi"]
description = "Validate your website offline --- It's just one `curl` command away."
date = 2022-06-01T00:11:00-04:00
series = ["HTML5 Validator"]
tags = ["html", "validator", "100DaysToOffload"]
categories = ["web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  openjdk = "13.0.1"
  vnu = "22.5.11"
  html5validator = "0.4.2"
  hugo = "v0.100.0"
[syndication]
  mastodon = 108400370094007294
[logbook]
  [logbook.Results]
    [[logbook.Results.notes]]
      timestamp = 2022-06-05T00:00:00-04:00
      note = """
  This website now has zero validation errors! :tada: All the errors
  listed in the [collapsed log below](#org-target--validation-log) are now resolved. See my [Zero HTML Validation Errors!]({{< relref "zero-html-validation-errors" >}}) post on how I did that.
  """
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Using the `.jar`](#using-the-dot-jar)
    - [Using `pip install`](#using-pip-install)
- [Using pre-compiled binary](#using-pre-compiled-binary)
- [Results](#results)
- [Conclusion](#conclusion)

</div>
<!--endtoc-->


I have been using the online HTML5 Validator
<https://html5.validator.nu/> for few years now. I have a link at the
bottom of each post to validate that page's HTML. For an example, see
the _html5 validator_ link at the [bottom](#bottom) of this
post.

But it didn't occur to me until now to look for a way to do the same
validation offline! Offline validation would be useful so that I can
look at any HTML generation problem before I deploy the website. So I
looked for a solution online, and of course it's [answered on
StackOverflow](https://stackoverflow.com/a/26505206/1219634) :smile:.

It turns out that same [Nu HTML5 Validator](https://github.com/validator/validator) project that provides the
online validation service, also provides a Java application as well as
pre-compiled binaries for Linux, Windows and MacOS for offline use!

To use the Java _.jar_ file, you need to have at least Java 8
installed on your system. But you don't need to have any version of
Java installed if you use the pre-compiled binary instead. See [its
documentation](https://validator.github.io/validator/) for more details.


## Using the `.jar` {#using-the-dot-jar}

<div class="note">

Requires at least Java 8

</div>

Download
: Download the latest `vnu.jar` from the project's [GitHub
    Releases section](https://github.com/validator/validator/releases).
    ```shell
    curl -ROLs https://github.com/validator/validator/releases/download/latest/vnu.jar
    ```

Run
: Below command runs the validator only on the HTML files in
    the `public/`
{{% sidenote %}}
    If you are using [Hugo](https://gohugo.io), the `hugo` command will publish the HTML
    files in the `public/` directory by default.
    {{% /sidenote %}} directory. See its [Usage documentation](https://validator.github.io/validator/#usage) for more details.
    ```shell
    java -jar vnu.jar --skip-non-html --errors-only public/
    ```
    For my usecase, if I don't provide the `--skip-non-html
           --errors-only` switches, the output is too noisy.


### Using `pip install` {#using-pip-install}

If you do not want to manually download the `.jar`, there's a Python
wrapper available to do the same for you: [html5validator](https://github.com/svenkreiss/html5validator).

Install
: ```shell
    pip install --user html5validator
    ```

Run
: ```shell
    html5validator --root public/
    ```
    It seems like this Python wrapper implicitly passes `--skip-non-html
           --errors-only` to the Java app. So those are not needed when running
    `html5validator`. But on the flip side, it needs the `--root` switch
    when specifying the directory to run the script on.

Note that you still need to have at least Java 8 installed when
running this Python app too, because it downloads and run the same
`.jar` behind the scenes.


## Using pre-compiled binary {#using-pre-compiled-binary}

If your system doesn't have the required Java version, you can use the
pre-compiled binary instead.

Download &amp; Extract
: Download and extract the `vnu.<OS>.zip` for
    your **OS** from the same _Releases_ section. Here, I am showing how
    to do that on Linux:
    ```shell
    curl -ROLs https://github.com/validator/validator/releases/download/latest/vnu.linux.zip
    unzip vnu.linux.zip
    ```
    The extracted binary path will be `vnu-runtime-image/bin/vnu`.

Run
: The run options will be the exact same; just that you will be
    running the binary directly instead of running through `java`.
    ```shell
    vnu-runtime-image/bin/vnu --skip-non-html --errors-only public/
    ```


## Results {#results}


I was a bit disappointed to see validation errors on my site, but then
it wasn't too bad .. ~~52~~ 46 errors:

Some I already fixed
: These 6 errors were fixed in [this commit](https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io/-/commit/4288a70fa473800b597d46c77bf4021d7c0cd060).
    ```text
    "file:/public/​getting-started-with-texlive/index.html":6.2198-6.2206: error: Element "package" not allowed as child of element "li" in this context. (Suppressing further errors from this subtree.)
    "file:/public/​getting-started-with-texlive/index.html":6.2256-6.2259: error: End tag "li" implied, but there were open elements.
    "file:/public/​getting-started-with-texlive/index.html":6.2198-6.2206: error: Unclosed element "package".
    "file:/public/​getting-started-with-texlive/index.html":6.2307-6.2315: error: Element "package" not allowed as child of element "li" in this context. (Suppressing further errors from this subtree.)
    "file:/public/​getting-started-with-texlive/index.html":6.2316-6.2319: error: End tag "li" implied, but there were open elements.
    "file:/public/​getting-started-with-texlive/index.html":6.2307-6.2315: error: Unclosed element "package".
    ```

Some I can probably fix
: ```text
    "file:/public/notes/​nim/index.html":472.480-472.486: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
    "file:/public/notes/​nim/index.html":1359.221-1359.231: error: Duplicate ID "log".
    ..
    ```

Some need to be ignored
: ```text
    "file:/public/​google4a938eaf9bbacbcd.html":1.1-1.52: error: Non-space characters found without seeing a doctype first. Expected "<!DOCTYPE html>".
    "file:/public/​google4a938eaf9bbacbcd.html":1.1-1.52: error: Element "head" is missing a required instance of child element "title".
    ..
    ```

And the rest would be out of my scope to fix
: ```text
    "file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: Start tag "a" seen but an element of the same type was already open.
    "file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: End tag "a" violates nesting rules.
    "file:/public/​auto-count-100daystooffload-posts/index.html":114.526-114.529: error: Stray end tag "a".
    ..
    ```

Expand the below drawer if you'd like to see the full log with 46
errors: <span class="org-target" id="org-target--validation-log"></span>

<details>
<summary>Output of running <code>html5validator --root public/</code></summary>
<div class="details">

```text
"file:/public/​google4a938eaf9bbacbcd.html":1.1-1.52: error: Non-space characters found without seeing a doctype first. Expected "<!DOCTYPE html>".
"file:/public/​google4a938eaf9bbacbcd.html":1.1-1.52: error: Element "head" is missing a required instance of child element "title".
"file:/public/notes/​plantuml/index.html":114.474-114.678: error: Attribute "title" not allowed on element "a" at this point.
"file:/public/notes/​nim-fmt/index.html":300.34-300.52: error: Duplicate ID "older-issue".
"file:/public/notes/​nim-fmt/index.html":306.20-306.33: error: Duplicate ID "floats".
"file:/public/notes/​nim-fmt/index.html":339.34-339.52: error: Duplicate ID "older-issue".
"file:/public/notes/​nim-fmt/index.html":341.320-341.335: error: Duplicate ID "integers".
"file:/public/notes/​nim-fmt/index.html":341.494-341.507: error: Duplicate ID "floats".
"file:/public/notes/​nim-fmt/index.html":385.34-385.48: error: Duplicate ID "strings".
"file:/public/notes/​string-fns-nim-vs-python/index.html":134.34-134.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":239.34-239.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":269.34-269.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":336.34-336.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":513.106-513.118: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":588.106-588.118: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":636.34-636.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":731.34-731.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":797.34-797.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":846.34-846.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":894.34-894.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":920.34-920.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":942.34-942.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":1012.34-1012.46: error: Duplicate ID "notes".
"file:/public/notes/​string-fns-nim-vs-python/index.html":1074.34-1074.46: error: Duplicate ID "notes".
"file:/public/notes/​nim/index.html":472.480-472.486: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/notes/​nim/index.html":1359.221-1359.231: error: Duplicate ID "log".
"file:/public/notes/​nim/index.html":2364.130-2364.149: error: Duplicate ID "named-tuples".
"file:/public/notes/​nim/index.html":2403.149-2403.172: error: Duplicate ID "anonymous-tuples".
"file:/public/notes/​nim/index.html":3370.284-3370.303: error: Duplicate ID "installation".
"file:/public/notes/​nim/index.html":3410.354-3410.373: error: Duplicate ID "installation".
"file:/public/notes/​nim/index.html":5033.34-5033.52: error: Duplicate ID "older-issue".
"file:/public/notes/​nim/index.html":5376.34-5376.45: error: Duplicate ID "json".
"file:/public/notes/​nim/index.html":6066.64-6066.81: error: Duplicate ID "references".
"file:/public/bits/​plantuml-version/index.html":7.37-7.94: error: An "img" element must have an "alt" attribute, except under certain conditions. For details, consult guidance on providing text alternatives for images.
"file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: Start tag "a" seen but an element of the same type was already open.
"file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: End tag "a" violates nesting rules.
"file:/public/​auto-count-100daystooffload-posts/index.html":114.526-114.529: error: Stray end tag "a".
"file:/public/​generics-not-exactly-in-systemverilog/index.html":118.232-118.238: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/​grep-po/index.html":51.139-51.145: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/​how-do-i-write-org-mode/index.html":23.194-23.200: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/​hugo-use-goat-code-blocks-for-ascii-diagrams/index.html":24.130-24.255: error: An "img" element must have an "alt" attribute, except under certain conditions. For details, consult guidance on providing text alternatives for images.
"file:/public/​hugo-modules-getting-started/index.html":6.865-6.871: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/​using-emacs-advice-to-silence-messages-from-functions/index.html":151.687-151.732: error: Start tag "a" seen but an element of the same type was already open.
"file:/public/​using-emacs-advice-to-silence-messages-from-functions/index.html":151.748-151.751: error: Stray end tag "a".
"file:/public/page/6/index.html":29.39-29.54: error: Duplicate ID "fnref:1".
"file:/public/page/6/index.html":49.169-49.184: error: Duplicate ID "fnref:1".
```
</div>
</details>


## Conclusion {#conclusion}

It was really easy to download the run the `vnu` application using
Java, the standalone Linux binary and also through the
`html5validator` Python wrapper.

After my quick trials, I think I will use the `html5validator`
approach more because,

1.  It works as I expect will the least number of switches.
2.  I am able to redirect the output using `html5validator --root
          public/ > validate.log`. _I tried the same using the `vnu.jar` and
    Linux compiled `vnu` binary, but the error log redirection didn't
    work with those._

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
