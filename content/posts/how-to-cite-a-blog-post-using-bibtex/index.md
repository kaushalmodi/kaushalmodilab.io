+++
title = "How to cite a blog post using BibTeX?"
author = ["Kaushal Modi"]
draft = true
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  twitter = ""
+++

<div class="ox-hugo-toc toc">

<div class="heading">

Table of Contents

</div>

-   [Braces and Quotes](#braces-and-quotes)
-   [Using `@online`](#using-online)
    -   [CMS (Chicago Manual Style -- Pandoc
        default)](#cms--chicago-manual-style-pandoc-default)
    -   [APA](#apa)
-   [Using `@misc`](#using-misc)
    -   [CMS (Chicago Manual Style -- Pandoc
        default)](#cms--chicago-manual-style-pandoc-default)
    -   [APA](#apa)
-   [CMS](#cms)
    -   [CMS -- Turabian](#cms-turabian)
-   [Links](#links)

</div>

<!--endtoc-->

## Braces and Quotes

``` text
title = "{Wikipedia:Citing Wikipedia}",
```

Above retains the casing of "W", "C" and the second "W".

Below do not:

-   Only quotes `text     title = "Wikipedia:Citing Wikipedia",`
-   Only braces `text     title = {Wikipedia:Citing Wikipedia},`

## Using `@online`

### CMS (Chicago Manual Style -- Pandoc default) {#cms--chicago-manual-style-pandoc-default}

``` text
<author: last, first>. <1@year: yyyy>. "<title>." <type>. <journal>. <2@year: yyyy>. <url>.
```

-   If `date` is specified in "yyyy-mm" format instead of `year` ("yyyy"
    format):
    -   `<1@year: yyyy>` above will be replaced with `<date: yyyy>`.
    -   `<2@year: yyyy>` above will be replaced with
        `<date: month yyyy>`.

### APA

``` text
<author: last, f>. (<year: yyyy>). <title> [<type>]. Retrieved <urldate: month dd, yyyy>, from <url>
```

-   If `date` is specified in "yyyy-mm" format instead of `year` ("yyyy"
    format):
    -   `<year: yyyy>` above will be replaced with
        `<date: yyyy, month>`.
-   Does **not** include `journal` (which one would use to save the blog
    name).

## Using `@misc`

### CMS (Chicago Manual Style -- Pandoc default) {#cms--chicago-manual-style-pandoc-default}

``` text
<author: last, first>. <year: yyyy>. "<title>." <type>. <italicized journal>. <url>.
```

-   If `date` is specified in "yyyy-mm" format instead of `year` ("yyyy"
    format):
    -   `<year: yyyy>` above will be replaced with `<date: yyyy>`.
    -   So the "month" portion in `date` is ignored.

### APA {#apa}

``` text
<author>. (<year: yyyy>). <title>. <italicized journal>. <type>. Retrieved from <url>
```

## CMS

-   If the same `author` is repeated, the full author name is replaced
    with "---------" for repeated instances.
-   This style does not put square brackets around the `type` field
    values (as APA does). So they are put in manually in the `.bib`
    file. This applies to *CMS--Turabian* style too.

### CMS -- Turabian

-   Duplicates the references in the "References" section **and**
    Footnotes.

## Links

-   <https://libguides.nps.edu/citation>
    -   *Chicago Manual of Styles*
        -   <https://libguides.nps.edu/citation/chicagonb>
            -   <https://libguides.nps.edu/citation/chicagonb#blog>
        -   <https://libguides.nps.edu/citation/chicagoad>
            -   <https://libguides.nps.edu/citation/chicagoad#blog>
    -   <https://libguides.nps.edu/citation/ieeebibtex>
-   Visit <https://github.com/citation-style-language/styles> and press
    `t` to bring up file search.
-   <https://tex.stackexchange.com/questions/3587/how-can-i-use-bibtex-to-cite-a-web-page>
-   <http://libanswers.snhu.edu/faq/190823>
-   <http://blog.apastyle.org/apastyle/2016/04/how-to-cite-a-blog-post-in-apa-style.html>
-   <https://tex.stackexchange.com/questions/411440/how-cite-a-website-with-bibtex>
-   <http://blogs.plos.org/mfenner/2011/07/21/how-to-formally-cite-a-blog-post/>
-   <https://www.mendeley.com/guides/web-citation-guide>
-   <https://en.wikipedia.org/wiki/Wikipedia:Citing_Wikipedia#BibTeX_entry>
-   <https://tex.stackexchange.com/a/124576/52678> -- Schroeder
    ([2012](#ref-schroeder2012))

## References {#references}

<div id="refs" class="references csl-bib-body hanging-indent">

<div id="ref-beyondPDF" class="csl-entry">

Fenner, Martin. 2011. "Beyond the PDF ... Is ePub." Blog.
*Gobbledygook*.
[http://blogs.plos.org/mfenner/2011/01/23/beyond-the-pdf-...-is-epub/](http://blogs.plos.org/mfenner/2011/01/23/beyond-the-pdf-…-is-epub/){.uri}.

</div>

<div id="ref-electronicWebDate" class="csl-entry">

"Raytheon Fires Four TALON Laser-Guided Rockets from MD 530g
Helicopter." 2014. Raytheon. 2014.
<http://raytheon.mediaroom.com/index.php?s=43&item=2624>.

</div>

<div id="ref-schroeder2012" class="csl-entry">

Schroeder, Stan. 2012. "Facebook Hits One Billion Active Users."
Mashable. October 2012.
<http://mashable.com/2012/10/04/facebook-one-billion/>.

</div>

<div id="ref-electronicWebAuthorDate" class="csl-entry">

Vergun, D. 2014. "Smarter Robots Partnering with Soldiers." 2014.
<http://www.army.mil/article/131847/Smarter_ground_robots_partnering_with_Soldiers/>.

</div>

<div id="ref-linkOnline" class="csl-entry">

Wikipedia contributors. 2004. "<span class="nocase">Wikipedia:Citing
Wikipedia @online</span>." Website. Gobbledygook. 2004.
<https://en.wikipedia.org/wiki/Wikipedia:Citing_Wikipedia#BibTeX_entry>.

</div>

<div id="ref-linkMisc" class="csl-entry">

---------. 2005. "Wikipedia:Citing Wikipedia." Website. *Gobbledygook*.
<https://en.wikipedia.org/wiki/Wikipedia:Citing_Wikipedia#BibTeX_entry>.

</div>

</div>
