+++
title = "Using Git Delta with Magit"
author = ["Kaushal Modi"]
description = """
  _Git Delta_ is a command line utility that beautifies git diffs in the
  terminal. But did you know that it can do the same in Magit too?
  """
date = 2022-07-06T22:04:00-04:00
lastmod = 2022-07-06T22:04:00-04:00
tags = ["git", "100DaysToOffload", "magit", "diff"]
categories = ["emacs"]
draft = false
creator = "Emacs 28.1.90 (Org mode 9.5.4 + ox-hugo)"
[versions]
  delta = "0.13.0"
  git = "2.37.0"
  emacs = "28.1.90"
[syndication]
  mastodon = 108606184219234504
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Installing `delta`](#installing-delta)
- [Installing `magit-delta`](#installing-magit-delta)
- [Configuring `delta`](#configuring-delta)

</div>
<!--endtoc-->


[Delta](https://github.com/dandavison/delta) is a highly configurable
{{% sidenote %}}
I am not kidding. Check out the output of [`delta --help`](https://dandavison.github.io/delta/full---help-output.html).
{{% /sidenote %}} command line utility that makes the git diffs look better, while also
syntax-highlighting
{{% sidenote %}}
[Difftastic](https://github.com/Wilfred/difftastic/) is another popular diff tool which compares files based on
their syntax. I like reviewing _git diffs_ from within Emacs
(Magit). But _difftastic_ [does not support Magit](https://github.com/Wilfred/difftastic/issues/251).
{{% /sidenote %}} the code in the diffs.

When I first heard of "syntax highlighted diffs", I wasn't sure what
that meant. If you are in the same boat, here's a screenshot that
shows that.

<a id="figure--git-delta-example"></a>

{{< figure src="delta-example.png" caption="<span class=\"figure-number\">Figure 1: </span>Example of how `delta` renders a _git diff_ for an `ox-hugo` commit" link="delta-example.png" >}}

But I do most of my git operations including viewing of diffs from
within Emacs, using [Magit](https://magit.vc).

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.. and thankfully _delta_ [works with Magit](https://dandavison.github.io/delta/using-delta-with-magit.html)!<br />

</div>

Below screenshot shows how the same diff looks like in Magit.

<a id="figure--git-magit-delta-example"></a>

{{< figure src="magit-delta-example.png" caption="<span class=\"figure-number\">Figure 2: </span>Example of how `magit-delta` renders a _git diff_ for an `ox-hugo` commit" link="magit-delta-example.png" >}}

The [`magit-delta`](https://github.com/dandavison/magit-delta) Emacs package makes this possible, which is also
developed by the `delta` author Dan Davison.

Caveat
: If the line numbers are enabled in `delta`, they mess up
    the interactive expanding and collapsing of diffs in Magit. See
    [Magit Delta Issue # 13](https://github.com/dandavison/magit-delta/issues/13#issuecomment-949820122) for more details.

Now, I am alright with not seeing the line numbers in Magit. But I
really liked to see the line numbers in the side-by-side view in the
terminal. Luckily, if disabled the `line-numbers` feature but enabled
the `side-by-side` view, I got what I wanted!

1.  Line numbers are disabled in Magit and expanding/collapsing of
    diffs works correctly. _I am also really glad that I don't see the
    side-by-side view in Magit diffs even when I enable that feature in
    `delta`, because I like to have my Emacs buffers only about 90
    characters wide._
2.  Line numbers **and** side-by-side view are enabled in the terminal.

I'll end this post with pointers to installing `delta` and
`magit-delta` and how to configure them.


## Installing `delta` {#installing-delta}

You can install `delta` (it's called `git-delta` in some package
managers) using one of the methods listed [in its manual](https://dandavison.github.io/delta/installation.html), or you can
download → extract its statically compiled binary for your OS from its
[GitHub Releases](https://github.com/dandavison/delta/releases) page.


## Installing `magit-delta` {#installing-magit-delta}

Once you put this snippet in your Emacs config and evaluate it, it
will install this package and enable the `magit-delta-mode` in the
Magit buffers.

<a id="code-snippet--enabling-magit-delta"></a>
```emacs-lisp
(use-package magit-delta
  :ensure t
  :hook (magit-mode . magit-delta-mode))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--enabling-magit-delta">Code Snippet 1</a>:</span>
  Installing and enabling <code>magit-delta</code> using <code>use-package</code>
</div>


## Configuring `delta` {#configuring-delta}

Here's a snippet for `delta` configuration from my `.gitconfig`. It's
mostly the same as the one in _delta_'s the [Getting Started](https://github.com/dandavison/delta#get-started) guide. The
main difference in my workaround for the `magit-delta` issue.

<a id="code-snippet--delta-gitconfig"></a>
```sh
[core]
    pager = delta

[interactive]
    diffFilter = delta --color-only
[add.interactive]
    useBuiltin = false # required for git 2.37.0

[diff]
    colorMoved = default

[delta]
    # https://github.com/dandavison/magit-delta/issues/13
    # line-numbers = true    # Don't do this.. messes up diffs in magit
    #
    side-by-side = true      # Display a side-by-side diff view instead of the traditional view
    # navigate = true          # Activate diff navigation: use n to jump forwards and N to jump backwards
    relative-paths = true    # Output all file paths relative to the current directory
    file-style = yellow
    hunk-header-style = line-number syntax
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--delta-gitconfig">Code Snippet 2</a>:</span>
  My configuration for <code>delta</code> in <code>.gitconfig</code>
</div>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
