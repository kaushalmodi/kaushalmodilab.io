+++
title = """
  Improving ox-hugo exported Org "info:" links
  """
author = ["Kaushal Modi"]
description = """
  In my previous post, I talked about how `info:` Org link export
  support got added to `ox-hugo`. This post is about making those
  exported links a tiny :pinching_hand: bit better.
  """
date = 2022-04-15T22:50:00-04:00
tags = ["100DaysToOffload", "info", "ox-hugo"]
categories = ["web", "hugo", "emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
  org = "release_9.5-532-gf6813d"
[syndication]
  mastodon = 108139531068430378
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [:flexed_biceps:1: Exported links now point to separate pages in the manual](#flexed-biceps-1-exported-links-now-point-to-separate-pages-in-the-manual)
- [:flexed_biceps: 2: Export Org manual links to `orgmode.org` URLs](#flexed-biceps-2-export-org-manual-links-to-orgmode-dot-org-urls)
- [:flexed_biceps: 3: Link hover text shows the _elisp_ code for accessing the Info node](#flexed-biceps-3-link-hover-text-shows-the-elisp-code-for-accessing-the-info-node)
- [Final code in `ox-hugo` that exports the `info:` links](#final-code-in-ox-hugo-that-exports-the-info-links)

</div>
<!--endtoc-->

After writing my [previous post]({{< relref "linking-and-exporting-org-info-links" >}}), I had shared its link on the Org mode
mailing list
{{% sidenote %}}
For folks who don't know, you can email the Org mode mailing list
(`emacs-orgmode@gnu.org`) about pretty much anything Org mode related:
any help you want, a bug you want to report, a patch you want to
submit, or just share your positive or negative experiences with Org
mode or thank someone for their awesome work.
{{% /sidenote %}} [here](https://lists.gnu.org/r/emacs-orgmode/2022-04/msg00162.html), and received some helpful feedback from Max Nikulin.

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;_Thank you Max!_<br />

</div>

This post is a response to his feedback, and it also describes a few
improvements made for `info:` link exports in `ox-hugo` based on that.

<div class="note">

The commits for this `info:` link export improvements can be found in
`ox-hugo` [PR # 620](https://github.com/kaushalmodi/ox-hugo/pull/620).

</div>


## :flexed_biceps:[^fn:1] 1: Exported links now point to separate pages in the manual {#flexed-biceps-1-exported-links-now-point-to-separate-pages-in-the-manual}

Feedback
: ```text
    For <info:emacs#Browse-URL> export to html produces the following link:
        https://www.gnu.org/software/emacs/manual/html_mono/emacs.html#Browse_002dURL
    I think, a better variant is
        https://www.gnu.org/software/emacs/manual/html_node/emacs/Browse_002dURL.html
    even though for the Org manual I often prefer single-page HTML version.
    ```

I really liked this suggestion!

-   Earlier, the Info node references were exporting to an <span class="underline">anchor</span> on a
    huge single-page HTML manual e.g. `emacs.html#Browse_002dURL`
-   After implementing this fix, the exported link points to a <span class="underline">separate
    HTML page</span> for that node e.g. `emacs/Browse_002dURL.html`.

Now, {{< highlight org "hl_inline=true" >}}[[info:emacs#Screen]]{{< /highlight >}} exports and renders to
[Emacs Info: Screen](https://www.gnu.org/software/emacs/manual/html_node/emacs/Screen.html "Emacs Lisp: (info \"(emacs) Screen\")"), and a Top level node like {{< highlight org "hl_inline=true" >}}[[info:emacs#Top]]{{< /highlight >}} exports and renders to [Emacs Info](https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html "Emacs Lisp: (info \"(emacs) Top\")").

For consistency, all `info:` links will now be exported to URLs
pointing to the separate node HTML pages, even for Org manual links.


## :flexed_biceps: 2: Export Org manual links to `orgmode.org` URLs {#flexed-biceps-2-export-org-manual-links-to-orgmode-dot-org-urls}

Feedback
: (About an example `info:` link pointing to Org manual:
    {{< highlight org "hl_inline=true" >}}[[info:org#Top]]{{< /highlight >}})
    ```text
    And the link target ideally should be https://orgmode.org/manual/index.html
    ```

This was another great suggestion!

By default, the Org manual `info:` links will export to URLs pointing
to the [Org manual shipped with Emacs](https://www.gnu.org/software/emacs/manual/html_node/org/index.html). That manual will be associated
with the Org mode version that shipped with the last stable version of
Emacs. _So depending on how long it has been since the last Emacs
release, you could be referring to quite an older version of the Org
manual._

If you install Org from GNU Elpa, you can get an
<mark>update of the latest stable Org version every week</mark> !
{{% sidenote %}}
There's a related post on [Org mode's Release Flow]({{< relref "org-contribution-flowchart" >}}) that might interest
you.
{{% /sidenote %}} And the manual hosted on <https://orgmode.org> gets updated every week
as well. I would like to always refer to the latest Org manual with
the latest fixes and documentation improvements, and so I agree with
Max's suggestion above.

Now {{< highlight org "hl_inline=true" >}}[[info:org#Top]]{{< /highlight >}} exports and renders as
[Org Info](https://orgmode.org/manual/index.html "Emacs Lisp: (info \"(org) Top\")") and the Org manual nodes like {{< highlight org "hl_inline=true" >}}[[info:org#Working with Source Code]]{{< /highlight >}} also export as a link to a
page in that manual: <span class="org-target" id="org-target--improving-info-link-exports--last-link"></span>
[Org Info: Working with Source Code](https://orgmode.org/manual/Working-with-Source-Code.html "Emacs Lisp: (info \"(org) Working with Source Code\")").


## :flexed_biceps: 3: Link hover text shows the _elisp_ code for accessing the Info node {#flexed-biceps-3-link-hover-text-shows-the-elisp-code-for-accessing-the-info-node}

Feedback
: ```text
    I would prefer
        info "(org) Top"
    to
        Org Info: Top
    since the former may be pasted to M-x : or to shell command prompt.
    ```

I wanted the link descriptions in the exported markdown to be more
"English" and understandable to people not familiar with the Emacs
Info interface. So I decided to keep this _mostly_ unchanged ..

I did not change the link _description_, but I did add a new HTML
**title** attribute to the link. This attribute contains the Emacs Lisp
code needed to access the Info manual from Emacs. The user will see
the text from this attribute when they hover the mouse over the
links. If you haven't already discovered this feature, try hovering
the mouse over [that last link above](#org-target--improving-info-link-exports--last-link) and you should see this :smiley: :

<a id="figure--info-link-title-attribute"></a>

{{< figure src="info-link-title-attribute.png" caption="<span class=\"figure-number\">Figure 1: </span>Hovering the mouse over the exported `info:` links will show the Emacs Lisp code for accessing the same node from Emacs" >}}

Of course, the user will still not be able to copy that text and paste
in Emacs. But it will still provide them enough hint on how to access
Info nodes in Emacs.


## Final code in `ox-hugo` that exports the `info:` links {#final-code-in-ox-hugo-that-exports-the-info-links}

Here's the version of the `org-hugo--org-info-export` function that
handles the exports of `info:` links as of today (<span class="timestamp-wrapper"><span class="timestamp">&lt;2022-04-15 Fri&gt;</span></span>):

<details title="Click to expand">
<summary><code>org-hugo--org-info-export</code> function from <code>ox-hugo</code></summary>
<div class="details">

<a id="code-snippet--org-hugo--org-info-export"></a>
```emacs-lisp { linenos=true, linenostart=888 }
(defun org-hugo--org-info-export (path desc format)
  "Add support for exporting [[info:..]] links for `hugo' format."
  (let* ((parts (split-string path "#\\|::"))
         (manual (car parts))
         (node (or (nth 1 parts) "Top"))
         (title (format "Emacs Lisp: (info \\\"(%s) %s\\\")" manual node))
         (desc (or desc
                   (if (string= node "Top")
                       (format "%s Info" (capitalize manual))
                     (format "%s Info: %s" (capitalize manual) node))))
         ;; `link' below is mostly derived from the code in
         ;; `org-info-map-html-url'.
         (link (cond ((member manual org-info-emacs-documents)
                      (let ((manual-url (if (string= (downcase manual) "org")
                                            "https://orgmode.org/manual"
                                          (format "https://www.gnu.org/software/emacs/manual/html_node/%s" manual)))
                            (node-url (if (string= node "Top")
                                          "index.html"
                                        (concat (org-info--expand-node-name node) ".html"))))
                        (format "%s/%s" manual-url node-url)))
                     ((cdr (assoc manual org-info-other-documents)))
                     (t
                      (concat manual ".html")))))
    (when (member format '(md hugo))
      (format "[%s](%s \"%s\")" desc link title))))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--org-hugo--org-info-export">Code Snippet 1</a>:</span>
  <code>ox-hugo</code>'s version of <code>org-info-export</code> (<code>ol-info.el</code>) function
</div>
</div>
</details>

[Link to code in the repo](https://github.com/kaushalmodi/ox-hugo/blob/5b3a0d8a7da49f602785aa20486bbbbeb35ebb36/ox-hugo.el#L888-L912)

I will leave it as an exercise for the reader to map the lines in
above code snippet to the features described in this post
:four_leaf_clover:.

[^fn:1]: Just for giggles, I am using :flexed_biceps: as a replacement
    for "Improvement"

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
