+++
title = "Straight and Curved Quotes in Emacs Lisp"
author = ["Kaushal Modi"]
description = """
  A short guide for getting the single quotes rendered as expected
  (straight or curved) in Emacs Lisp function and variable documentation
  strings and `message` outputs.
  """
date = 2022-04-10T10:05:00-04:00
tags = ["100DaysToOffload", "single-quotes", "grave-accents"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108108267906289393
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Emacs 25 -- Curved quotes rendering feature](#emacs-25-curved-quotes-rendering-feature)
- [Preventing curved quotes in documentation strings](#preventing-curved-quotes-in-documentation-strings)
- [Preventing curved quotes in messages](#preventing-curved-quotes-in-messages)
- [Summary](#summary)
- [References](#references)

</div>
<!--endtoc-->

This post is related to the curved quotes feature from Emacs 25
(September 2016), but I got inspired to write this as I was reading
the NEWS file of the recently released Emacs 28.1 :sparkles: .. [this
specific part related to the `shortdoc` library](https://git.savannah.gnu.org/cgit/emacs.git/tree/etc/NEWS?h=emacs-28&id=886339747b8d34fc09fd69a143cf548daf92dce6#n219). Turns out that I had
never used `shortdoc`
{{% sidenote %}}
If you also haven't used this library before, it's basically a "cheat
sheet generator" or an aggregator of related functions grouped
together with code examples and their outputs -- for example,
functions related to string manipulation, buffer operations, etc. It
is like <https://tldr.sh/> but for Emacs Lisp functions.
{{% /sidenote %}} . So as I was reading through the help for various user-facing
`shortdoc` functions, `C-h f shortdoc-add-function` led me to
this:

<a id="figure--elisp-curved-quotes--docstring-unexpected-curved-quotes"></a>

{{< figure src="shortdoc-add-function-curved-quotes-at-wrong-place.png" caption="<span class=\"figure-number\">Figure 1: </span>Notice the curved closing single quotations in the Emacs Lisp example in that doc string" link="shortdoc-add-function-curved-quotes-at-wrong-place.png" >}}

We don't have curved quotes in Emacs Lisp syntax :face_with_monocle:
.. So I look up the [source of `shortdoc-add-function`](https://git.savannah.gnu.org/cgit/emacs.git/tree/lisp/emacs-lisp/shortdoc.el?h=emacs-28&id=98abf01fd681931f8870569ff559b547579d7cef#n1349):

<a id="code-snippet--elisp-curved-quotes--docstring-unescaped-single-quotes"></a>
```emacs-lisp { linenos=true, linenostart=1349, hl_lines=["9-10"] }
(defun shortdoc-add-function (group section elem)
  "Add ELEM to shortdoc GROUP in SECTION.
If GROUP doesn't exist, it will be created.
If SECTION doesn't exist, it will be added.

Example:

  (shortdoc-add-function
    'file \"Predicates\"
    '(file-locked-p :no-eval (file-locked-p \"/tmp\")))"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--docstring-unescaped-single-quotes">Code Snippet 1</a>:</span>
  Unescaped single quotes in Emacs Lisp documentation strings
</div>

The documentation looks correct at first glance, but there's a minor
problem. This post talks about how to fix that documentation and then
a bit more on how to deal with auto-conversion to curved quotes.


## Emacs 25 -- Curved quotes rendering feature {#emacs-25-curved-quotes-rendering-feature}

Emacs 25 had landed with one of my favorite new features --- the
auto-rendering of grave accents (`` ` ``) and straight quotes (`'`​) to
curved (or curly) quotes (`‘`, `’`). From [its `NEWS` file](https://git.savannah.gnu.org/cgit/emacs.git/tree/etc/NEWS?h=emacs-25#n1284):

> New variable 'text-quoting-style' to control how Emacs translates
> quotes.  Set it to 'curve' for curved single quotes, to 'straight' for
> straight apostrophes, and to 'grave' for grave accent and apostrophe.
> The default value nil acts like 'curve' if curved single quotes are
> displayable, and like 'grave' otherwise.  The new variable affects
> display of diagnostics and help, but not of info.  As the variable is
> not intended for casual use, it is not a user option.

I like the rendering to curved quotes because it improves the
readability of documentation strings in the &lowast;Help&lowast; buffers (`C-h v`,
`C-h f`, etc.), and they look great in &lowast;Messages&lowast; buffer. It helps
differentiate between typographical use of quotes like in ‘Hello’
versus the straight quotes used in Emacs Lisp syntax like
{{< highlight emacs-lisp "hl_inline=true" >}}(setq foo 'some-symbol){{< /highlight >}} or the usage of
grave accents like {{< highlight emacs-lisp "hl_inline=true" >}}(setq foo `(,(+ 1 1))){{< /highlight >}}.

This feature renders grave accents (`` ` ``) to `‘` and straight quotes
(`'`​) to `’` by default. But there would be times when you would want
to literally print grave accents or straight quotes in &lowast;Help&lowast; and
&lowast;Messages&lowast; buffers.


## Preventing curved quotes in documentation strings {#preventing-curved-quotes-in-documentation-strings}

In documentation strings, some times you would want to show literal
straight quotes or grave accents, for instance, when showing some
example Emacs Lisp code snippets in there. [Above image](#figure--elisp-curved-quotes--docstring-unexpected-curved-quotes) shows one of
such examples.

More than the visual inaccuracy of seeing curved quoted where straight
quotes should be,
<mark>if someone copies that code to try it out, it will
not work</mark> !

This should be fixed by escaping the `'` characters.

<div class="note">

Emacs Lisp uses a special escaping sequence in documentation strings:
`\\=`.

-   Escape `` ` `` using `` \\=` ``.
-   Escape `'` using `\\='`.

</div>

Below we see
[Code Snippet 1](#code-snippet--elisp-curved-quotes--docstring-unescaped-single-quotes) after
this fix:

<a id="code-snippet--elisp-curved-quotes--docstring-escaped-single-quotes"></a>
```emacs-lisp { linenos=true, linenostart=1349, hl_lines=["9-10"] }
(defun shortdoc-add-function (group section elem)
  "Add ELEM to shortdoc GROUP in SECTION.
If GROUP doesn't exist, it will be created.
If SECTION doesn't exist, it will be added.

Example:

  (shortdoc-add-function
    \\='file \"Predicates\"
    \\='(file-locked-p :no-eval (file-locked-p \"/tmp\")))"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--docstring-escaped-single-quotes">Code Snippet 2</a>:</span>
  Correctly escaped single quotes in Emacs Lisp documentation strings
</div>

.. and now the straight quotes get rendered as straight quotes:

<a id="figure--elisp-curved-quotes--docstring-escaped-single-quotes"></a>

{{< figure src="shortdoc-add-function-single-quotes-correctly-escaped.png" caption="<span class=\"figure-number\">Figure 2: </span>Correctly escaped single quotes in documentation strings" link="shortdoc-add-function-single-quotes-correctly-escaped.png" >}}


## Preventing curved quotes in messages {#preventing-curved-quotes-in-messages}

The same auto-rendering of curved quotes happens in the messages that
get printed to the &lowast;Messages&lowast; buffer and Echo Area as well. So the
below behavior where **‘Hi’** gets printed is expected and nice:

<a id="code-snippet--elisp-curved-quotes--message-lsqm-hi-rsqm"></a>
```emacs-lisp
(message "`Hi'")      ;=> "‘Hi’"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--message-lsqm-hi-rsqm">Code Snippet 3</a>:</span>
  <code>message</code> auto-converting <code>`Hi'</code> to <code>‘Hi’</code>
</div>

But if someone does below, the rendered **’Hi’** looks pretty odd:

<a id="code-snippet--elisp-curved-quotes--message-rsqm-hi-rsqm"></a>
```emacs-lisp
(message "'Hi'")      ;=> "’Hi’"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--message-rsqm-hi-rsqm">Code Snippet 4</a>:</span>
  <code>message</code> auto-converting <code>'Hi'</code> to <code>’Hi’</code> :open_mouth:
</div>

I had come across this surprise rendering in Emacs 25 and had raised
[this issue](https://lists.gnu.org/archive/html/bug-gnu-emacs/2015-10/msg00234.html) then. From Paul Eggert's reply, I learned that to prevent
this curved quote conversion, we should make the `message` function
print the string processed by the `format` function and not the string
directly. You could do this ..

<a id="code-snippet--elisp-curved-quotes--fix-message-format"></a>
```emacs-lisp
(message "%s" (format "%s" "'Hi'")) ;=>"'Hi'"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--fix-message-format">Code Snippet 5</a>:</span>
  Passing <code>format</code> formatted string to <code>message</code>
</div>

But below will do the same thing and is much easier to type:

<a id="code-snippet--elisp-curved-quotes--fix-message-format-indirect"></a>
```emacs-lisp
(message "%s" "'Hi'") ;=> "'Hi'"
(message "%s" "`Hi'") ;=> "`Hi'"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--elisp-curved-quotes--fix-message-format-indirect">Code Snippet 6</a>:</span>
  Implicit passing of a string to <code>format</code> for formatting, before getting printed by <code>message</code>
</div>


## Summary {#summary}

<a id="table--elisp-curved-quotes--summary"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--elisp-curved-quotes--summary">Table 1</a>:</span>
  Summary of techniques for enforcing straight quotes and grave accents in printed messages and documentation strings
</div>

| Description                 | Solution                                | Outcome      |
|-----------------------------|-----------------------------------------|--------------|
| Render `'` in docstring     | _doc string snippet_: `"foo \\='bar"`   | `foo 'bar`   |
| Render `` ` `` in docstring | _doc string snippet_: ``"foo \\=`bar"`` | ``foo `bar`` |
| Print `'` as `'`            | `(message "%s" "'foo")`                 | `'foo`       |
| Print `` ` `` as `` ` ``    | ``(message "%s" "`foo")``               | `` `foo ``   |

By the way, I submitted a patch for fixing the escaping of straight
quotes in `shortdoc-add-function` documentation string
{{% sidenote %}}
I planned to fix just this straight quote escaping issue, but then I
also ended up slightly improving the documentation of the `(FUNC :eval
EVAL)` and other forms used for adding a function's documentation to
`shortdoc`.
{{% /sidenote %}} in [debbugs # 54782](https://debbugs.gnu.org/cgi/bugreport.cgi?bug=54782) and now it's [in the emacs _master_ branch](https://git.savannah.gnu.org/cgit/emacs.git/commit/?id=cca47ae555bfddf87b4871988555738c335f8457)! :tada:


## References {#references}

Fixing curved quotes conversion in documentation strings
: -   [Elisp Info: Text Quoting Style](https://www.gnu.org/software/emacs/manual/html_node/elisp/Text-Quoting-Style.html "Emacs Lisp: (info \"(elisp) Text Quoting Style\")")

Fixing curved quotes conversion in printed messages
: -   [Elisp Info: Formatting Strings](https://www.gnu.org/software/emacs/manual/html_node/elisp/Formatting-Strings.html "Emacs Lisp: (info \"(elisp) Formatting Strings\")")

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
