+++
title = "Binding Nim to C++ std::list"
author = ["Kaushal Modi"]
description = """
  How to use the Nim `importcpp` pragma to bind to C++ Standard Template
  Libraries like `std::list`.
  """
date = 2022-03-14T01:49:00-04:00
tags = ["nim", "binding", "cpp", "stl", "list", "100DaysToOffload"]
categories = ["programming"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  nim = "1.7.1"
[syndication]
  mastodon = 107953341579339229
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Goal: Be able to easily access C++ `std::list` elements](#nim-cpp-bindings-goal)
- [`importcpp` pragma](#importcpp-pragma)
- [Bindings to the C++ `<list>` header](#bindings-to-the-c-plus-plus-list-header)
- [Test C++ code](#test-c-plus-plus-code)
- [Test Nim code](#test-nim-code)
- [Extending the `std::list` API in Nim](#extending-the-std-list-api-in-nim)
- [Final Code](#final-code)
- [Summary](#summary)
- [References](#references)

</div>
<!--endtoc-->

I like :heart: using [Nim](https://nim-lang.org/) as my go-to programming language -- be it for
scripting, some CLI tool or any [other application](/tags/nim). The nice thing when
using Nim is that you don't have to throw away any of the past work
done in C or C++, because it is pretty easy to bind those C/C++
libraries and extend their functionality in Nim.

To demonstrate that extensibility, in this post, I will add some new
API in Nim that builds on top of the `std::list` <abbr aria-label="Standard Template Library" tabindex=0>STL</abbr>
{{% sidenote %}}
The C++ STL is a set of template classes to provide common programming
data structures and functions such as lists, stacks, arrays, etc.
{{% /sidenote %}} library. I won't be re-writing any of the `std::list` methods in Nim
--- No need to re-invent the `std::list` wheel in Nim! --- I will
directly use the C++ `std::list` methods to build new API functions in
Nim. :exploding_head:


## Goal: Be able to easily access C++ `std::list` elements {#nim-cpp-bindings-goal}

From the [_std::list_ reference](https://en.cppreference.com/w/cpp/container/list), we can see that we cannot access a
_list_ element by index using syntax like `list_obj[index]`. At the
end of this post, you will be able to convert C++ _list_ objects to
Nim _sequences_ and make the _list_ data super-accessible --- You can
print them using `echo`, use built-in libraries like [`sequtils`](https://nim-lang.github.io/Nim/sequtils), and
more.


## `importcpp` pragma {#importcpp-pragma}

The first thing we'll need is to be able to call the C++ _list_
methods directly in Nim, and we will need the `importcpp` pragma
{{% sidenote %}}
This post will highlight some key features of this pragma. For full
details, check out this section in the Nim Manual [linked](#ref-importcpp) in
References.
{{% /sidenote %}} for that. This pragma is used to import a C++ _method_ or _type_ into
the Nim space.

This pragma is typically used like this:

<a id="code-snippet--nim-bindings-importcpp"></a>
```nim
{.link: "/path/to/libCppCode.so".}

type
  MyNimType {.importcpp: "MyCppType", header: "libCppCode.hpp".} = object

proc myNimProc(): MyNimType {.importcpp: "myCppMethod", header: "libCppCode.hpp".}

var
  foo = MyNimProc()
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-importcpp">Code Snippet 1</a>:</span>
  General use of <code>importcpp</code> pragma
</div>

-   `MyCppType` type from the C++ library is mapped to `MyNimType`
{{% sidenote %}}
    You would typically name the type and method identifiers same same
    when binding external libraries to Nim, just to prevent any
    confusion. You can name them differently if you want to though.
    {{% /sidenote %}} in the Nim code.
-   `myCppMethod` from C++ is mapped to `myNimProc`.
-   Then the code can call that `myNimProc` in the Nim code along with
    other Nim stuff.
-   The [`link` pragma](https://nim-lang.github.io/Nim/manual.html#noalias-pragma-link-pragma) is needed if we need to tell the Nim compiler
    where to find the implementation
{{% sidenote %}}
    For the case of linking to the C++ STL `<list>` header, we do not
    need to provide the `std::list` implementation because the GNU GCC
    compiler provides that.
    {{% /sidenote %}} of linked methods.


## Bindings to the C++ `<list>` header {#bindings-to-the-c-plus-plus-list-header}

Below code snippet shows us all the `<list>` header bindings we will
need to meet the [goal](#nim-cpp-bindings-goal) of this post.

<a id="code-snippet--nim-std-list-bindings"></a>
```nim { linenos=true, anchorlinenos=true, lineanchors=org-coderef--045394 }
type
  List*[T]                              {.importcpp: "std::list", header: "<list>".} = object
  ListIter*[T]                          {.importcpp: "std::list<'0>::iterator", header: "<list>".} = object

proc initList*[T](): List[T]            {.importcpp: "std::list<'*0>()", constructor, header: "<list>".}
proc size*(l: List): csize_t            {.importcpp: "size", header: "<list>".}
proc begin*[T](l: List[T]): ListIter[T] {.importcpp: "begin", header: "<list>".}
proc `[]`*[T](it: ListIter[T]): T       {.importcpp: "*#", header: "<list>".}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-std-list-bindings">Code Snippet 2</a>:</span>
  Nim bindings for basic <code>std::list</code> object types and methods
</div>

-   [Line 2](#org-coderef--045394-2) binds the C++ `std::list` type with the Nim
    `List` type. So, a {{< highlight nim "hl_inline=true" >}}List[cint]{{< /highlight >}} type in Nim
    get mapped to {{< highlight cpp "hl_inline=true" >}}std::list<int>{{< /highlight >}} in C++.
-   [Line 3](#org-coderef--045394-3) binds the C++ `iterator` type from
    `std::list` with the Nim type `ListIter`.
    -   Note special pattern `<'0>`
{{% sidenote %}}
        See [:open_book: `importcpp` for procs](https://nim-lang.github.io/Nim/manual.html#importcpp-pragma-importcpp-for-procs) for details on such special
        patterns.
        {{% /sidenote %}} in the `importcpp` argument for this binding. The apostrophe
        followed by an integer _i_ is replaced by the _i'th_ parameter in
        the `ListIter[T]` type i.e. the `T` type. So, {{< highlight nim "hl_inline=true" >}}ListIter[cint]{{< /highlight >}} in Nim gets mapped to {{< highlight cpp "hl_inline=true" >}}std::list<int>::iterator{{< /highlight >}} in C++.
-   [Line 5](#org-coderef--045394-5) binds the _list_ constructor `()` with
    the Nim proc `initList[T]()`. As the constructor returns a _list_
    type, the return type on the Nim side set to the equivalent
    `List[T]`.
    -   Note the slightly different pattern in the `importcpp` argument
        for this binding: `<'*​0>`. If we had **not** used that _asterisk_
        there, that would have mapped to the proc's return type,
        `List[T]`. But we want the C++ mapped constructor to look like
        {{< highlight cpp "hl_inline=true" >}}std::list<int>(){{< /highlight >}} i.e. we need `T` and not
        `List[T]` in the C++ template type. The `*` does the job of
        deriving `T` from its "container" type `List[T]`.
-   In lines [6](#org-coderef--045394-6) and [7](#org-coderef--045394-7), `size` and
    `begin` on C++ get mapped to same named procs on the Nim side, with
    the correct return types.
    -   [`csize_t`](https://nim-lang.github.io/Nim/system.html#csize_t) is a Nim type that maps to the C `size_t` type.
    -   As [`begin`](https://en.cppreference.com/w/cpp/container/list/begin) returns an _iterator_ handle, the return type of
        `begin` is `ListIter[T]`.
    -   Note that I did not use the `std::list` prefix for these mappings,
        because these procs will always be called in context of their
        arguments. For example, {{< highlight nim "hl_inline=true" >}}listIterVar.size(){{< /highlight >}}
        in Nim will map to {{< highlight cpp "hl_inline=true" >}}listIterVar.size(){{< /highlight >}} in
        C++..
-   [Line 8](#org-coderef--045394-8) maps the C/C++ _indirection operator `*`_
    with Nim _dereferencing operator `[]`_. The input argument type is
    an _iterator_. So if `iter` is a variable of type `ListIter[cint]`,
    {{< highlight nim "hl_inline=true" >}}iter[]{{< /highlight >}} will return the value of type `cint`
    pointed to by that iterator.
    -   The `importcpp` argument for this mapping has a different pattern:
        `*#`. The `#` is replaced by the first argument of the proc:
        `iter`. So {{< highlight nim "hl_inline=true" >}}iter[]{{< /highlight >}} in Nim code
{{% sidenote %}}
        `iter[]` is same as writing `` `[]`(iter) ``, but one might agree that the
        former style is better and more readable.
        {{% /sidenote %}} will translate to {{< highlight cpp "hl_inline=true" >}}*​iter{{< /highlight >}}[^fn:1] on the C++
        side.


## Test C++ code {#test-c-plus-plus-code}

We have the `<list>` bindings ready, but now we need some test C++
code which will be our "library" for use in the Nim code.

So here it is:

<a id="code-snippet--nim-bindings-cpp-test-code"></a>
```cpp
void generateList(int num, std::list<int> *retList)
{
    for (int i = 0; i < num; ++i) {
        retList->push_back(i * 2);
    }
}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-cpp-test-code">Code Snippet 3</a>:</span>
  Dummy C++ "library" that we will extend in Nim
</div>

The C++ "library" here has a `generateList` API with expects an
integer `num` as the first arg, and it save a `num` element
`std::list<int>` object to the pointer passed as its second arg.

Now, we can either go the conventional route of compiling this code to
a shared object (`.so` or `.dll`) and then linking it to the Nim code
using the [`link` pragma](https://nim-lang.github.io/Nim/manual.html#noalias-pragma-link-pragma).

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;But.. that is boring :zzz:<br />

</div>

We will do something cooler.. we will inline that C++ code in Nim
using the [`emit` pragma](https://nim-lang.github.io/Nim/manual.html#noalias-pragma-emit-pragma) and bind to it using `importcpp` just as we
did for the `<list>` methods! :sunglasses:

<a id="code-snippet--nim-bindings-emit"></a>
```nim
{.emit: """
void generateList(int num, std::list<int> *retList)
{
    for (int i = 0; i < num; ++i) {
        retList->push_back(i * 2);
    }
}
""".}
proc generateList(num: cint; lPtr: ptr List[cint]) {.importcpp: "generateList(@)".}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-emit">Code Snippet 4</a>:</span>
  Inlining C++ code in Nim using the <code>emit</code> pragma and then binding to it using <code>importcpp</code>
</div>

Here, I also introduce you readers to a new `importcpp` pattern:
`@`. The `@` expands to comma-separated arguments passed on the Nim
side.

Note the equivalence between the C++ function signature
{{< highlight cpp "hl_inline=true" >}}void generateList(int num, std::list<int> *​retList){{< /highlight >}} and Nim proc signature {{< highlight nim "hl_inline=true" >}}proc generateList(num: cint; lPtr: ptr List[cint]){{< /highlight >}}.


## Test Nim code {#test-nim-code}

Below Nim code will call the test C++ `generateList` method defined
above and then print the whole _list_ object after converting it to a
Nim sequence.

<a id="code-snippet--nim-bindings-test-nim-code"></a>
```nim
var
  l = initList[cint]()
generateList(10, addr l)
var
  s = l.toSeq()
echo "sequence   = ", s
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-test-nim-code">Code Snippet 5</a>:</span>
  Test Nim code that will call the <code>generateList</code> method implemented in C++
</div>

-   In above snippet, [`initList`](#org-coderef--045394-5) bindings defined in
    [Code Snippet 2](#code-snippet--nim-std-list-bindings) is used to initialize the _list_ object
    `l`.
-   Its address or pointer is passed to `generateList` which will
    populate the Nim allocated memory with 10 elements.
-   Then we call a `toSeq` proc (that we will define in the next
    section) to convert that _list_ to a Nim sequence.
-   Now that the _list_ object is converted to a Nim sequence, printing
    it out as easy as you see above :smile:.


## Extending the `std::list` API in Nim {#extending-the-std-list-api-in-nim}

The [end goal](#nim-cpp-bindings-goal) is to be able to write a `toSeq` proc that can convert a
`std::list` object to a Nim sequence. Here's a psuedo-code I began
before it evolved into [Code Snippet 7](#code-snippet--nim-bindings-toSeq).

<a id="code-snippet--nim-bindings-toSeq-pseudo"></a>
```text
proc toSeq (l: list_obj): sequence
  lBegin = handle to the iterator for the beginning of list_obj; use `begin`
  lSize = number of elements in list_obj; use `size`

  lIter = lBegin
  for idx in 0 ..< lSize:
    sequence[idx] = *lIter; use the `[]` dereferencing operator
    lIter = pointer to the next element; use `next` from <iterator>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-toSeq-pseudo">Code Snippet 6</a>:</span>
  Pseudocode on how I plan to implement <code>toSeq</code>
</div>

As it turned out, the real Nim code wasn't longer or more complicated
than the pseudocode :laughing:.

<a id="code-snippet--nim-bindings-toSeq"></a>
```nim { linenos=true, anchorlinenos=true, lineanchors=org-coderef--7ad52f }
proc next*[T](it: ListIter[T]; n = 1): ListIter[T] {.importcpp: "next(@)", header: "<iterator>".}

proc toSeq*[T](l: List[T]): seq[T] =
  result = newSeq[T](l.size())
  var
    it = l.begin()
  for i in 0 ..< l.size():
    result[i] = it[]
    it = it.next()
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-toSeq">Code Snippet 7</a>:</span>
  Adding new API <code>toSeq</code> for <code>std::list</code> objects
</div>

-   [Line 1](#org-coderef--7ad52f-1) - We needed a binding to the [`std::next`](https://en.cppreference.com/w/cpp/iterator/next)
    method from iterator so that we can increment the iterator starting
    from the `begin()` returned value.
-   [Line 4](#org-coderef--7ad52f-4) - We already know the size of the _list_
    object, so we pre-allocated the Nim sequence to the same size by
    using the [`newSeq`](https://nim-lang.github.io/Nim/system.html#newSeq) proc.
-   Then rest of the code follows the idea presented in the pseudocode
    above.


## Final Code {#final-code}

The final code to (i) wrap the `<list>` types and methods and (ii) one
method from `<iterator>`, and (iii) define a proc for converting
`std::list` objects to Nim sequences was only 22 lines! (including
comments and blank lines)

<details>
<summary>Click here to see the full code + output</summary>
<div class="details">

<a id="code-snippet--nim-bindings-full-code"></a>
```nim { linenos=true, linenostart=1 }
when not defined(cpp): {.error: "This file needs to be compiled with cpp backend: Run 'nim -b:cpp r <file>'.".}

## <list> bindings -- https://en.cppreference.com/w/cpp/container/list
type
  List*[T]                              {.importcpp: "std::list", header: "<list>".} = object
  ListIter*[T]                          {.importcpp: "std::list<'0>::iterator", header: "<list>".} = object

proc initList*[T](): List[T]            {.importcpp: "std::list<'*0>()", constructor, header: "<list>".}
proc size*(l: List): csize_t            {.importcpp: "size", header: "<list>".}
proc begin*[T](l: List[T]): ListIter[T] {.importcpp: "begin", header: "<list>".}
proc `[]`*[T](it: ListIter[T]): T       {.importcpp: "*#", header: "<list>".}

## std::list processing procs and iterators
proc next*[T](it: ListIter[T]; n = 1): ListIter[T] {.importcpp: "next(@)", header: "<iterator>".}

proc toSeq*[T](l: List[T]): seq[T] =
  result = newSeq[T](l.size())
  var
    it = l.begin()
  for i in 0 ..< l.size():
    result[i] = it[]
    it = it.next()

## Test
when isMainModule:
  {.emit: """
  void generateList(int num, std::list<int> *retList)
  {
      for (int i = 0; i < num; ++i) {
          retList->push_back(i * 2);
      }
  }
  """.}
  proc generateList(num: cint; lPtr: ptr List[cint]) {.importcpp: "generateList(@)".}

  var
    l = initList[cint]()
  generateList(10, addr l)
  var
    s = l.toSeq()
  echo "sequence   = ", s
  echo "first elem = ", s[0]
  echo "elem 5     = ", s[5]
  echo "last elem  = ", s[^1]
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--nim-bindings-full-code">Code Snippet 8</a>:</span>
  Full Nim solution for converting <code>std::list</code> to a Nim sequence, with test C++ code
</div>

```text
sequence   = @[0, 2, 4, 6, 8, 10, 12, 14, 16, 18]
first elem = 0
elem 5     = 10
last elem  = 18
```
</div>
</details>

You can try this code by saving to a `list_to_seq.nim` file and
running `nim -b:cpp r list_to_seq.nim`.

Alternatively, you can paste this code on <https://play.nim-lang.org/>,
set the 'Compilation target' to **C++** and hit the yellow 'Run!'
button.


## Summary {#summary}

It becomes fairly easy to map any C++ (or C) library to Nim. Once that
it done, you can leverage the power of Nim to extend that mapped
library or manipulate data generated by that library. And you do not
need to be a C++ programmer to do so (I am not).

The folks on [Nim Forum](https://forum.nim-lang.org/), [Discord](https://discord.gg/nim) and elsewhere have been very generous
with their help and support, and that's one of the main reasons it's a
joy to use Nim. :pray:


## References {#references}

-   <span class="org-target" id="ref-importcpp"></span>:open_book: [Nim Manual -- `importcpp` pragma](https://nim-lang.github.io/Nim/manual.html#noalias-pragma-importcpp-pragma)
-   [C++ `std::list` reference](https://en.cppreference.com/w/cpp/container/list)
-   Nim [`cppstl`](https://github.com/Clonkk/nim-cppstl)
{{% sidenote %}}
    This package contains bindings to `std::string`, `std::vector` and
    `std::complex` as of <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-03-10 Thu&gt;</span></span>.
    {{% /sidenote %}} package -- can be installed using `nimble install cppstl`

[^fn:1]: I have almost no experience with coding in C++; I got help with
    this `importcpp: *#` binding from user _sls1005_ on [this Nim Forums
    thread](https://forum.nim-lang.org/t/8994). Thank you!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
