+++
title = """
  Follow-up: Golang Quirk: Number-strings starting with "0" are Octals
  """
author = ["Kaushal Modi"]
description = """
  Follow-up post to my earlier post on octals in Golang -- Feedback to
  reactions that varied from ridicule to helpfulness to empathy.
  """
date = 2018-04-23T23:39:00-04:00
series = ["Golang Octals"]
tags = ["golang", "octal", "quirk"]
categories = ["hugo", "replies"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
+++

First of all---I get it. Golang is not the **only** language that has
this odd behavior related to _octals_. But following the foot-steps of
ancestor languages in this particular aspect does not mean that Golang
is doing the Right Thing(TM).

I got many interesting comments at:

-   [A thread on _r/programmingcirclejerk_](https://www.reddit.com/r/programmingcirclejerk/comments/8dasl5/what_is_octal_s/)
-   [A thread on _r/golang_](https://www.reddit.com/r/golang/comments/8d9d5n/golang_quirk_numberstrings_starting_with_0_are/)
-   [HN thread](https://news.ycombinator.com/item?id=16871683)
-   And on the [original post]({{< relref "golang-quirk-number-strings-starting-with-0-are-octals" >}}) itself.

Here are some highlights.

[**zcdopizocioics**](https://news.ycombinator.com/user?id=zcdopizocioics) from Hacker News
: The `strconv` function simply acts in
    the same manner as the language does, see
    <https://play.golang.org/p/rg0VwhCDVB_C>. The idea for `strconv`
    originates in c (see atoi etc), and c also has 0-octal
    notation. Go is simply mimicing c in this regard.

    **My reply** -- _I just wished the octals needed a stricter prefix
    like "0o" as they are not very common compared to decimal and hex
    (in my experience at least)._

[**karmakaze**](https://news.ycombinator.com/user?id=karmakaze) from Hacker News
: A better solution is to always provide the
    base (e.g. 10) rather than using 0 if you don't want special
    interpretation of '0x' and '0' prefixes. That should be in
    Hugo. The problem is not with the golang library which was well
    documented and always worked as such. By placing any blame on the
    golang library, there's less incentive to fix Hugo.

    **My reply** -- _That's a valid point. I have [made that suggestion](https://github.com/gohugoio/hugo/issues/4628#issuecomment-382595019) to
     the Hugo devs. Someone on Reddit also suggested using
     `strconv.Atoi()` instead of `ParseInt()`. I have added to the
     suggestion too._

[**/u/shekelharmony**](https://www.reddit.com/user/shekelharmony) from Reddit
: This isn't really a quirk of Go - the
    documentation says exactly how `strconv.ParseInt()` works, and
    you would only get that behavior by intentionally setting the
    second argument to 0. If you're always working with decimals, you
    might use `strconv.Atoi()` instead since it only takes one
    argument. You could say it's a quirk of Hugo though, since it
    sounds like they don't have a function for parsing decimals at
    all.

    **My reply** -- _.. I'll definitely forward your suggestion with the
    Hugo dev team._ (I then do that, which I linked above.)

[**/u/FascinatedBox**](https://www.reddit.com/user/FascinatedBox) from Reddit
: Java and C both have this and it's
    incredibly stupid. Every once in a while some noob thinks that
    leading zeroes don't matter (they shouldn't). So they write a
    number like 0123 and it doesn't come out to 123 but there is
    nothing you can look up to figure out why. There's no function
    you're calling to say "okay, why is function X doing this
    transformation?" It's really not hard to have some sort of a
    prefix. We have a prefix for binary numbers when languages have
    that. We have one for hex. Why not octal. My language uses `0c`
    to denote octal so that people don't accidentally fall into this
    trap. I added that in precisely because I saw someone in a
    programming class get burned by accidental octal. I knew what the
    problem was right away, because I'm a PL nerd. But a lot of
    people don't want octal, and they certainly aren't expecting it.

    **My reply** -- _Thank you. Exactly my point.. We have prefixes for
    binary and hexadecimal. So I find it odd that octal is given a
    special treatment by it not needing a prefix! And Golang was
    created in 2009; it shouldn't have blindly ported that from
    C. And who IS writing octal literals today, so furiously, in so
    much abundance, that they cannot afford using a normal prefix
    like `0o`? Kudos to Python for obsoleting the `0` octal prefix in
    lieu of `0o` in Python 3._

[**/u/stone_henge**](https://www.reddit.com/user/stone_henge) from Reddit
: poor guy, he just goes on and on about it
    forever, no way for anyone to step in in the middle of it and
    point out to him that he's being dumb.

    **My reply** -- _Yup. Poor, dumb, me._

[**/u/Noughmad**](https://www.reddit.com/user/Noughmad) from Reddit
: The handling of octal numbers in C is
    stupid, I have been bitten by it especially when trying to align
    multiple numbers with a different number of digits. There should
    be some non-digit identifier (and not O because it looks to much
    like 0 in a fixed-width font) used for specifying octal numbers,
    like x for hexadecimal and now b for binary. But once again, C
    gets a pass because it was made over 40 years ago. Go doesn't
    have this excuse.

    **My reply** -- _Thank you. Exactly._

[**/u/pingpong**](https://www.reddit.com/user/pingpong) from Reddit
: Relevant golang bugs, where people try to
    fix octal: [golang issue #22671](https://github.com/golang/go/issues/22671), [golang issue #151](https://github.com/golang/go/issues/151), [golang issue
    \#12711](https://github.com/golang/go/issues/12711).

    **My reply** -- _Great! So I am not alone._

---

You can enjoy more of such comments by visiting the Reddit thread
linked above.

I would though welcome constructive comments below, or if you prefer,
at one of those reddit/HN threads linked above.


## Conclusion {#conclusion}

Golang is a modern language born in 2009. It could have fixed this by
requiring a more logical prefix like `0o` for Octals. _Or can it
still?_

At least I hope that parsing of numbers in front-matter in Hugo gets
fixed as it's unlikely for folks to use octal numbers there.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
