+++
title = "titleref: Referencing Hugo posts by their titles"
author = ["Kaushal Modi"]
description = """
  A custom Hugo shortcode to create cross-references to other posts
  using their title strings.
  """
date = 2022-03-01T00:20:00-05:00
series = ["Hugo Shortcodes"]
tags = ["shortcodes", "cross-referencing", "100DaysToOffload", "org-macro"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  hugo = "0.93.0"
[syndication]
  mastodon = 107879643918611754
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [`relref` shortcode](#relref-shortcode)
- [`relref` Org macro](#relref-org-macro)
- [`titleref` shortcode](#titleref-shortcode)
- [`titleref` Org macro](#titleref-org-macro)
- [Summary](#summary)

</div>
<!--endtoc-->



## `relref` shortcode {#relref-shortcode}

Hugo has a built-in shortcode [`relref`](https://gohugo.io/content-management/cross-references/) that gets relative links
{{% sidenote %}}
There's also a `ref` shortcode which creates absolute links. `ref` can
be used wherever `relref` is used.
{{% /sidenote %}} to other posts by using their `url`. In [Hugo lingo](https://gohugo.io/content-management/organization#path-breakdown-in-hugo), a post's `url`
consists of the post's `section` and `slug`
{{% sidenote %}}
This post's `section` is `posts` and `slug` is
`titleref-referencing-hugo-posts-by-their-titles`. So the `url` is
`posts/titleref-referencing-hugo-posts-by-their-titles`.
{{% /sidenote %}} .

This page's relative link can be fetched by using {{< highlight md "hl_inline=true" >}}{{</* relref "posts/titleref-referencing-hugo-posts-by-their-titles" */>}}{{< /highlight >}}. If a
post's slug is unique across all the posts, its `section` part can be
skipped. So the same `relref` can be written as {{< highlight md "hl_inline=true" >}}{{</* relref "titleref-referencing-hugo-posts-by-their-titles" */>}}{{< /highlight >}} and that will
still return
[ this ]({{< relref "titleref-referencing-hugo-posts-by-their-titles" >}})
same link.

Problem Statement
: The issue with `relref` is that it returns only
    the link --- the description still needs to be written manually. I
    typically use a post's title as description when I link to it. So
    the full link with description will look like this when writing in
    Org mode
{{% sidenote %}}
    In Markdown, the same link will look like this: {{< highlight md "hl_inline=true" >}}[titleref: Referencing Hugo posts by their titles]({{</* relref "titleref-referencing-hugo-posts-by-their-titles" */>}}){{< /highlight >}}.
    {{% /sidenote %}} for exporting using [ox-hugo](https://ox-hugo.scripter.co):
    {{< highlight org "hl_inline=true" >}}[[@@hugo:{{</* relref "titleref-referencing-hugo-posts-by-their-titles" */>}}@@][titleref: Referencing Hugo posts by their titles]]{{< /highlight >}}.

You can see how impractical and verbose that is!

_If you write your posts in Markdown, you can jump to the [`titleref`
shortcode](#titleref-shortcode) section._


## `relref` Org macro {#relref-org-macro}

An Org macro can help a little over here.

<a id="code-snippet--relref-org-macro"></a>
```org
#+macro: relref @@hugo:[@@ $1 @@hugo:]({{</* relref "$2" */>}})@@
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--relref-org-macro">Code Snippet 1</a>:</span>
  <code>relref</code> Org macro
</div>

<span class="org-target" id="org-target--relref-macro-example"></span> With this macro, that same `relref` link can
be rewritten as {{< highlight org "hl_inline=true" >}}{{{relref(titleref: Referencing Hugo posts by their titles,titleref-referencing-hugo-posts-by-their-titles)}}}{{< /highlight >}}.

It's a bit less verbose compared to using the shortcode directly, but
I still wasn't happy with the [redundancy](https://en.wikipedia.org/wiki/Redundancy_(information_theory)).


## `titleref` shortcode {#titleref-shortcode}

<div class="note">

In that _relref_ link, we are providing both, the post slug and the
post title.
<mark>Both are uniquely associated with each-other</mark> , and Hugo can internally derive one from the other. So there's no
need for us to specify both!

</div>

And so I thought that it should be pretty simple to specify just the
title of post that I want to link, and let Hugo derive the post's
permalink --- and it was simple, by using this custom
{{% sidenote %}}
A user can add custom shortcodes or override the default Hugo
shortcodes by putting them in the `layouts/shortcodes/` directory of
the site or theme repo.
{{% /sidenote %}} shortcode!

An astute Org mode user might think that this idea looks like the
{{< highlight org "hl_inline=true" >}}[[*Heading]]{{< /highlight >}} syntax [supported by Org](https://orgmode.org/manual/Internal-Links.html), and they
would be right! This `titleref` idea is inspired by that Org mode
feature.

<a id="code-snippet--titleref-sc"></a>
```go-html-template { linenos=true, hl_lines=["5","12","13"], anchorlinenos=true, lineanchors=org-coderef--4922be }
{{- $title_anchor := (.Get 0) -}}
{{- $title := trim (index (split $title_anchor "#") 0) " " -}}
{{- $anchor := trim (index (split $title_anchor "#") 1) " " -}}
{{- $desc := (.Get 1) | markdownify -}}
{{- $filtered := where site.RegularPages "Title" $title -}}
{{- $first_match := index $filtered 0 -}}
{{- with $first_match -}}
    {{- if $anchor -}}
        {{- $anchor = printf `#%s` $anchor -}}
    {{- end -}}
    {{- $anchor = $anchor | default "" -}}
    {{- $desc = $desc | default .Title -}}
    {{- (printf `<a href="%s%s">%s</a>` .RelPermalink $anchor $desc) | safeHTML -}}
{{- else -}}
    {{- (errorf `%s: Page titled "%s" not found` .Position $title) -}}
{{- end -}}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--titleref-sc">Code Snippet 2</a>:</span>
  <code>titleref</code> Shortcode
</div>

This shortcode is designed to take up to two arguments:

title (+anchor)
: The first argument can be just the title of the
    post that we want to link (e.g. _"My post"_). Optionally, it can
    also be followed by an anchor in that post, separated by "#"
    (e.g. _"My post#some-heading"_).

(description)
: The second argument is optional, and it's the link
    description. If this argument is not set, the description is set to
    the post's title.

<details title="Click to expand">
<summary>How this shortcode works</summary>
<div class="details">

-   In lines [2](#org-coderef--4922be-2) and [3](#org-coderef--4922be-3), the title and
    anchor portions are parsed from the first argument.
-   In line [4](#org-coderef--4922be-4), if the description is specified using a
    second argument, it's converted to HTML using `markdownify`.
-   :sparkles: [Line 5](#org-coderef--4922be-5) is the main logic -- It
    returned a list of all the pages whose _Title_ matches the title
    parsed from the first argument.
<mark>This list should have only 1 element as the assumption is that all
    blog posts have unique titles.</mark>
-   The first (and only) _page_ element of that list is saved on
    `$first_match` in line [6](#org-coderef--4922be-6).
-   If the _Title_ based search was successfully in that main logic,
    `$first_match` won't be _nil_. In line [12](#org-coderef--4922be-12),
    the link description is set to the `.Title` from that page object,
    if a description wasn't already set using the second
    argument. Finally an HTML link is printed by using `.RelPermalink`
    (_relative permalink_) from the _page_ object and the description.
-   If the _Title_ based search fails in the main logic, an error is
    printed on line [15](#org-coderef--4922be-15).
</div>
</details>

<details title="Click to expand">
<summary>Examples of using the <code>titleref</code> shortcode (for Markdown users)</summary>
<div class="details">

-   {{< highlight md "hl_inline=true" >}}{{</* titleref "TITLE" */>}}{{< /highlight >}} -- Link to a page
    with that _TITLE_ and set the link description to be the same as the
    _TITLE_.
-   {{< highlight md "hl_inline=true" >}}{{</* titleref "TITLE" "DESC" */>}}{{< /highlight >}} -- Link to a
    page with that _TITLE_, with _DESC_ as description.
-   {{< highlight md "hl_inline=true" >}}{{</* titleref "TITLE#ANCHOR" "DESC" */>}}{{< /highlight >}} --
    Link to an _ANCHOR_ in a page with that _TITLE_, with _DESC_ as
    description.
</div>
</details>


## `titleref` Org macro {#titleref-org-macro}

But the fun doesn't end here for an Org mode user! :smiley:

I do not prefer using Hugo shortcodes directly in my Org source. So I
created an Org macro to wrap this shortcode.

<a id="code-snippet--titleref-org-macro"></a>
```org
#+macro: titleref @@hugo:{{</* titleref "$1" "@@ $2 @@hugo:" */>}}@@
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--titleref-org-macro">Code Snippet 3</a>:</span>
  <code>titleref</code> Org macro
</div>

Now {{< highlight org "hl_inline=true" >}}{{{titleref(titleref: Referencing Hugo posts by their titles,)}}}{{< /highlight >}} will export to {{< titleref "titleref: Referencing Hugo posts by their titles" "  " >}}.

Here are some example uses of this macro:

-   {{< highlight org "hl_inline=true" >}}{{{titleref(TITLE,)}}}{{< /highlight >}} -- Link to a page
    with that _TITLE_ and set the link description to be the same as the
    _TITLE_.
-   {{< highlight org "hl_inline=true" >}}{{{titleref(TITLE,DESC)}}}{{< /highlight >}} -- Link to a page
    with that _TITLE_, with _DESC_ as description.
-   {{< highlight org "hl_inline=true" >}}{{{titleref(TITLE#ANCHOR,DESC)}}}{{< /highlight >}} -- Link to
    an _ANCHOR_ in a page with that _TITLE_, with _DESC_ as description.


## Summary {#summary}

By using the `titleref` shortcode (or Org macro) instead of `relref`,
we now have a tremendous reduction of redundancy.

[Before](#org-target--relref-macro-example)
: -   Org mode: {{< highlight org "hl_inline=true" >}}{{{relref(titleref: Referencing Hugo posts by their titles,titleref-referencing-hugo-posts-by-their-titles)}}}{{< /highlight >}}
    -   Markdown: {{< highlight md "hl_inline=true" >}}[titleref: Referencing Hugo posts by their titles]({{</* relref "titleref-referencing-hugo-posts-by-their-titles" */>}}){{< /highlight >}}

Now
: -   Org mode: {{< highlight org "hl_inline=true" >}}{{{titleref(titleref: Referencing Hugo posts by their titles,)}}}{{< /highlight >}}
    -   Markdown: {{< highlight md "hl_inline=true" >}}{{</* titleref "titleref: Referencing Hugo posts by their titles" */>}}{{< /highlight >}}

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
