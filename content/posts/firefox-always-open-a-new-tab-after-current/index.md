+++
title = "Firefox: Always open a New Tab after Current"
author = ["Kaushal Modi"]
description = """
  The default behavior of Firefox opening new tabs from links had been
  troubling me for a while .. it was time I tried to do something about
  it.
  """
date = 2022-06-15T02:08:00-04:00
tags = ["firefox", "tab", "add-on", "100DaysToOffload"]
categories = ["web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  firefox = "101.0.1"
[syndication]
  mastodon = 108480021175931766
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [The Issue](#the-issue)
- [Expected Behavior](#expected-behavior)
- [False Leads](#false-leads)
- [Firefox Bug # 1485683](#firefox-bug-1485683)
- [Solution](#solution)

</div>
<!--endtoc-->


This was one of the those little things in software that bug me, but I
don't start looking for a solution right-away because of reasons like
"I am too busy with something else", or "The bug is not so bad.. let
me see if I can live with it".

But no .. this particular issue with how Firefox dealt with opening
new tabs from links was now getting on my nerves.


## The Issue {#the-issue}

While visiting a page, I typically middle-click on links so that they
open in new tabs .. and I do that a lot! And this is the order of
where the new tabs would open:

<a id="figure--firefox-new-tabs-unexpected"></a>

{{< figure src="unexpected.svg" caption="<span class=\"figure-number\">Figure 1: </span>Unexpected order of opening of new tabs opened after each middle-click" >}}

<div class="org-center">

In my view, this behavior is completely wrong!

</div>

As you can see above, if I already have a bunch of links open (see the
last row where "Tab 1", "Tab 2" and "Tab 3" are already open), the
last opened tab will land up between those tabs and a sea of other
previously opened tabs! When you have over a dozen of tabs already
open, hunting for the _last opened tabs_ is really annoying.


## Expected Behavior {#expected-behavior}

I would instead expect for each middle-click to open the new tab
**immediately to the right** of the current tab. Below diagram shows it
very clearly that this behavior would save me the exercise of hunting
for that new tab.

<a id="figure--firefox-new-tabs-expected"></a>

{{< figure src="expected.svg" caption="<span class=\"figure-number\">Figure 2: </span>**Expected** order of opening of new tabs opened after each middle-click" >}}


## False Leads {#false-leads}

So I searched online for "firefox new tab location" and similar search
terms, and found _Q&amp;A_ on Mozilla Support like [How do I make tabs open
on the right by default?](https://support.mozilla.org/en-US/questions/1229062#answer-1141742) and [New tabs ALWAYS open to the right of
the last open tab, not to the right of current tab](https://support.mozilla.org/en-US/questions/1295586#answer-1334375).

The "Chosen solutions" for both of these issues suggested setting
`browser.tabs.insertAfterCurrent = true` and
`browser.tabs.insertRelatedAfterCurrent = true` (which is the default)
in the Firefox **about:config**.

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;But that didn't work!!<br />

</div>

Setting `browser.tabs.insertAfterCurrent = true` affected the "new tab
opening" behavior when opening only blank new tabs using `Ctrl+T` or
hitting the "New Tab" button.
<mark>This setting had **nothing** to do with the location of new tabs opened
by the "Open Link in New Tab" or "middle-click on link" action.</mark>


## Firefox Bug # 1485683 {#firefox-bug-1485683}

After scouring through many search results, I landed up on [Firefox
Bug # 1485683: browser.tab.insertaftercurrent order](https://bugzilla.mozilla.org/show_bug.cgi?id=1485683) which reported
the exact same issue I was seeing :tada:.

So I replied to that bug thread confirming that I still saw that issue
on Firefox 101.0.1 .. **4 years later** .. just in case that motivates
someone to still fix it.

But my issue isn't fixed yet.


## Solution {#solution}

Finally, the [Open Tabs Next to Current](https://addons.mozilla.org/en-US/firefox/addon/open-tabs-next-to-current/) ([repo](https://github.com/sblask/webextension-open-tabs-next-to-current)) Firefox Add-on saved the
day!

The introduction of this add-on incorrectly states this:

> After the addition of the `browser.tabs.insertAfterCurrent` setting in
> **about:config** you do not need this extension anymore.

But I still tried installing it to see .. just in case
.. :fingers_crossed: ..

<div class="verse">

And it worked!!<br />

</div>

I could finally get new tabs to open in the order as shown in the
[Expected Tab Order figure](#figure--firefox-new-tabs-expected) above :face_with_tears_of_joy:.

Thank you Sebastian Blask for **Open Tabs Next to Current**!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
