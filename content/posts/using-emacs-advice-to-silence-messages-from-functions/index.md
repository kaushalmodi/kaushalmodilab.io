+++
title = "Using Emacs advice to silence messages from functions"
author = ["Kaushal Modi"]
description = """
  Using the `:around` advice to prevent messages from being displayed in
  the _echo_ area or the &lowast;Messages&lowast; buffer.
  """
date = 2022-03-19T02:25:00-04:00
tags = ["advice", "100DaysToOffload"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.0.92"
[syndication]
  mastodon = 107984145057464668
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Problem Statement](#problem-statement)
- [Course of action](#course-of-action)
- [<span class="section-num">1</span> Finding the culprit functions](#finding-the-culprit-functions)
    - [Using plain-old _grep_ or _ripgrep (rg)_](#using-plain-old-grep-or-ripgrep--rg)
    - [Using `debug-on-message`](#using-debug-on-message)
- [<span class="section-num">2</span> Figure out how to silence those messages](#figure-out-how-to-silence-those-messages)
- [<span class="section-num">3</span> Applying the 'silence' advice](#applying-the-silence-advice)
    - [Limiting the scope of these advices](#limiting-the-scope-of-these-advices)
- [Conclusion](#conclusion)

</div>
<!--endtoc-->

Today I was working on a minor cleanup of the `ox-hugo` package and as
a part of that, I figured out a way to reduce the noisy messages
printed in the &lowast;Messages&lowast;
{{% sidenote %}}
Random tidbit: You can quickly access the &lowast;Messages&lowast; buffer using the
default binding `C-h e`.
{{% /sidenote %}} buffer. This post describes how I used the [:open_book: Emacs Advice
feature](https://www.gnu.org/software/emacs/manual/html_node/elisp/Advising-Functions.html) to solve the noisy-messages-buffer problem.

What is this 'advising'?
: In Emacs, _advising_
{{% sidenote %}}
    _Advice_ with a 'c' is the _noun_ form. The _noun_ form is used
    everywhere in the Emacs advising API e.g. `advice-add`,
    `advice-remove`. _Advise_ with an 's' is the _verb_ form. [More
    Reading](https://www.merriam-webster.com/words-at-play/advise-vs-advice-usage).
    {{% /sidenote %}} is a means to modify a function defined in any library --- You can
    modify the arguments to the function, return values of that
    function, run other functions before and/or after that advised
    function, .. or may be just replace that function with something
    else entirely!


## Problem Statement {#problem-statement}

If I run `C-c C-e H A`
{{% sidenote %}}
This is the default binding of `ox-hugo` to export all the post
subtrees in the current Org file.
{{% /sidenote %}} in this site's Org file, I will see a wall of messages, which won't be
too useful to look at if I am exporting dozens of posts in one go.

Here's a snippet from that which shows the text printed while two of
the posts were being exporting:

<a id="code-snippet--silence-messages-noise"></a>
```text { hl_lines=["2","10"] }
[ox-hugo] 60/ Exporting ‘Nim: Deploying static binaries’ ..
org-babel-exp process nim at position 246918...
Evaluation of this nim code block is disabled.
org-babel-exp process shell at position 246985...
org-babel-exp process shell at position 247505...
org-babel-exp process shell at position 248054...
org-babel-exp process nim at position 250620...
Evaluation of this nim code block (code__musl_config_nims) is disabled.
org-babel-exp process yaml at position 253830...
Wrote /home/kmodi/hugo/scripter.co/content/posts/nim-deploying-static-binaries/index.md
[ox-hugo] 61/ Exporting ‘Binding Nim to C++ std::list’ ..
org-babel-exp process nim at position 261418...
Evaluation of this nim code block (code__nim_bindings_importcpp) is disabled.
org-babel-exp process nim at position 262802...
Evaluation of this nim code block (code__nim_std_list_bindings) is disabled.
org-babel-exp process cpp at position 266921...
org-babel-exp process nim at position 267957...
Evaluation of this nim code block (code__nim_bindings_emit) is disabled.
org-babel-exp process nim at position 268861...
Evaluation of this nim code block (code__nim_bindings_test_nim_code) is disabled.
org-babel-exp process text at position 269849...
org-babel-exp process nim at position 270412...
Evaluation of this nim code block (code__nim_bindings_toSeq) is disabled.
org-babel-exp process nim at position 271808...
Evaluation of this nim code block (code__nim_bindings_full_code) is disabled.
Wrote /home/kmodi/hugo/scripter.co/content/posts/binding-nim-to-c-plus-plus-std-list/index.md
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--silence-messages-noise">Code Snippet 1</a>:</span>
  Snippet of the noise seen in the &lowast;Messages&lowast; buffer when exporting all post subtrees using <code>ox-hugo</code>
</div>

After applying the advices described in this post, that output
reduced to this:

<a id="code-snippet--silence-messages-denoised"></a>
```text
[ox-hugo] 60/ Exporting ‘Nim: Deploying static binaries’ ..
[ox-hugo] 61/ Exporting ‘Binding Nim to C++ std::list’ ..
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--silence-messages-denoised">Code Snippet 2</a>:</span>
  Snippet of the &lowast;Messages&lowast; buffer after applying the <i>silencing</i> advices
</div>


## Course of action {#course-of-action}

Here is how I tackled this problem:

1.  Find the functions responsible for printing those messages.
2.  Figure out how to silence those messages.
3.  Advise those functions (temporarily, while the export is happening)
    to not print those messages.


## <span class="section-num">1</span> Finding the culprit functions {#finding-the-culprit-functions}

In [Code Snippet 1](#code-snippet--silence-messages-noise), I have highlighted 2 lines. I first
needed to figure out which functions printed those messages.  I'll
show two methods of finding sources of any printed messages.


### Using plain-old _grep_ or _ripgrep (rg)_ {#using-plain-old-grep-or-ripgrep--rg}

This method is pretty easy (but not robust) to use if the search
string is unique enough. The first highlighted line above is a good
candidate for this:

> org-babel-exp process nim at position 246918...

The message gives me a clue that it's originating somewhere in the
_Org Babel_ source code (one of the `ob-*.el` files). So I _cd_ to the
Org source directory and search for the _"org-babel-exp process .."_
string using [`rg`](https://github.com/BurntSushi/ripgrep) ..

<a id="figure--finding-org-babel-exp-process-message"></a>

{{< figure src="finding-org-babel-exp-process-message.png" caption="<span class=\"figure-number\">Figure 1: </span>Finding the source of \"org-babel-exp process ..\" message using ripgrep (`rg`) in Org source" link="finding-org-babel-exp-process-message.png" >}}

.. and sure enough, there is this one and only one hit .. in the
`org-babel-exp-src-block` function in [`lisp/ob-exp.el`](https://git.savannah.gnu.org/cgit/emacs/org-mode.git/tree/lisp/ob-exp.el/?id=91681fc03334285dc0879fcb9a27583bd7ab9782#n93).

But this method is not robust for some obvious reasons like,

-   the string being _grepped_ might not be present in raw literal form
    like above,
-   may be it's created through variable interpolation, or
-   may be that whole string is not present on the same line; may be it
    wrapped to the next line.

While this _grep_ method helped find the source of _"org-babel-exp
process .."_, let's use a better way of source tracing for the next
message.


### Using `debug-on-message` {#using-debug-on-message}

From `C-h v debug-on-message`, we see:

> If non-nil, debug if a message matching this regexp is displayed.

This means that each time a message gets displayed that matches the
regular expression set in this variable, we get a backtrace to that
message. _Who said we can get backtraces for errors only‽_ 🤓

The second highlighted line from [Code Snippet 1](#code-snippet--silence-messages-noise) is:

> Wrote /home/kmodi/hugo/scripter.co/content/posts/nim-deploying-static-binaries/index.md

After evaluating {{< highlight emacs-lisp "hl_inline=true" >}}(setq debug-on-message "Wrote"){{< /highlight >}}, when I exported this post, I saw a backtrace that looked
like this.

<a id="code-snippet--debug-on-message-backtrace"></a>
```text { hl_lines=["2"] }
Debugger entered--Lisp error: "Wrote /home/kmodi/hugo/scripter.co/content/posts/u..."
  write-region(nil nil "/home/kmodi/hugo/scripter.co/ ..
  basic-save-buffer-2()
  basic-save-buffer-1()
  ..
  basic-save-buffer(nil)
  save-buffer()
  write-file("/home/kmodi/hugo/scripter.co/content/posts/using-e...")
  ..
  org-export-to-file(hugo "/home/kmodi/hugo/scripter.co/content/posts/using-e..." ..
  org-hugo-export-to-md(nil :subtreep nil)
  ..
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--debug-on-message-backtrace">Code Snippet 3</a>:</span>
  Backtrace created on setting <code>debug-on-message</code> to <code>"Wrote</code>"
</div>

The top line of that backtrace shows the source of that message:
`write-region`.

<div class="note">

Remember to reset `debug-on-message` back to _nil_ by evaluating
{{< highlight emacs-lisp "hl_inline=true" >}}(setq debug-on-message nil){{< /highlight >}} once
you are done with this debug!

</div>


## <span class="section-num">2</span> Figure out how to silence those messages {#figure-out-how-to-silence-those-messages}

Now that I knew that I needed to alter the `org-babel-exp-src-block`
and `write-region` functions
{{% sidenote %}}
There was a third function too that I found using this second method:
`table-generate-source`. This was printing _"Generating source..."_
when parsing each `table.el` message.
{{% /sidenote %}} , I wanted to quiet these messages in both the _echo_ area and the
&lowast;Messages&lowast; buffer, but only when exporting all the post
subtrees from a file.

Emacs provides us ways to separately control the displaying of
messages in those regions:

Prevent messages in the _echo_ area
: From [:open_book: Elisp --
    Displaying Messages](https://www.gnu.org/software/emacs/manual/html_node/elisp/Displaying-Messages.html)
{{% sidenote %}}
    You can visit the same manual page from within Emacs by `C-h i g
           (elisp)Displaying Messages`.
    {{% /sidenote %}} , if
<mark>`inhibit-message` is set to _t_</mark> , `message` outputs are not shown in the _echo_ area (though they
    would still be logged in the &lowast;Messages&lowast; buffer.

Prevent messages in the &lowast;Messages&lowast; buffer
: From [:open_book: Elisp
    -- Logging Messages](https://www.gnu.org/software/emacs/manual/html_node/elisp/Logging-Messages.html)
{{% sidenote %}}
    `C-h i g (elisp)Logging Messages`
    {{% /sidenote %}} , if
<mark>`message-log-max` is set to _nil_</mark> , no messages will be printed in the &lowast;Messages&lowast; buffer.


## <span class="section-num">3</span> Applying the 'silence' advice {#applying-the-silence-advice}

With that information, I created this _advising function_ which calls
the function name it receives in its first argument.

<a id="code-snippet--silence-messages-advising-fn"></a>
```emacs-lisp { linenos=true, linenostart=1 }
(defun org-hugo--advice-silence-messages (orig-fun &rest args)
  "Advice function that silences all messages in ORIG-FUN."
  (let ((inhibit-message t)      ;Don't show the messages in Echo area
        (message-log-max nil))   ;Don't show the messages in the *Messages* buffer
    (apply orig-fun args)))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--silence-messages-advising-fn">Code Snippet 4</a>:</span>
  Advising function to silence the messages in the advised <code>orig-fun</code>
</div>

Note that the above function does not set the `inhibit-message` and
`message-log-max` variables globally (e.g. using `setq`). We do not
want to permanently stop the printing of messages for all the
functions! That's why they are set in the `let` scope or the local
scope, and the `orig-fun` is called within that scope.

This snippet, though, only defines the advising function. The advices
are actually applied using the `advice-add`
{{% sidenote %}}
The counterpart of `advice-add` is `advice-remove`, to remove the
advising functions from the original functions.
{{% /sidenote %}} function.

Emacs provides a [variety](https://www.gnu.org/software/emacs/manual/html_node/elisp/Advice-Combinators.html) of _Advice Combinators_. These combinators
are used to specify _how_ an advice should be applied to the original
function. I have found the `:around` combinator to usually fit my
needs because the advices I add typically look like:

1.  Do something before.
2.  Set some variables in a `let` scope.
3.  Call the original function with its arguments inside that scope.
4.  Do something afterwards.

As I wanted to apply the same advice to multiple functions
(`org-babel-exp-src-block` and `write-region`), I used `dolist` to
loop through them and advise them to :shushing_face: :

<a id="code-snippet--silence-messages-advice-add"></a>
```emacs-lisp { linenos=true, linenostart=1 }
(dolist (fn '(org-babel-exp-src-block write-region)
  (advice-add fn :around #'org-hugo--advice-silence-messages))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--silence-messages-advice-add">Code Snippet 5</a>:</span>
  Enabling the advices
</div>


### Limiting the scope of these advices {#limiting-the-scope-of-these-advices}

You might wonder --- If `ox-hugo` is applying these advices, wouldn't
it affect the global behavior of `write-region`, etc. outside of
`ox-hugo` exports as well?

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;The answer is "no".<br />

</div>

In `ox-hugo`, the `advice-add` calls are made just before the "export
all post subtrees" operation begins. Once the export command finishes,
`advice-remove` calls remove those advices from those functions. So
those advices are effective only <span class="underline">for the duration of the export</span>.

Details of the implementation can be seen in [this `ox-hugo` commit](https://github.com/kaushalmodi/ox-hugo/commit/6e4f74d20dcb7e5da65cd504d1c64c944319c5f0).


## Conclusion {#conclusion}

The main effort here was to (i) identify the problem, (ii) find out
which functions were causing the problem, and (iii) what knobs or
variables are available to us to help fix that.

Once that is done, you write an _advising_ function and `advice-add`
that function to the original function using a combinator of your
choice
{{% sidenote %}}
If you are not sure which combinator to use, pick `:around` :smiley:.
{{% /sidenote %}} .

In this post, I am using Emacs advices to make `ox-hugo` exports less
noisy. But you can add similar advices to your Emacs config
{{% sidenote %}}
You can find the ways I am using advices in my Emacs config [here](https://github.com/kaushalmodi/.emacs.d/search?q=advice-add).
{{% /sidenote %}} to tweak and fine-tune any function that's getting on your nerves
:smile:.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
