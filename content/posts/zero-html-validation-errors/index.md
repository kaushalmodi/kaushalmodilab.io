+++
title = "Zero HTML Validation Errors!"
author = ["Kaushal Modi"]
description = """
  How I fixed my site content and went down from 46 HTML validations
  errors down to 0!
  """
date = 2022-06-05T17:58:00-04:00
series = ["HTML5 Validator"]
tags = ["html", "validator", "100DaysToOffload"]
categories = ["emacs", "web", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  mastodon = ""
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [<span class="section-num">1</span> Avoid duplicate heading `id` attributes](#avoid-duplicate-heading-id-attributes)
- [<span class="section-num">2</span> Remove inline `<style>` elements](#remove-inline-style-elements)
- [<span class="section-num">3</span> Ensure that all images have captions or `alt` attributes](#ensure-that-all-images-have-captions-or-alt-attributes)
- [<span class="section-num">4</span> Do not have hyperlinks in headings](#do-not-have-hyperlinks-in-headings)
- [Validation Ignores](#validation-ignores)
    - [<span class="section-num">5</span> Ignore errors due to hyperlinks in inline SVG](#ignore-errors-due-to-hyperlinks-in-inline-svg)
    - [<span class="section-num">6</span> Ignore files not expected to serve HTML content](#ignore-files-not-expected-to-serve-html-content)
- [Summary](#summary)

</div>
<!--endtoc-->


In my [previous HTML5 Validator post]({{< relref "offline-html5-validator" >}}), I mentioned:

> I was a bit disappointed to see validation errors on my site, but then
> it wasn't too bad .. 46 errors.

But they truly say ..

<div class="org-center">

Ignorance is bliss.

</div>

So .. once I realized that my site had 46 validation errors, I lost
that _bliss_ .. and I couldn't rest easy --- I had to fix them all
:grin:.

This post summarizes the categories of those errors and how I fixed
{{% sidenote %}}
The fixes mentioned in this post refer to changes in Org mode
content. But you should be able to derive equivalent fixes for
Markdown too.
{{% /sidenote %}} them all.


## <span class="section-num">1</span> Avoid duplicate heading `id` attributes {#avoid-duplicate-heading-id-attributes}

```text
"file:/public/notes/​string-fns-nim-vs-python/index.html":636.34-636.46: error: Duplicate ID "notes".
"file:/public/notes/​nim-fmt/index.html":300.34-300.52: error: Duplicate ID "older-issue".
"file:/public/notes/​nim-fmt/index.html":306.20-306.33: error: Duplicate ID "floats".
"file:/public/page/6/index.html":29.39-29.54: error: Duplicate ID "fnref:1".
```

Errors with above kind of signatures were fixed by,

1.  Converting headings to description lists

    I had a bunch of generic headings like "Notes" and "Older Issue" in
    some of my posts. After taking a second look at those, it made more
    sense to convert those to description lists. So in Org mode, I
    converted headings like {{< highlight org "hl_inline=true" >}}* Notes{{< /highlight >}} to
    description lists {{< highlight org "hl_inline=true" >}}- Notes ::{{< /highlight >}}.
2.  Setting `CUSTOM_ID` heading property

    For the cases, where the headings needed to be left as so, their
    IDs were uniquified by setting their `CUSTOM_ID` property. For
    example, below fixed the _Duplicate ID "floats"_ errors.
    <a id="code-snippet--using-custom-id-to-uniquify-heading-id"></a>
    ```diff
     ** Precision
     ..
     *** Floats
    +:PROPERTIES:
    +:CUSTOM_ID: precision-floats
    +:END:
     ..
     ** Type (only for numbers)
     ..
     *** Floats
    +:PROPERTIES:
    +:CUSTOM_ID: type-floats
    +:END:
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--using-custom-id-to-uniquify-heading-id">Code Snippet 1</a>:</span>
      Using <code>CUSTOM_ID</code> property to uniquify heading ID's
    </div>
3.  Prevent footnote links in post summaries

    This issue was due to me not being conscious about how the footnote
    references work in a post _versus_ on a page outside that post's
    context.  The issue was caused by footnote references getting into
    the post summaries parsed by Hugo, which will then show up on the
    list pages.

    The fix was simple --- Edit the post summaries so that they don't
    contain any footnote references.


## <span class="section-num">2</span> Remove inline `<style>` elements {#remove-inline-style-elements}

```text
"file:/public/​grep-po/index.html":51.139-51.145: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
"file:/public/​how-do-i-write-org-mode/index.html":23.194-23.200: error: Element "style" not allowed as child of element "div" in this context. (Suppressing further errors from this subtree.)
```

Errors with above kind of signatures were fixed by,

1.  Avoiding export of raw `<style>` elements in the Markdown content

    I figured out which functions were responsible for injecting
    `<style>` elements in Markdown content and then advised them to
    stop that. After applying these advises, I lost the in-content
    rules for CSS classes `.org-center` and `.csl-entry`. So I put
    those rules directly in this website's CSS.
    <a id="code-snippet--advices-to-prevent-style-elem-in-exports"></a>
    ```emacs-lisp
    (defun modi/org-blackfriday-center-block (_center-block contents info)
      (let* ((class "org-center"))
        (format "<div class=\"%s\">%s\n\n%s\n</div>"
                class (org-blackfriday--extra-div-hack info) contents)))
    (advice-add 'org-blackfriday-center-block :override #'modi/org-blackfriday-center-block)

    (defun modi/org-cite-csl-render-bibliography (bib-str)
      (replace-regexp-in-string "<style>\\.csl-entry[^<]+</style>" "" bib-str))
    (advice-add 'org-cite-csl-render-bibliography :filter-return #'modi/org-cite-csl-render-bibliography)
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--advices-to-prevent-style-elem-in-exports">Code Snippet 2</a>:</span>
      Emacs-Lisp advices to prevent <code>&lt;style&gt;</code> elements in exports
    </div>
2.  Removing unnecessary micro-styling

    I found a single case, where an inline CSS rule was defined in
    content for CSS class `.repr-type` for a table. I just removed that
    without affecting the looks of that rendered table too much.


## <span class="section-num">3</span> Ensure that all images have captions or `alt` attributes {#ensure-that-all-images-have-captions-or-alt-attributes}

```text
"file:/public/​hugo-use-goat-code-blocks-for-ascii-diagrams/index.html":24.130-24.255: error: An "img" element must have an "alt" attribute, except under certain conditions. For details, consult guidance on providing text alternatives for images.
```

Errors with above kind of signatures were easily fixed by ensuring
that all images had captions
{{% sidenote %}}
Thankfully, there were only two images that were missing captions.
{{% /sidenote %}} . The Hugo `figure` shortcode adds the caption to the `alt` attribute if
the `alt` is not specified separately.

As an example, here's how I fixed the above error:

<a id="code-snippet--adding-a-caption-to-an-image"></a>
```diff
+ #+name: fig__disproportionate_box_drawing
+ #+caption: Disproportionate box drawing characters
[[file:images/​hugo-use-goat-code-blocks-for-ascii-diagrams/ascii-diagram-rendered-in-plain-text-code-block.png]]
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--adding-a-caption-to-an-image">Code Snippet 3</a>:</span>
  A <code>git diff</code> showing addition of caption to an image
</div>


## <span class="section-num">4</span> Do not have hyperlinks in headings {#do-not-have-hyperlinks-in-headings}

```text
"file:/public/​using-emacs-advice-to-silence-messages-from-functions/index.html":151.687-151.732: error: Start tag "a" seen but an element of the same type was already open.
"file:/public/​using-emacs-advice-to-silence-messages-from-functions/index.html":151.748-151.751: error: Stray end tag "a".
"file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: Start tag "a" seen but an element of the same type was already open.
"file:/public/​auto-count-100daystooffload-posts/index.html":114.446-114.475: error: End tag "a" violates nesting rules.
"file:/public/​auto-count-100daystooffload-posts/index.html":114.526-114.529: error: Stray end tag "a".
```

While the hyperlinks in headings work well, they created invalid HTML
in the Hugo-generated TOC. So while these errors were created
technically because of a bug in Hugo
{{% sidenote %}}
It's really cool when you end up finding a bug in an upstream project
while trying to fix the errors in your own thing :sunglasses:.
{{% /sidenote %}} , I wanted to fix these errors on my end as soon as I can.

I reviewed the errors, and this is all it took to get rid of them all:

1.  Remove manually inserting hyperlinks in headings
    <a id="code-snippet--removing-hyperlink-from-a-heading"></a>
    ```diff
     show two methods of finding sources of any printed messages.
    -***** Using plain-old /grep/ or [[https://github.com/BurntSushi/ripgrep][/rg/]]
    +***** Using plain-old /grep/ or /ripgrep (rg)/
     This method is pretty easy (but not robust) to use if the search
     ..
     Org source directory and search for the /"org-babel-exp process .."/
    -string ..
    +string using [[https://github.com/BurntSushi/ripgrep][~rg~]] ..
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--removing-hyperlink-from-a-heading">Code Snippet 4</a>:</span>
      Removing hyperlink from a heading
    </div>
2.  Remove Org Radio links that created links in headings

    Here, a Org heading happened to contain the string "Day count",
    which was also an [an Org Radio link](https://orgmode.org/manual/Radio-Targets.html "Emacs Lisp: (info \"(org) Radio Targets\")") in that post. While ideally
    that shouldn't have mattered, I removed that radio link to get
    around this Hugo bug.
    <a id="code-snippet--removing-an-org-radio-link"></a>
    ```diff
     .. /Just may be/. But regardless, I am already enjoying writing once
    -again, and it's great to see the <<<Day count>>> (counting up to 100)
    +again, and it's great to see the Day count (counting up to 100)
     increase with each new post!
    ```
    <div class="src-block-caption">
      <span class="src-block-number"><a href="#code-snippet--removing-an-org-radio-link">Code Snippet 5</a>:</span>
      Removing an Org Radio link
    </div>


## Validation Ignores {#validation-ignores}

Above fixes fixed 43 out of 46 errors, but the remaining 3 were unfixable.


### <span class="section-num">5</span> Ignore errors due to hyperlinks in inline SVG {#ignore-errors-due-to-hyperlinks-in-inline-svg}

```text
"file:/public/notes/​plantuml/index.html":114.474-114.678: error: Attribute "title" not allowed on element "a" at this point.
```

This error was caused by hyperlinks in inline SVG elements. These SVG
elements are created by [PlantUML](https://plantuml.com/). The _hyperlinks in SVG_ feature
works great, and as these are generated by PlantUML, I chose to just
ignore these errors.

I ignored this error by adding the `--ignore-re
'notes/plantuml.*Attribute.*title.*not allowed'` switch to the
`html5validator` command.


### <span class="section-num">6</span> Ignore files not expected to serve HTML content {#ignore-files-not-expected-to-serve-html-content}

```text
"file:/public/googleFOO.html":1.1-1.52: error: Non-space characters found without seeing a doctype first. Expected "<!DOCTYPE html>".
```

The `googleFOO.html` file here is not a valid HTML file. It's a just a
_cookie_ file that was used by Google to verify that I own this
domain.

This error was masked by adding the `--ignore 'googleFOO'` switch to
the `html5validator` command.


## Summary {#summary}

Once I fixed the 43 errors by tweaking the Org mode content, and added
those two ignores, I had **zero validation errors**! :tada:

If you are interested in the fix details, [here are the commits](https://gitlab.com/kaushalmodi/kaushalmodi.gitlab.io/-/compare/a7cac5dd1293442a51fd5020a3bcef8da7f75fdc...42d6c72d533c337b5c67fa09207f68e45efe6e6c).

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
