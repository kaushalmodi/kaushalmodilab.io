+++
title = "Fragmentions"
author = ["Kaushal Modi"]
date = 2018-03-28T00:50:00-04:00
aliases = ["/replies/fragmentions"]
tags = ["javascript"]
categories = ["replies", "web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  twitter = 978859503717441536
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Fragmention(er)](#fragmention--er)
- [Reference](#reference)

</div>
<!--endtoc-->

<div class="mf2 reply">In reply to: <p><a class="u-in-reply-to h-cite" rel="in-reply-to" href="http://www.kevinmarks.com/fragmentions.html">http://www.kevinmarks.com/fragmentions.html</a></p></div>

<!--more-->

Have you ever got tired of having to find the a heading's `id` (_view
site source using `Ctrl + U`_) so that you can send someone a link
like `https://example.com/#foo`?[^fn:1]

And then becoming even more frustrated when you find that the heading
that you want to link does not even have an `id` attribute!?

And then in the end you resort to just telling them "Go to that page,
and search for \`foo'"? ---

Today I came across this wonderful Javascript utility script called
[`fragmentions.js`](https://github.com/chapmanu/fragmentions) by [Jonathan Neal](http://www.jonathantneal.com/) (the idea for that was pitched by
Kevin Marks) that alleviates that pain --- at least on this site (or
more, if that site also uses this script, or you use one of the
extensions listed in below reference).

As I have this script loaded in the footer of all pages on this site,
if you want to link to a phrase _"frobnicate foobar"_ on a page (like
this one), simply prefix that with `#`, and append that to that page's
link.. like
<https://scripter.co/replies/fragmentions/#frobnicate%20foobar>[^fn:2] ---
_Go ahead, click that link, try it._

You too can do that for your sites by downloading the
`fragmentions.js` from the above linked project repo and loading it in
your site footer.


## Fragmention(er) {#fragmention--er}

<div class="mf2 reply">In reply to: <p><a class="u-in-reply-to h-cite" rel="in-reply-to" href="https://kartikprabhu.com/notes/fragmentioner">https://kartikprabhu.com/notes/fragmentioner</a></p></div>

[Kartik Prabhu](https://kartikprabhu.com/) then goes an extra mile, and comes up with
[`fragmentioner.js`](https://github.com/kartikprabhu/fragmentioner/) (and `fragmentioner.css`) that's designed to be
used along with `fragmentions.js`.

This project adds a nifty feature --- While `fragmentions.js` allows you
to link to `#any arbitrary phrase`, `fragmentioner.js` allows you to
**select a phrase** and derive a _fragmentions_ link using that.

I have now added that to this site too. _To try it, select any phrase,
and click the "Link to text" popup._


## Reference {#reference}

-   <https://indieweb.org/fragmention>

[^fn:1]: No wonder you will see, what I call "headline hashes", for all
    post sub-headings on my blog :smile:.
[^fn:2]: I replaced the space with `%20` so that you can click that link
    right away, but you can type the literal space when typing in the
    browser address bar.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
