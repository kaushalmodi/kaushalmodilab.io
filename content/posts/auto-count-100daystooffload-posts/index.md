+++
title = "Auto-count #100DaysToOffload posts"
author = ["Kaushal Modi"]
description = """
  Let Hugo do the counting of how far along I got into the
  _#100DaysToOffload_ challenge.
  """
date = 2022-02-20T02:39:00-05:00
tags = ["100DaysToOffload", "template", "partial"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  hugo = "0.92.0"
[syndication]
  mastodon = 107829226964847928
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [_Day count_ stub at the bottom of each post](#day-count-stub-at-the-bottom-of-each-post)
- [`get-post-index-with-tag.html` Partial](#get-post-index-with-tag-dot-html-partial)
- [Closing](#closing)

</div>
<!--endtoc-->

I learned about the [_#100DaysToOffload_](https://100daystooffload.com/) challenge from my [Mastodon](https://mastodon.technology/)
stream about a month back. Motivated by that, this year I have now
already written a lot more blog posts than my last two years combined
{{% sidenote %}}
My grand total number of posts from the last two years is
zero, and this is my 5th post this year. :laughing:
{{% /sidenote %}} .

[Sidenotes using only CSS]({{< relref "sidenotes-using-only-css" >}}) was the first post of my first
_#100DaysToOffload_ challenge that I started on <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-02-03 Thu&gt;</span></span>. May
be I will get to 100 posts by <span class="timestamp-wrapper"><span class="timestamp">&lt;2023-02-02 Thu&gt; </span></span> :crossed_fingers:
.. _Just may be_. But regardless, I am already enjoying writing once
again, and it's great to see the Day count (counting up to 100)
increase with each new post!

But it's not fun to manually type that _Day count_ in each post. This
post is about a little Hugo templating code, a [Hugo partial](https://gohugo.io/templates/partials/)
{{% sidenote %}}
Think of a _Hugo partial_ as a function which you can then call
anywhere in your template or theme files (not in the content files
like Org or Markdown).
{{% /sidenote %}} , that does that job for me :sunglasses:.

Definition of this mission
: I want to have a line at the bottom of
    all posts reading "This is Day NN of #100DaysToOffload.". But that
    should happen
<mark>only for the posts with a _100DaysToOffload_ tag</mark> .


## _Day count_ stub at the bottom of each post {#day-count-stub-at-the-bottom-of-each-post}

So I defined a _partial_ that would calculate that Day count, and then
I called that _partial_ at the bottom of my _single_
{{% sidenote %}}
The [_single_](https://gohugo.io/templates/single-page-templates/) template is the template that will be used to render the
content from a single content file, like a post page.. like the page
you are seeing right now. Compared to that, the _list_ template is
used to render "list" type pages like the [home page](/), pages listing all
the [ posts ]({{< relref "posts" >}}), all the [ tags ]({{< relref "tags" >}}), etc.
{{% /sidenote %}} template.

The template file for _single_ pages is typically
`layouts/_default/single.html` in the site or theme directory. Below
snippet is added towards the bottom of that template.

<a id="code-snippet--using-get-post-index-with-tag"></a>
```go-html-template
{{ $day := (partial "get-post-index-with-tag.html"
                    (dict "page" . "tag" "100DaysToOffload")) }}
{{ if (gt $day 0) }}
    {{ printf `<small id="day-counter">This is <strong>Day %d</strong> of <a href="https://100daystooffload.com/">#100DaysToOffload</a>.</small>` $day | safeHTML }}
{{ end }}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--using-get-post-index-with-tag">Code Snippet 1</a>:</span>
  Using the <code>get-post-index-with-tag.html</code> partial
</div>

Explanation of the above snippet:

-   _Partial_ `get-post-index-with-tag.html` is called with a `dict` type
    input (think of a map or associative array) with keys `page` and
    `tag`.
-   This _partial_ will return a number representing the chronological
    index of the current page in a pool of the specified tag.
-   Almost always, you would want to pass the current page's
    context[^fn:1] -- the _dot_ -- to the called _partial_ so that it
    can extract useful metadata from the page's context, like the page
    parameters, content, etc. So the "." or the current page context is
    passed to the partial using the `page` key.
-   The `tag` key is _"100DaysToOffload"_ as we want to know which
    number post is the current post with that specific tag. For example,
    this is the 5th post so far.
-   If the _partial_ returns a non-zero value,
{{% sidenote %}}
    The _partial_ call will return a value of 0 if a page is **not**
    tagged that _"tag"_ key's value.
    {{% /sidenote %}} the _Day count_ line is printed with the shown HTML formatting.


## `get-post-index-with-tag.html` Partial {#get-post-index-with-tag-dot-html-partial}

Below is the definition of the _partial_. It is saved as
`layouts/partials/get-post-index-with-tag.html` in the site or theme
directory.

<a id="code-snippet--defn-get-post-index-with-tag"></a>
```go-html-template { linenos=true, anchorlinenos=true, lineanchors=org-coderef--52750f }
{{ $page := .page }}
{{ $tag := .tag }}
{{ $index := -1 }}
{{ if (in $page.Params.tags $tag) }}
    {{ with site.Taxonomies.tags }}
        {{ $weighted_pages := index . ($tag | lower) }}
        {{ range $idx, $p := $weighted_pages.Pages.ByDate }}
            {{ if (eq $page.Permalink $p.Permalink) }}
                {{ $index = $idx }}
            {{ end }}
        {{ end }}
    {{ end }}
{{ end }}
{{ $index = (add $index 1) }}
{{ return $index }}
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--defn-get-post-index-with-tag">Code Snippet 2</a>:</span>
  Definition of <code>get-post-index-with-tag.html</code> partial
</div>

-   The partial was passed in a map with keys "page" and "tag". We save
    those to local variables `$page` and `$tag`.
-   On [line 3](#org-coderef--52750f-3), a local variable `$index` is
    initialized to -1. Later in the code (line [14](#org-coderef--52750f-14)),
    we increment this variable by 1 and return that. So, if `$tag` is
    not found for a page, `$index` + 1 = 0 is returned.
-   [Line 4](#org-coderef--52750f-4) checks if the current page's _tags_
    front-matter has the `$tag` (which is set to _"100DaysToOffload"_ in
    [Code Snippet 1](#code-snippet--using-get-post-index-with-tag)).
-   In [line 6](#org-coderef--52750f-6), we get an object
    `$weighted_pages` that has a collection or list of all pages with
    the `$tag` tag set. _That list will contain the current page too._
-   In [line 7](#org-coderef--52750f-7), we sort this filtered
    page list `$weighted_pages.Pages` by _date_ using the `.ByDate`
    method, and then loop through that.
{{% sidenote %}}
    Here, we just get a list of **all** the posts with that `$tag`. We
    don't deal with cases like.. "What if I were on my second or later
    attempts of _#100DaysToOffload_ challenge?". I will deal with that
    when I need to, and then probably I will have a followup post for
    that :smile:.
    {{% /sidenote %}} In this line, `$idx` is the loop counter variable, and `$p` will
    hold the page item from the list.
-   In [line 8](#org-coderef--52750f-8), the permalink of each page
    (`$p`) in that page list is compared with the current page's
    permalink. When we find a match, we update the `$index` variable
    with that loop counter variable's value. This match must happen only
    once because all pages have unique permalinks.
{{% sidenote %}}
    I would have liked to use a `break` statement once the first match
    was found, to avoid wasting time going through other pages in the
    list. But the Hugo/Go templating doesn't have that feature.
    {{% /sidenote %}}
-   The list indices begin with 0. So if this were the first page in the
    date sorted list, `$index` would be set to 0. But "Day 0 of 100"
    would sound weird. So in [line 14](#org-coderef--52750f-14), we first
    increment the `$index` value, and then return that.


## Closing {#closing}

That's it!

With that little _partial_ defined and then called in the
_single.html_ layout file, you can see how the automatic Day count
shows up <a href="#day-counter"> below</a>.

Happy Hugo Templating! :tada:

[^fn:1]: You can learn more about Hugo's _dot_ notion and page context
    from [this wonderfully written post](https://www.regisphilibert.com/blog/2018/02/hugo-the-scope-the-context-and-the-dot/) by Regis Philibert.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
