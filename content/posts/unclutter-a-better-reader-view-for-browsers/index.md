+++
title = "Unclutter: A better Reader View for browsers"
author = ["Kaushal Modi"]
description = """
  Unclutter is a better looking _Reader View_ browser add-on available
  for both Firefox and Chrome.
  """
date = 2022-06-12T23:07:00-04:00
lastmod = 2022-06-12T23:07:00-04:00
tags = ["browser", "reader-mode", "100DaysToOffload", "add-on"]
categories = ["web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  firefox = "101.0.1"
  unclutter = "0.15.4"
[syndication]
  mastodon = 108468018565956112
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Why?](#why)
- [Caveat](#caveat)
- [Closing](#closing)

</div>
<!--endtoc-->


I am writing this post to share this wonderful browser add-on
**Unclutter** (available for both Firefox and Chrome) by [Peter Hagen](https://github.com/lindylearn). I
discovered this a few days back on [Hacker News](https://news.ycombinator.com/item?id=31620466). But given how awesome
this add-on is, I am surprised that the HN post didn't gain any
traction.

I'll quickly post the important links related to this add-on, and then
briefly list few points on why I like it.

<div class="org-center">

:point_right: [**Unclutter Homepage**](https://unclutter.lindylearn.io/) | [**Source code**](https://github.com/lindylearn/unclutter) | Add-ons:
[**Firefox**](https://addons.mozilla.org/en-GB/firefox/addon/lindylearn/), [**Chrome**](https://chrome.google.com/webstore/detail/unclutter-ad-blocker-for/ibckhpijbdmdobhhhodkceffdngnglpk) :point_left:

</div>


## Why? {#why}

I use Firefox as my primary browser. If you are too, you might be
wondering "why install this add-on when Firefox already has a [Reader
View](https://support.mozilla.org/en-US/kb/firefox-reader-view-clutter-free-web-pages)".

Here are my reasons:

1.  You do not lose the article's **origin style**!
2.  The _Unclutter_ author Peter Hagen was **super-responsive** when I
    [opened an issue](https://github.com/lindylearn/unclutter/issues/24) on his repo. This add-on did not work well on one
    of the pages on this site, but he fixed it in less than 24 hours!
3.  This website shows an outline on the side, but many websites
    don't. _Unclutter_ adds a similar **outline** for any page where it's
    enabled.
4.  _Unclutter_ can be **auto-enabled** for your chosen sites. For example,
    ever since I have discovered this add-on, I have auto-enabled it on
    <https://www.masteringemacs.org>.
5.  You can customize a **key-binding** or shortcut to toggle this
    add-on. For now, I am keeping the default binding `Alt + C`.
6.  The **subtle animation** you see when enabling/disabling this add-on
    is pretty cool.
7.  Like the Firefox built-in _Reader View_, _Unclutter_ also removes
    distractions -- So **no ads or pop-ups**!

It has other features like annotating pages with your notes privately
and displaying comments from [Hacker News](https://news.ycombinator.com/) and [Hypothes.is](https://web.hypothes.is/), but I don't
use those.


## Caveat {#caveat}

While viewing a page with _Unclutter_ enabled, there are no
issues. But I have noticed that if I toggle it off, the original CSS
gets broken. This issue is [tracked on its repo](https://github.com/lindylearn/unclutter/issues/25). Until this gets fixed,
a quick workaround is to do `Ctrl + F5` (force reload the page and
thus its CSS too) after disabling _Unclutter_.


## Closing {#closing}

But this minor issue doesn't prevent it from making it my default
"reader view" app on Firefox.

I'll end this post with a before and after when visiting [this
page](https://www.masteringemacs.org/article/why-emacs-has-buffers). Feel free to click those images to view them in higher
resolution.

|                                   |                                         |
|-----------------------------------|-----------------------------------------|
| [![](original.png)](original.png) | [![](uncluttered.png)](uncluttered.png) |

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
