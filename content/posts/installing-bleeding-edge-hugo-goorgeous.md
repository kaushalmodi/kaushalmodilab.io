+++
date = "2017-03-07T21:36:13-05:00"
title = "Installing bleeding edge Hugo + Goorgeous"
categories = ["hugo", "org"]
tags = ["bash", "installation", "go"]
+++

**Outdated** post on how to build `hugo` from source with the latest
commit from the _goorgeous_ package.

<!--more-->

**Update (2017/06/16)**

Updated the github source references as per
the changes in `hugo` v0.23[^fn:4]. Also made the shell
script [ShellCheck][7]-passing! :raised_hands: §

---

You already got `go` installed, and that's why are you reading it.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If not -- Go! [Install it!][1]

So.. now that you have `go` installed, why settle for the
release version of [`hugo`][2]! You are reading this blog post
generated using the bleeding edge of `hugo`[^fn:1].

In addition, Hugo v0.19 onwards supports the [*org-mode*][4]
syntax[^fn:2], which is so much more awesome than *markdown*,
especially if you are using emacs.

This post is about how you can install the latest versions of `hugo` and the go
package that adds the *org-mode* support -- [`goorgeous`][3].

## First time install of hugo

When any package gets installed using `go get`, it gets installed
inside the `$GOPATH/src/` directory. We install `hugo` using `go get`
for this *one-and-only* time to get that correct directory structure
in there:

```bash
go get -u -v github.com/gohugoio/hugo
```

## Installing hugo from master

Now you should find the `hugo` source code in
`$GOPATH/src/github.com/gohugoio/hugo/`.

**From here on, run all commands in that directory!&nbsp;**[^fn:3]

To get the latest files from the *master* branch do:
```bash
git fetch --all # fetch new branch names if any
git checkout master
# I also do hard reset as I don't develop any code in go
git reset --hard origin/master
```

### Govendor

Hugo does its package dependency management using [`govendor`][5],
which, not surprisingly, is also a `go` package. (`goorgeous` is one
of these dependent packages.)

So you would install `govendor` like any other `go` package:

```bash
go get -u -v github.com/kardianos/govendor
```

The package dependency database is stored in
`$GOPATH/src/github.com/gohugoio/hugo/vendor/vendor.json`. This JSON
file specifies what other `go` packages need to be installed from
which git repo, using which commit.

Govendor makes installing the right versions of the dependent packages
easy -- Just run the below.

```bash
govendor sync
```

### Latest `goorgeous`

**Note:** If you do not wish to update to the latest version of
`goorgeous`, you can skip this step.

The commit hash for `goorgeous` in that JSON file might not
point to its latest version. But we are interested in getting the
latest-and-greatest *org-mode* support for `hugo` ..

The good news is that `govendor` allows that:

```bash
# Fetch the goorgeous package from its master branch
govendor fetch github.com/chaseadamsio/goorgeous
```

*See the Govendor [Quick Start Guide][6] for frequently used
commands.*

### Static Executable

We want to make the `hugo` executable a **static** executable.

That way it is *100% portable* --- independent of dynamically linked
libraries. This is also how the *Continuous Integration* engines (like
the Gitlab CI Runner that generates this blog) can use this custom
built `hugo` binary without any dynamic library dependency issue.

To enable static building of `go` binaries, these env variables need
to be set:

```bash
export CGO_ENABLED=0
export GO_EXTLINK_ENABLED=0
```

*Note that above (and the snippet that follows) work in a `bash`
shell. Even if your default shell is not `bash`, you can run the [full
`bash` script](#full-build-script) at the end of this post.*

### Building Hugo

We now finally build `hugo`, with some customization so that
running `hugo version` prints the *Build Date* and *Commit Hash*
too.

```bash
package="github.com/gohugoio/hugo"
commithash=$(git rev-parse --short HEAD 2>/dev/null)
builddate=$(date +%FT%T%z)
go install -v \
   -ldflags "-X ${package}/hugolib.CommitHash=${commithash} \
             -X ${package}/hugolib.BuildDate=${builddate}" \
   ${package}
```

This will install the binary as `$GOPATH/bin/hugo`.

*Now don't forget to add `$GOPATH/bin` to your `$PATH`!*

Verify that the binary got built as expected by running `hugo
version`.

> Hugo Static Site Generator v0.24-DEV-411AC930 linux/amd64 BuildDate: 2017-06-16T11:16:50-04:00

## Full Build Script

Here is the full `bash` script. You can save it as `hugo-build.sh` and
it will run just fine on any shell (as long as you haven't removed
`bash` from your system :smile:).

```bash
#!/usr/bin/env bash

here=$(pwd)

package="github.com/gohugoio/hugo"

export CGO_ENABLED=0
export GO_EXTLINK_ENABLED=0

if ! hash govendor 2>/dev/null
then
    go get -u -v github.com/kardianos/govendor
fi

# Install hugo for the first time so that the ${GOPATH}/src/${package}
# directory gets populated.
if [[ ! -d "${GOPATH}/src/${package}" ]] || ( ! hash hugo 2>/dev/null )
then
    go get -u -v ${package}
fi

# Update to hugo master branch
cd "${GOPATH}/src/${package}" || exit
git fetch --all # fetch new branch names if any
git checkout master
# git fetch --all
# Force update the vendor file in case it got changed
git reset --hard origin/master

# Synchronize all the dependent packages as per the just updated vendor file
govendor sync

# Update the goorgeous package to its master branch
# You can comment out the below line if you do not need to fetch the
# latest version of goorgeous.
govendor fetch github.com/chaseadamsio/goorgeous
# govendor fetch github.com/chaseadamsio/goorgeous@=fixNewlineParagraphs

commithash=$(git rev-parse --short HEAD 2>/dev/null)
builddate=$(date +%FT%T%z)
go install -v \
   -ldflags "-X ${package}/hugolib.CommitHash=${commithash} \
             -X ${package}/hugolib.BuildDate=${builddate}" \
   ${package}

echo "Hugo Version Check:"

cd "${here}" || exit
```

[^fn:1]: To see the master branch commit of `hugo` used to build this site, do <kbd>Ctrl</kbd> + <kbd>U</kbd> in your browser and search for *Hugo Commit Hash*.
[^fn:2]: Hugo v0.19 [Release Notes](https://github.com/gohugoio/hugo/releases/tag/v0.19)
[^fn:3]: You need to be in the `${GOPATH}/src/${package}` directory in order to build any `go` `${package}` (unless you are doing a plain `go get`).
[^fn:4]: Hugo v0.23 [Release Notes](https://github.com/gohugoio/hugo/releases/tag/v0.23)

[1]: {{< relref "installing-go-toolchain" >}}
[2]: https://gohugo.io/
[3]: https://github.com/chaseadamsio/goorgeous
[4]: https://orgmode.org/
[5]: https://github.com/kardianos/govendor
[6]: https://github.com/kardianos/govendor#quick-start-also-see-the-faq
[7]: https://github.com/koalaman/shellcheck
