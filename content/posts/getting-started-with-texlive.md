+++
title = "Getting started with texlive"
date = "2014-05-29T17:32:05-04:00"
categories = ["texlive", "latex"]
+++

A list of my frequently used texlive manager commands.

<!--more-->

* `man tlmgr` Get help on TeX Live Manager
* `tlmgr info <PACKAGE>` Display detailed information about _PACKAGE_, such as the installation status and description
* `tlmgr update <PACKAGE>` Install _PACKAGE_
* `tlmgr update --list` Just report what needs to be updated
* `tlmgr update --all` Make the local TeX installation correspond to what is in the package repository
