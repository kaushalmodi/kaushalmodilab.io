+++
title = "Gujarati Transliteration"
author = ["Kaushal Modi"]
description = """
  You can phonetically write a non-English language on an English
  keyword in Emacs, and that transforms into that non-English
  script. This is called [_transliteration_](https://en.wikipedia.org/wiki/Transliteration), and I demonstrate that for
  the Gujarati language in this post.
  """
date = 2022-06-27T18:43:00-04:00
lastmod = 2022-06-27T18:43:00-04:00
series = ["Gujarati in Emacs"]
tags = ["gujarati", "transliteration", "100DaysToOffload"]
categories = ["emacs"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
series_weight = 4002
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108551998050403845
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Enabling transliteration](#enabling-transliteration)
- [Toggling the input method](#toggling-the-input-method)
- [Caveats with Gujarati and other Indic language transliteration](#caveats-with-gujarati-and-other-indic-language-transliteration)
- ["Input method" cheat sheet](#input-method-cheat-sheet)
- [Closing](#closing)
- [References](#gujarati-transliteration-references)

</div>
<!--endtoc-->


Emacs provides the transliteration feature using the
**set-input-method** command. I'll introduce that and few related
functions in this post to get to help get started with transliteration
quickly.


## Enabling transliteration {#enabling-transliteration}

Emacs uses the "input method" feature to do character conversion from
ASCII to the target language or script. The "input method", stored in
`current-input-method`, is _nil_ by default. In this state, you see
the exact ASCII in Emacs buffer, that you typed on the
keyboard[^fn:1].

In this post, my target non-English language is [Gujarati](https://en.wikipedia.org/wiki/Gujarati_language). So I want to
type on my English keyboard and have Gujarati script letters show up
in the buffer.

Emacs provides the `set-input-method` command to change the _current
input method_. This command is bound to <kbd>C-x</kbd> <kbd>RET</kbd>
<kbd>C-\\</kbd> by default. Pick the new input method after calling
that command.

<div class="note">

To see the available input methods, do `M-x list-input-methods`.

</div>

As I want to do Gujarati transliteration, I pick the `gujarati-itrans`
method.

If you don't know Gujarati, don't fret! The commands shown here will
work when transliterating to other languages too --- only the
Gujarati-specific _input method_ `gujarati-itrans` will change to the
input method of your choice.


## Toggling the input method {#toggling-the-input-method}

I often need to switch between the Gujarati and English languages in
the same document. You can see me doing that in this post next section
onwards. The `toggle-input-method` command bound by default to
<kbd>C-\\</kbd> is helpful here.

So if I am already in the "Gujarati transliteration mode" calling this
command will set `current-input-method` back to _nil_. Repeating that
same call will again set `current-input-method` to `gujarati-itrans`,
and I will once again be in the "Gujarati transliteration mode".


## Caveats with Gujarati and other Indic language transliteration {#caveats-with-gujarati-and-other-indic-language-transliteration}

Apologies, but this section is meaningful only if you know how to read
Gujarati. So you can safely skip to the next section.

Below table is a quick glimpse of some nuances in Gujarati
transliteration. I will save my explanation and instead show some of
the mistakes I made in transliteration using examples.

| ASCII input | Gujarati Transliteration | Rough Translation                       |
|-------------|--------------------------|-----------------------------------------|
| _ram_       | રમ્                       | (incorrect spelling, no meaning)        |
| _rama_      | રમ                       | play                                    |
| _raama_     | રામ                      | a popular name Raama (as in Lord Raama) |
| _angreji_   | અન્ગ્રેજિ                   | (incorrect spelling)                    |
| _hu.n_      | હું                        | I                                       |
| _chhu.n_    | છું                        | am                                      |
| _a.ngrejii_ | અંગ્રેજી[^fn:2]             | English (language)                      |
| _Ime.cksa_  | ઈમૅક્સ                     | this literally reads "Emacs"            |


## "Input method" cheat sheet {#input-method-cheat-sheet}

Thankfully Emacs provides full help through the
`describe-input-method` command bound to <kbd>C-h</kbd>
<kbd>C-\\</kbd>
{{% sidenote %}}
If you haven't already noticed the consistency in these bindings, the
default bindings with <kbd>C-\\</kbd> in them are related to "input
method" commands.
{{% /sidenote %}} by default.

For example, `M-x describe-input-method gujarati-itrans` gives this:

<a id="figure--gujarati-itrans-help"></a>

{{< figure src="gujarati-itrans-help.png" caption="<span class=\"figure-number\">Figure 1: </span>Partial screen capture of Gujarati transliteration cheat sheet `C-h C-\ gujarati-itrans`" link="gujarati-itrans-help.png" >}}

The &lowast;Help&lowast; that shows up looks formidable at the first glance. Though,
I found comfort in the fact that roughly half of the key sequences
were obvious and roughly half resulted in Gujarati characters that I
have never found the need of! :smiley:


## Closing {#closing}

Typing this in the "transliteration mode":

> maaru naama kaushala chhe. mane e jaaNii ne aana.nda thaaya chhe ke
> hu.n aa sahelaaI thI lakhI shaku chhu.n. (joDanI-bhula maapha.)

will result in:

મારુ નામ કૌશલ છે. મને એ જાણી ને આનંદ થાય છે કે હું આ સહેલાઈ થી લખી શકુ
છું. (જોડણી-ભુલ માફ.)

_Translation: My name is Kaushal. I am happy knowing that I can write
this easily._


## References {#gujarati-transliteration-references}

-   [Emacs Info: Input Methods](https://www.gnu.org/software/emacs/manual/html_node/emacs/Input-Methods.html "Emacs Lisp: (info \"(emacs) Input Methods\")")
    -   [Emacs Info: Select Input Method](https://www.gnu.org/software/emacs/manual/html_node/emacs/Select-Input-Method.html "Emacs Lisp: (info \"(emacs) Select Input Method\")")
-   `M-x describe-input-method gujarati-itrans`

[^fn:1]: I am assuming an English keyboard here.
[^fn:2]: This spelling might still render incorrectly on your browser
    depending on the unicode character set available for Gujarati on your
    system.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
