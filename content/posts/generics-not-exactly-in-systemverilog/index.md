+++
title = "Generics (not exactly) in SystemVerilog"
author = ["Kaushal Modi"]
description = """
  Using _parameterized classes_ with _static functions_ to make up for
  the lack of generics in SystemVerilog.
  """
date = 2022-02-11T01:48:00-05:00
tags = ["parameterized-classes", "typename", "static", "methods", "generics", "100DaysToOffload"]
categories = ["systemverilog"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  xlm = "21.03-s10"
[syndication]
  mastodon = 107778058388412283
  twitter = 1492147409899728902
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [A real _Generics_ example](#a-real-generics-example)
- [_Poor man's Generics_ in SystemVerilog](#poor-man-s-generics-in-systemverilog)
    - [Parameterized Classes](#parameterized-classes)
    - [Static methods](#static-methods)
    - [Should compile with all types _T_](#should-compile-with-all-types-t)
- [Final Code](#final-code)
- [Cluelib](#cluelib)

</div>
<!--endtoc-->

Many modern languages like [Nim](https://nim-lang.org) allow defining _Generic_ functions
types, etc that can work for any type. See Nim Manual <a href="#citeproc_bib_item_2">2022</a> _Generics_.

This post demonstrates how you can make generics _kind of_ (if you
really squint your eyes, you can see it :smile:) work with
SystemVerilog.


## A real _Generics_ example {#a-real-generics-example}

The focus of this post is not how to write generics in Nim, but here's
a quick summary:

-   [Line 1](#org-coderef--51b0fc-1) overloads the `+` operator for any type `T`.
-   [Line 2](#org-coderef--51b0fc-2) is a **compile time** check (using `when`) to
    see if `T` is a _string_, and then concatenates _a_ and _b_ inputs
    using the `&` _string concat_ operator.
-   For [other types](#org-coderef--51b0fc-4), it does the expected "a + b".

<!--listend-->

<a id="code-snippet--code-generics-nim"></a>
```nim { linenos=true, anchorlinenos=true, lineanchors=org-coderef--51b0fc }
proc `+`[T](a, b: T): T =
  when T is string:
    a & b
  else:
    a + b

echo 1 + 2
echo 100.1 + 100.2
echo "x" + "y" + "z"
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--code-generics-nim">Code Snippet 1</a>:</span>
  Example of generics in Nim
</div>

```text
3
200.3
xyz
```

We will now see how to write something like this in SystemVerilog.


## _Poor man's Generics_ in SystemVerilog {#poor-man-s-generics-in-systemverilog}

SystemVerilog is a strongly typed compiled language. So you need to
define the types for _function_ and _task_ arguments.

So if you need an _add_ function for integers, you would do:

<a id="code-snippet--sv-add-int"></a>
```systemverilog
function int add_int(input int a, b);
  return a + b;
endfunction
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sv-add-int">Code Snippet 2</a>:</span>
  <code>add_int</code> for adding integers
</div>

If you need an _add_ function for real numbers, you would do:

<a id="code-snippet--sv-add-real"></a>
```systemverilog
function real add_real(input real a, b);
  return a + b;
endfunction
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--sv-add-real">Code Snippet 3</a>:</span>
  <code>add_real</code> for adding reals (doubles)
</div>

I am using the **add** functions here only to demonstrate the
concept. In real world code, you would come across many cases where
you would want to avoid such function redefinitions. Some examples
are: dealing with objects of different classes, or queues with
elements of different types (e.g. queues of _ints_, queues of
_strings_), or dynamic arrays of different types, etc.


### Parameterized Classes {#parameterized-classes}

In [`add_int`](#code-snippet--sv-add-int), we see that the type `int` has to be specified in the
function signature. That `int` can be replaced by a _parameter_
representing a _type_ only if that function is defined inside a
_parameterized class_ (See <span style="color:grey;">§</span> **Parameterized classes** <a href="#citeproc_bib_item_1">2018, sec. 8.25</a>).

So it would look like this:

<a id="code-snippet--parameterized-class1-sv"></a>
```systemverilog
class math #(parameter type T = int);
  function T add(input T a, b);
    return a + b;
  endfunction
endclass
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--parameterized-class1-sv">Code Snippet 4</a>:</span>
  Parameterized class and method
</div>

This code, though has a problem --- In order to call that function, we
would first need to construct an object of that class for each type
_T_ we plan to use.


### Static methods {#static-methods}

The solution to that is to declare the methods/functions in the class
as **static**. That way, that method can be called directly without
constructing the object first. See <span style="color:grey;">§</span> **Static methods** <a href="#citeproc_bib_item_1">2018, sec. 8.10</a>.

And because we don't need to construct an object of that class, and
don't want anyone else to unnecessary construct it, we declare the
class as _virtual_.

<a id="code-snippet--parameterized-class2-sv"></a>
```systemverilog { hl_lines=["1","2"] }
virtual class math #(parameter type T = int);
  static function T add(input T a, b);
    return a + b;
  endfunction
endclass
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--parameterized-class2-sv">Code Snippet 5</a>:</span>
  Virtual parameterized class and <b>static</b> method
</div>

Now we can call {{< highlight systemverilog "hl_inline=true" >}}math #(int)::add(1, 2){{< /highlight >}} and {{< highlight systemverilog "hl_inline=true" >}}math #(real)::add(100.1, 100.2){{< /highlight >}} and get the expected results.


### Should compile with all types _T_ {#should-compile-with-all-types-t}

But.. what if the type _T_ is _string_ ..

`$typename` system function (See <span style="color:grey;">§</span> **Type name function** <a href="#citeproc_bib_item_1">2018, sec. 20.6.1</a>) comes to our help here. This function takes in a
variable identifier and returns a string name of that variable's
type. So if a variable _x_ is a _string_, `$typename(x)` will return
`"string"`.

So, we can define the `add` method like this, right?

<a id="code-snippet--parameterized-class3-sv"></a>
```systemverilog { hl_lines=["3","4"] }
virtual class math #(parameter type T = int);
  static function T add(input T a, b);
    if ($typename(a) == "string")
      return {a, b};
    else
      return a + b;
  endfunction
endclass
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--parameterized-class3-sv">Code Snippet 6</a>:</span>
  <code>add</code> method with a condition for <i>string</i> type &#x2013; Won't compile!
</div>

:cross_mark: **Wrong** :cross_mark:

<div class="note">

Even if you have the code execute conditionally based on the
`$typename`, note that the {{< highlight systemverilog "hl_inline=true" >}}if ($typename(a) == "string"){{< /highlight >}} check is happening at run time, and so the
**entire code** needs to compile for any type _T_ that's planned to be
used.

</div>

In the above example, because the `{a, b}` concatenation isn't meant
for types like _ints_ and _reals_, it will fail compilation.
{{% sidenote %}}
That's the reason for the "(not exactly)" in this post's title :smile:
{{% /sidenote %}}

In order to make `add` work with strings as well, we need this messy
_cast_ when the type name is _"string"_ : {{< highlight systemverilog "hl_inline=true" >}}$cast(ret_val, $sformatf("%s%s", a, b));{{< /highlight >}}. It's basically
telling the compiler: "Trust me, I know what I am doing.. I will be
executing this code only when the _ret_val_ is a string."


## Final Code {#final-code}

Finally, here's the entire code with the _generic_ `add` method and
some test code.

<a id="code-snippet--math-sv"></a>
```systemverilog
virtual class math #(parameter type T = int);

  static function T add(input T a, b);
    if ($typename(a) == "string") begin
      T ret_val;
      $cast(ret_val, $sformatf("%s%s", a, b));
      return ret_val;
    end else begin
      return a + b;
    end
  endfunction : add

endclass : math

module test;

    $display("1 + 2 = %p", math #(int)::add(1, 2));                // 3
    $display("1.1 + 2.2 = %p", math #(real)::add(1.1, 2.2));       // 3.3
    $display("1 + 1 = %p", math #(bit)::add(1, 1));                // 0
    $display("abc + def = %p", math #(string)::add("abc", "def")); // "abcdef"

    $finish;
  end
endmodule : test
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--math-sv">Code Snippet 7</a>:</span>
  Parameterized class <code>math</code> with <i>static</i> function <code>add</code>
</div>


## Cluelib {#cluelib}

If you liked how these _"generics"_ work in SystemVerilog and how the
{{< highlight systemverilog "hl_inline=true" >}}math #(int)::add(1, 2));{{< /highlight >}} syntax
looks, check out the [**cluelib**](https://github.com/cluelogic/cluelib) library by _cluelogic.com_. It is an
amazing SystemVerilog library with a big collection of _"generic"_
functions like above for handling strings, queues and dynamic arrays,
and a lot more. You can take a peek at its API [here](http://cluelogic.com/tools/cluelib/api/framed_html/index.html).

## References

<div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>IEEE Standard for SystemVerilog–Unified Hardware Design, Specification, and Verification Language. (2018). <i>IEEE Std 1800-2017 (Revision of IEEE Std 1800-2012)</i>, 1–1315. <a href="https://doi.org/10.1109/IEEESTD.2018.8299595">https://doi.org/10.1109/IEEESTD.2018.8299595</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>Nim contributors. (2022). Nim Manual [Website]. In <i>Nim</i>. <a href="https://nim-lang.org/docs/manual.html">https://nim-lang.org/docs/manual.html</a></div>
</div>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
