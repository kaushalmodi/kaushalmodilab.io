+++
title = "How to quickly create a table in Org mode"
author = ["Kaushal Modi"]
date = 2014-05-28T12:44:06-04:00
tags = ["table"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
+++

Set the buffer major mode to `org-mode`. If the file extension is
`.org`, that major mode would be set automatically by emacs.

Follow the below steps to create a table ..

<!--more-->

1.  Start writing the headers of the table starting with a vertical pipe
    `|`.
    ```text
    | Header1 | Header2 | Header3 |▮
    ```
2.  Once you have finished writing the headers, do `C-u C-c -` to insert
    a horizontal line above the header row.
    ```text
    |---------+---------+---------|
    | Header1 | Header2 | Header3 |▮
    ```
3.  Then do `C-c -` to insert a horizontal line below the header row.
    ```text
    |---------+---------+---------|
    | Header1 | Header2 | Header3 |▮
    |---------+---------+---------|
    ```
4.  Go down one row ( `C-n` ) and hit TAB and org-mode will figure out
    that you need to create a new row and will put the cursor in the
    first cell of the new row.
    ```text
    |---------+---------+---------|
    | Header1 | Header2 | Header3 |
    |---------+---------+---------|
    |▮        |         |         |
    ```
5.  You can now use TAB and Shift + TAB to navigate the cells and new
    rows will be created when you hit TAB when you are in the last cell
    of the last created row.

    When you want to close the table with a bottom border, hit `C-c -`
    when the cursor is in the last row.
    ```text
    |---------+---------+---------|
    | Header1 | Header2 | Header3 |
    |---------+---------+---------|
    | A       | B       | C▮      |
    |---------+---------+---------|
    ```

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
