+++
title = "Time formatting in Go"
author = ["Kaushal Modi"]
description = "A little cheat sheet to help remember the Go time formatting syntax."
date = 2018-06-12T12:21:00-04:00
tags = ["time", "format", "golang"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  go = "1.10.1"
  hugo = "v0.43-DEV-0F1FC01E"
[syndication]
  twitter = 1006638682219204608
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [`Time.Format` Syntax](#time-dot-format-syntax)
- [Hugo's `dateFormat`](#hugo-s-dateformat)
    - [Ordinal dates](#ordinal-dates)
- [Examples](#examples)
- [References](#references)

</div>
<!--endtoc-->

Today on Hugo Discourse, I came across the -- _Why "2006"?_ -- question
regarding the use of `{{ now.Format "2006" }}` in Hugo template. That
template simply prints the current year i.e. the time when that
template was rendered by Hugo.

As I was [answering that](https://discourse.gohugo.io/t/how-do-i-display-the-current-year/1174/12?u=kaushalmodi), I thought that this was a good time to
document this for myself too.. and thus this post.


## `Time.Format` Syntax {#time-dot-format-syntax}

Hugo uses the [Go `Time.Format`](https://golang.org/pkg/time/#Time.Format) _method_[^fn:1] for formatting date and
time strings. But that format specification uses a strange syntax ---
You need to associate 1, 2, 3, 4, 5, 6, 7 with date elements as
follows:

<a id="table--go-time-format-syntax"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--go-time-format-syntax">Table 1</a>:</span>
  Cryptic dates used for Go <code>Time.Format</code>
</div>

| Number | Field Strings                                      | Meaning                |
|--------|----------------------------------------------------|------------------------|
| 1      | 01 (_with optional leading zero_), 1, Jan, January | month                  |
| 2      | 02 (_with optional leading zero_), 2, Mon, Monday  | day                    |
| 3      | 03 (_with optional leading zero_), 3, 15           | hour                   |
| 4      | 04 (_with optional leading zero_), 4               | minute                 |
| 5      | 05 (_with optional leading zero_), 5               | seconds                |
| 6      | 06, 2006                                           | year                   |
| -7     | -0700, -07:00, -07, MST                            | timezone               |
| n/a    | PM (**not AM**)                                    | AM or PM (_uppercase_) |
| n/a    | pm (**not am**)                                    | am or pm (_lowercase_) |
| n/a    | **anything else**                                  | _shows up as it is_    |

You can visualize a formatting string `"Jan 2 15:04:05 2006 MST"` as
below:

```text
Jan 2 15:04:05 2006 MST
  1 2  3  4  5    6  -7
```


## Hugo's `dateFormat` {#hugo-s-dateformat}

The `dateFormat` accepts a date/time format using the same syntax.

Here is the signature of that function:

```text
dateFormat "<FORMAT_STRING>" "<DATE_RFC3339>"
```

-   The `<FORMAT_STRING>` uses one or more date/time fields as shown in
    the "Field Strings" column in [Table 1](#table--go-time-format-syntax). Few
    examples:
    -   _"Jan 2 15:04:05 2006 MST"_
    -   _"2006-01-02"_
    -   _"Jan 2, Mon"_
    -   _"It's 3 O'clock"_
-   The `<DATE_RFC3339>` is a date string compatible with [RFC3339](https://tools.ietf.org/html/rfc3339#section-5.8). Few
    example valid date strings:
    -   _"2017-07-31"_
    -   _"2017-07-31T17:05:38"_
    -   _"2017-07-31T17:05:38Z"_
    -   _"2017-07-31T17:05:38+04:00"_
    -   _"2017-07-31T17:05:38-04:00"_


### Ordinal dates {#ordinal-dates}

In addition, [`humanize`](https://gohugo.io/functions/humanize/) can be used to generate _ordinal_ date, like
"1st", "2nd", "3rd", ..


## Examples {#examples}

{{< timeformattingexamples "2018-06-13T11:30:00-04:00" >}}


## References {#references}

-   gohugohq.com -- [Hugo dateFormat](https://gohugohq.com/howto/hugo-dateformat/)
-   Hugo documentation -- [`dateFormat`](https://gohugo.io/functions/dateformat/)
-   [f\*\*\*inggodateformat](https://github.com/bdotdub/fuckinggodateformat)

[^fn:1]: I am not a Go dev.. let me know if the correct term for that is
    a "function" or a "method" in Go :smile:. **Update** <span class="timestamp-wrapper"><span class="timestamp">&lt;2018-11-08 Thu&gt;</span></span>:
    Thanks to _Mendy_ in comments, I have now fixed referencing
    `Time.Format` as a **method**.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
