+++
title = "Optimize your FontAwesome"
author = ["Kaushal Modi"]
description = "How to trim the FontAwesome JS for your website, using Emacs Lisp."
date = 2018-04-04T15:20:00-04:00
tags = ["FA5", "fontawesome", "javascript", "optimize", "minified"]
categories = ["emacs", "elisp", "web"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  fontawesome = "5.0.8 Pro"
[syndication]
  twitter = 981614503459807232
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [FontAwesome 5 Setup](#fontawesome-5-setup)
- [Problem](#problem)
- [Solution](#solution)
- [Code](#code)
- [Result](#result)

</div>
<!--endtoc-->

This post briefly goes through the FontAwesome 5 setup, the problem
statement and a solution for that.

I signed up for the [FontAwesome 5 Pro Kickstarter](https://www.kickstarter.com/projects/232193852/font-awesome-5), and I have to say
that this is one of my few Kickstarter fundings that delivered really
well!

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;_Thank you FontAwesome!_<br />

</div>


## FontAwesome 5 Setup {#fontawesome-5-setup}

FontAwesome 5 supports an awesome new way of loading icons, using
[SVG with JS](https://fontawesome.com/get-started/svg-with-js). The benefit is that you simply load one
`fontawesome-all.js` file and _everything just works_!.

<a id="code-snippet--fa-setup"></a>
```html
<head>
  <!--load everything-->
  <script defer src="/static/fontawesome/fontawesome-all.js"></script>
</head>
<body>
  <!--icon-->
  <i class="fas fa-user"></i>
</body>
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--fa-setup">Code Snippet 1</a>:</span>
  Fontawesome 5 Setup
</div>


## Problem {#problem}

With FontAwesome 5 Pro, I get about 2700 icons packed in that
`fontawesome-all.js`. While that is great, there are 2 issues:

1.  I don't need all of those 2700 icons on my site. That file is
    roughly 1.7 **MB** (minified or not) which I need not unnecessarily
    load for all my visitors.
2.  Anyone can easily pirate the whole JS file and use it for free!


## Solution {#solution}

The solution is simple --- I just comment out all the lines with SVG
code for the icons that I don't use.

But searching for the icons I need in all the icon packs (Solid,
Regular, Light, Brand) in that single 5k+ line JS file is not fun,
especially when I want to add/remove icons occasionally.

And so `fontawesome-choose` was born!

-   I list the icons I need in `fontawesome-choose-icons`.
-   Run `M-x fontawesome-choose` in the `fontawesome-all.js` file
    buffer.

The code is in the next section, but you can also find the latest
version [in my repo](https://github.com/kaushalmodi/.emacs.d/blob/master/elisp/misc/fontawesome-choose.el).

_Note that while I have used this on Fontawesome 5 Pro, it should work
just as well on Fontawesome 5 Free too._


## Code {#code}

<a id="code-snippet--fontawesome-choose"></a>
```emacs-lisp
;; Helper function `fontawesome-choose' used to uncomment only the
;; icons the user cares about in `fontawesome-all.js'.

(defconst fontawesome-choose-icons '("list-alt" ;categories
                                     "tags"
                                     "rss"
                                     "link"
                                     "heart" ;like
                                     "reply"
                                     "retweet"
                                     "github" ;"github-alt" "github-square"
                                     "twitter" ;"twitter-square"
                                     "gitlab")
  "List of icons to choose from fontawesome-all.js.
Used in `fontawesome-choose'.")

(defun fontawesome-choose ()
  "Comment out all icons in fontawesome-all.js except the selected few.

Minifying the resultant .js will then remove the commented icons,
thus drastically reducing the minified JS size.

Set the `fontawesome-choose-icons' variable to the list of icons
that you want to keep uncommented."
  (interactive)
  (let ((case-fold-search nil)
        (count 0))
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "^var icons" nil :noerror)
        (let ((begin (progn
                       (forward-line 1)
                       (point)))
              end)
          (re-search-forward "^\\};")
          (forward-line 0)
          (backward-char 1)
          (setq end (point))
          ;; First comment all the lines
          (save-excursion
            (narrow-to-region begin end)
            (goto-char (point-min))
            (while (re-search-forward "^\\s-*\\(\"\\)" nil :noerror)
              (replace-match "// \\1" nil nil nil 1))
            (widen))
          ;; Now uncomment only the selected icons
          (save-excursion
            (narrow-to-region begin end)
            (goto-char (point-min))
            (let* ((icon-regexp (regexp-opt fontawesome-choose-icons 'symbols))
                   (regexp (format "^\\s-*\\(//\\s-*\\)\"%s\":" icon-regexp)))
              (while (re-search-forward regexp nil :noerror)
                (replace-match "" nil nil nil 1)
                (setq count (1+ count))))
            (widen))))
      (message "fontawesome-choose: Uncommented %d icons matching %S"
               count fontawesome-choose-icons))))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--fontawesome-choose">Code Snippet 2</a>:</span>
  <code>fontawesome-choose</code> &#x2014; Comment out all the FontAwesome icons you don't need
</div>


## Result {#result}

This solution solves the first problem wonderfully --- After
minification, `fontawesome-all.js` is just 71 **KB** (down from 1.7
**MB**).

That's a 25x factor reduction! The minification step is important
because that removes all the commented lines from the JS. I do the
minification using [`tdewolff/minify`](https://github.com/tdewolff/minify).

The second problem, that of piracy, is difficult to solve completely
using this. But whoever pirates this _reduced_ FontAwesome from my
site won't get the real deal :sunglasses:.

Emacs-Lisp saves the day once again!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
