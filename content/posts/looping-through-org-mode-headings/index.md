+++
title = "Looping through Org mode headings"
author = ["Kaushal Modi"]
description = """
  Using the `org-map-entries` API to loop through selected or all
  headings in an Org file.
  """
date = 2022-05-18T23:29:00-04:00
tags = ["looping", "100DaysToOffload"]
categories = ["emacs", "org", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
  org = "release_9.5.3-504-gcdbb1c"
[syndication]
  mastodon = 108326581975012446
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [`org-map-entries` API](#org-map-entries-api)
    - [_MATCH_ strings](#match-strings)
    - [Comparison Types](#comparison-types)
    - [Other notes](#other-notes)
- [Example: Modifying a property in all headings](#example-modifying-a-property-in-all-headings)
- [`org-map-entries` References](#org-map-entries-references)

</div>
<!--endtoc-->


[Below question](https://framapiaf.org/@postroutine/108313152514542145) on Mastodon by the user [@postroutine](https://framapiaf.org/@postroutine) inspired me to
write this post:

> I got a lot of Org-Mode headings and I want to modify their properties
> (add, remove, edit). Is there a function to do the same modifications
> on each heading?

I think that the best solution to that question is using the
**org-map-entries** function.

But somehow when replying to that question then, that wasn't what
first came to my mind! .. when ironically that function is the [main
function](https://github.com/kaushalmodi/ox-hugo/blob/2b169e5e83d608e80f4faee9d681b98d87041f58/ox-hugo.el#L4781-L4788) that enables my preferred _subtree-based flow_ in `ox-hugo`
:laughing:. So I am writing this post to better ingrain the following
concept in myself ..

<div class="note">

If you need to loop through headings in an Org buffer, and especially
if you **need to modify that buffer** in the process, use
**org-map-entries**[^fn:1].

</div>

Next,

1.  I will give a give introduction to the `org-map-entries` API.
2.  Then provide a super-short solution to the above question.


## `org-map-entries` API {#org-map-entries-api}

I will give only a broad level overview on how to use this function. I
would encourage the reader to refer to the resources at the end of
this post to learn more about it.

So let's start by looking at this function's signature:

<a id="code-snippet--org-map-entries-signature"></a>
```emacs-lisp
(org-map-entries FUNC &optional MATCH SCOPE &rest SKIP)
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--org-map-entries-signature">Code Snippet 1</a>:</span>
  Signature of the <code>org-map-entries</code> function
</div>

The `org-map-entries` function iterates through all the headings
meeting the _MATCH_ criteria in the determined _SCOPE_, and then calls
the specified function _FUNC_ at each of those headings.

-   The `FUNC` function accepts **no** arguments and is called at the
    beginning of each Org heading.
-   The optional second argument _MATCH_ is either _nil_, `t` or a
    _search string_.
    -   If _MATCH_ is _nil_ or `t`, **all** headings will be visited by the
        iteration and _FUNC_ will be called on all of them.
    -   But if _MATCH_ is a string, the headings will first be filtered
        based on that string and then the _FUNC_ will be called on only
        those.
-   For explanations on the optional _SCOPE_ and _SKIP_ arguments, see
    [Org Info: Using the Mapping API](https://orgmode.org/manual/Using-the-Mapping-API.html "Emacs Lisp: (info \"(org) Using the Mapping API\")") or <kbd>C-h</kbd> <kbd>f</kbd> `org-map-entries` from
    within Emacs.

Here's a typical `org-map-entries` call that loops through **all** the
headings in the **visible** buffer: {{< highlight emacs-lisp "hl_inline=true" >}}(org-map-entries #'some-function){{< /highlight >}} where all the optional
argument values are _nil_. Next, we'll see some examples of
string-type _MATCH_ arguments used for filtering the headings.


### _MATCH_ strings {#match-strings}

Below table shows few examples of match string patterns.

<a id="table--org-map-entries-search-strings"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--org-map-entries-search-strings">Table 1</a>:</span>
  String-type <i>MATCH</i> argument examples for <code>org-map-entries</code>
</div>

| Search string                        | Description                                            | Example                                                                                  |
|--------------------------------------|--------------------------------------------------------|------------------------------------------------------------------------------------------|
| **`"TAG"`**                          | Tag name                                               | `"foo"` matches all headings with that tag                                               |
| **`"{TAG REGEXP}"`**                 | Regexp matching tags                                   | `"{f.*}"` matches all headings whose tags match that regexp                              |
| **`"TAG1+TAG2+.."`**                 | Tag set intersection                                   | `"foo+bar"` matches all headings with both of those tags                                 |
| **`"TAG1-TAG2+.."`**                 | Tag set difference                                     | `"foo-bar"` matches all headings with `foo` tag but without `bar` tag                    |
| **`"TAG1`&vert;​`TAG2`&vert;​`.."`**   | Tag set union or boolean _OR_                          | `"foo`​&vert;​`bar"` matches all headings with either of those tags                        |
| **`"TAG1&TAG2&.."`**                 | Tag set intersection or boolean _AND_                  | `"foo&bar"` is same as `"foo+bar"`                                                       |
| **`"PROP`​=​`\"STRVAL\""`**            | Specified property value matching a string             | `"color`​=​`\"blue\""` matches all headings where `color` property is `blue`               |
| **`"PROP<>\"STRVAL\""`**             | Specified property value not matching a string         | `"color<>\"blue\""` matches all headings where `color` property is not `blue`            |
| **`"PROP`​=​`{VAL REGEXP}"`**          | Specified property value matching a regexp             | `"color={b.*}"` matches all headings where `color` property value matches '`b.*`' regexp |
| **`"PROP[OP]NUMVAL"`**               | Specified property value compared with a numeric value | `"some_num`​&gt;=​`10"` matches all headings where `some_num` property is &gt;=10          |
| **`"LEVEL[OP]VAL"`**                 | Check value of headline's special property _LEVEL_     | `"level>2"` matches all headlines at levels greater than 2                               |
| **`"TODO[OP]\"STRVAL\""`**           | Check value of headline's _TODO_ state                 | `"TODO`​=​`\"DONE\""` matches all headlines with _TODO_ state set to 'DONE'                |


### Comparison Types {#comparison-types}

-   If the comparison value is a plain number, a numerical comparison is
    done, and the allowed operators are '&lt;', '=​', '&gt;', '&lt;=​', '&gt;=​',
    and '&lt;&gt;'.
-   If the comparison value is enclosed in double quotes, a string
    comparison is done, and the same operators are allowed.
-   If the comparison value is enclosed in curly braces, a regexp match
    is performed. For this comparison, only '=​' (regexp matches) and
    '&lt;&gt;' (regexp does not match) operators are allowed.
-   Comparison with dates and [Group Tags](https://orgmode.org/manual/Tag-Hierarchy.html "Emacs Lisp: (info \"(org) Tag Hierarchy\")") is also possible. See
    [Org Info: Matching tags and properties](https://orgmode.org/manual/Matching-tags-and-properties.html "Emacs Lisp: (info \"(org) Matching tags and properties\")") for more details.


### Other notes {#other-notes}

-   The property names are case-insensitive. So these all work the
    same: `"COLOR<>\"blue\""`, `"color<>\"blue\""`,
    `"Color<>\"blue\""`.
-   The "tag" and "property" matches can be mixed up using the boolean
    '`&`', '`|`', '`+`' and '`-`​' operators. So searching
    '`+LEVEL=3+boss-TODO​="DONE"`' lists all level three headlines
    that have the tag 'boss' and are <span class="underline">not</span> marked with the TODO
    keyword 'DONE'.
-   '`&`' binds more strongly than '`|`'.
-   Grouping of match expressions using parentheses is not
    supported.


## Example: Modifying a property in all headings {#example-modifying-a-property-in-all-headings}

Below is an example solution to the [Mastoson question](https://framapiaf.org/@postroutine/108313152514542145) that I
referenced in the beginning of this post.

<a id="code-snippet--set-props-all-headings"></a>
```emacs-lisp
(defun test/set-property-at-heading ()
  "Function to be called at the beginning of an Org heading."
  (let ((el (org-element-at-point)))
    (org-set-property "foo" (org-element-property :title el))))
(org-map-entries #'test/set-property-at-heading)
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--set-props-all-headings">Code Snippet 2</a>:</span>
  Dummy example showing how to set a property for all Org headings using <code>org-map-entries</code>
</div>

-   It defines a function that parses the Org element at point using
    `org-element-at-point`, gets the `title` property of the element
{{% sidenote %}}
    This function is designed to be called by `org-map-entries` and so
    the point at the time of calling this function will always be on a
    heading.
    {{% /sidenote %}} , and sets that to the _headline_ element's `foo` property.
-   The `org-map-entries` call now simply calls this function on each
    heading in the visible scope of the Org buffer.


## `org-map-entries` References {#org-map-entries-references}

-   <kbd>C-h</kbd> <kbd>f</kbd> `org-map-entries`
-   [Org Info: Using the Mapping API](https://orgmode.org/manual/Using-the-Mapping-API.html "Emacs Lisp: (info \"(org) Using the Mapping API\")")
-   [Org Info: Matching tags and properties](https://orgmode.org/manual/Matching-tags-and-properties.html "Emacs Lisp: (info \"(org) Matching tags and properties\")")

[^fn:1]: Org mode has another popular mapping/looping API function
    **org-element-map**. I won't go into much detail about that in this post
    --- I'll just mention that `org-element-map` is not the best choice if
    you need to modify the original Org buffer. It's main use is to loop
    through a parsed <abbr aria-label="Abstract Syntax Tree" tabindex=0>AST</abbr> of an Org buffer
    and optional modify those elements _in memory_.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
