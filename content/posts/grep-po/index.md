+++
title = "grep -Po"
author = ["Kaushal Modi"]
description = "Using `grep` to do substring extraction in shell scripts."
date = 2022-02-16T21:34:00-05:00
tags = ["grep", "regex", "string", "perl", "100DaysToOffload"]
categories = ["unix", "shell"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  grep = "GNU grep 2.20"
[syndication]
  mastodon = 107811040216127880
  twitter = ""
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Problem statement](#grep-po-problem-statement)
- [Solution using `grep -Po`](#solution-using-grep-po)
- [Arriving to this solution](#arriving-to-this-solution)
- [Summary](#summary)

</div>
<!--endtoc-->

I like [regular expressions](https://en.wikipedia.org/wiki/Regular_expression)
{{% sidenote %}}
I recommend using <https://regex101.com/> to practice regular
expressions of different flavors (PCRE2, PCRE, Python, etc.) whether
or not you are new to using <abbr aria-label=" regular expression" tabindex=0>regex</abbr>.
{{% /sidenote %}} as they allow me to be concise and specific about what I need to
search.

And I have liked using regular expressions for many years, ever since
I learned Perl about fifteen years back. I am writing this post as I
am remembering the delight I felt when I realized that I can use the
familiar Perl regular expressions to do string parsing in shell
scripts. I am not exactly sure, but I probably learned about this
`grep -Po` trick from _stackexchange_ (<a href="#citeproc_bib_item_1">camh, 2011</a>).


## Problem statement {#grep-po-problem-statement}

I could be parsing a log file with a line like `web report:
https://foo.bar/detail.html` and I need to extract the
`https://foo.bar` part to a shell script variable.


## Solution using `grep -Po` {#solution-using-grep-po}

<div class="note">

This solution requires a GNU `grep` version supporting `-P`, that's
compiled with `libpcre`.
{{% sidenote %}}
_GNU grep_ gained the PCRE (`-P`) feature back [in 2000](https://git.savannah.gnu.org/cgit/grep.git/commit/?id=05860b2d966701a5a9f70a650d32b30ae2612eeb).
{{% /sidenote %}} Also I have never come across a system or
used one that did not have such a `grep` version installed.

</div>

I'll throw the solution out here and then dig into the details.

<a id="code-snippet--grepPo-example"></a>
```shell
echo "def\nabc" | grep -Po 'a\K.(?=c)' # => b
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--grepPo-example">Code Snippet 1</a>:</span>
  Extracting "b" from "abc" using <code>grep -Po</code>
</div>

The _grep_ switches used here are:

`-P`
: Use (P)erl regular expressions. This allows us to use the
    [_look around_ regex](https://www.regular-expressions.info/lookaround.html) syntax like `(?=..)` and special characters like
    `\K` (<a href="#citeproc_bib_item_2">“perlre - Perl regular expressions,” n.d.</a>).

`-o`
: Print only the matched portion to the (o)utput


## Arriving to this solution {#arriving-to-this-solution}

Now I'll start with a basic example and build up to the [above
solution](#code-snippet--grepPo-example).

Problem
: Let's say I have this text with two lines "def" and "abc"
    and I want<span class="org-target" id="org-target--wanted-grep-output"></span> to output whatever character is between "a" and "c".

<!--listend-->

-   Below, the regular expression for matching any character between "a"
    and "c" ( `'a.c'` ) is correct, but that will output the whole input
    because the _grep_ of that regex succeeded.
    ```shell
    echo "def\nabc" | grep 'a.c' # => def\nabc
    ```
-   Now we add the _grep_ `-o` switch so that it outputs only the
    matched portion. As the regex is `'a.c'`​, the `-o` switch will
    output every part of the input that matched that. So the output is
    "abc". It's still not what we [wanted](#org-target--wanted-grep-output).
    ```shell
    echo "def\nabc" | grep -o 'a.c' # => abc
    ```
-   Now we bring in the powerful Perl regex feature _positive
    lookahead_.
{{% sidenote %}}
    Positive lookahead is used when you want to match something <span class="underline">only
    if</span> it's followed by something else. It's syntax looks like `q(?=u)`
    where that expression matches if a `q` is followed by a `u`, without
    making the `u` part of the match -- [reference](https://www.regular-expressions.info/lookaround.html).
    {{% /sidenote %}} But this is still not exactly what we want because "a" is still
    considered as part of the match. Now the output is "ab".
    ```shell
    echo "abc" | grep -Po 'a.(?=c)' # => ab
    ```
-   We only need a special character that marks a point in the regex
    that tells "don't consider anything before this as part of the
    match". The `\K` special construct described in the [Perl regular
    expressions doc](https://perldoc.perl.org/perlre#Lookaround-Assertions) as:

    > There is a special form of this construct, called `\K` (available
    > since Perl 5.10.0), which causes the regex engine to "keep"
    > everything it had matched prior to the `\K` and not include it in
    > matched string. This effectively provides non-experimental
    > variable-length lookbehind of any length.

    And, thus we have the final solution:
    ```shell
    echo "abc" | grep -Po 'a\K.(?=c)' # => b
    ```


## Summary {#summary}

Taking the example from the [problem statement](#grep-po-problem-statement), this will work:

```bash
string="web report: https://foo.bar/detail.html"
substring=$(grep -Po 'web report:\s*\K.*?(?=/detail\.html)' <<< "${string}")
echo "${substring}"
```

```text
https://foo.bar
```

## References

<div class="csl-bib-body">
  <div class="csl-entry"><a id="citeproc_bib_item_1"></a>camh. (2011). Can grep output only specified groupings that match? [Website]. In <i>Unix stackexchange</i>. <a href="https://unix.stackexchange.com/a/13472/57923">https://unix.stackexchange.com/a/13472/57923</a></div>
  <div class="csl-entry"><a id="citeproc_bib_item_2"></a>perlre - Perl regular expressions. (n.d.). [Website]. In <i>Perldoc 5.34.0</i>. Retrieved February 16, 2022, from <a href="https://perldoc.perl.org/perlre">https://perldoc.perl.org/perlre</a></div>
</div>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
