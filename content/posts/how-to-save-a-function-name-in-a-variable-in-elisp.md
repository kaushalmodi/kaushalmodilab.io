+++
title = "How to save a function name in a variable in elisp?"
date = "2014-03-12T15:53:31-04:00"
categories = ["emacs", "elisp", "function", "variable", "theme"]
+++

I have a couple of theme packages installed on my emacs but I would
like to assign a default theme.

I set the [zenburn theme][3] via a function `zenburn`. I set the
[leuven theme][4] via another function `leuven`. But in my emacs
startup I didn't want to hard-code either of these function and thus
arose the need to set a variable to one of these functions.

<!--more-->

*You assign the function to a variable using `defvar` and you call
that function linked to that variable using `funcall`.*

```elisp
(defvar default-theme 'zenburn)

;; zenburn
(defun zenburn ()
  "Activate zenburn theme."
  (interactive)
  ;; disable other themes before setting this theme
  (disable-theme 'leuven)
  (load-theme 'zenburn t))

(funcall default-theme) ;; Set the default theme
```

You can check out my full emacs config for visual settings on
my [git][1].

[Reference][2]


[1]: https://github.com/kaushalmodi/.emacs.d/blob/master/setup-files/setup-visual.el
[2]: https://stackoverflow.com/questions/9942675/in-elisp-how-do-i-put-a-function-in-a-variable
[3]: https://github.com/bbatsov/zenburn-emacs
[4]: https://github.com/fniessen/emacs-leuven-theme
