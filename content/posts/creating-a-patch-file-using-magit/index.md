+++
title = "Creating a patch file using Magit"
author = ["Kaushal Modi"]
description = "Quick tip on how to create _git_ patch files in Emacs using Magit."
date = 2022-05-08T08:48:00-04:00
tags = ["git", "magit", "100DaysToOffload"]
categories = ["emacs"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108266424037164573
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Single-file patch](#single-file-patch)
- [Multi-file patch](#multi-file-patch)
- [More Resources](#more-resources)

</div>
<!--endtoc-->

Recently I came across few instances where people were asking
questions related to creating patches for contributions to Emacs and
Org mode repos [here](https://www.reddit.com/r/emacs/comments/udjk8l/how_do_you_actually_send_pull_requests_in/) and then [here](https://github.com/kaushalmodi/ox-hugo/discussions/618#discussioncomment-2690410). I was in the same shoes back then
when I was about to make my first contribution to Emacs. And so I
thought of sharing this tip on how to use Magit to create patch files.

If you have been using both Emacs and git, you might have already
heard about the awesome [Magit](https://magit.vc/) package. If you haven't :astonished:,
check out [this screenshot-annotated review of what Magit is](https://emacsair.me/2017/09/01/magit-walk-through/). With that
out of the way, and assuming that you already have it installed
{{% sidenote %}}
{{< highlight emacs-lisp "hl_inline=true" >}}(use-package magit :ensure t){{< /highlight >}}
{{% /sidenote %}} , here's how to create a patch file using Magit ..


## Single-file patch {#single-file-patch}

1.  Commit your changes to the git repo first.
2.  Bring up the **Magit Log** view. From the Magit status buffer, you
    would type <kbd>l</kbd> <kbd>l</kbd> to show the log of the current branch.
3.  Move the point to the commit that you want to send as a patch file,
    and hit <kbd>W</kbd> <kbd>c</kbd> <kbd>c</kbd> <kbd>RET</kbd>.
    -   The last <kbd>RET</kbd> selects the commit the point is on, in the
        &lowast;magit-log&lowast; buffer.
    -   If the first line of the commit log of the selected commit is
        "Update docstrings for shortdoc.el", you'll see a patch file
        named `0001-Update-docstrings-for-shortdoc.el.patch` created in
        your git repo root.
    -   You can now email this patch file
<mark>as an attachment</mark> to [bug-gnu-emacs@gnu.org](mailto:bug-gnu-emacs@gnu.org) (if contributing to Emacs) or to
        [emacs-orgmode@gnu.org](mailto:emacs-orgmode@gnu.org) (if contributing to Org mode).


## Multi-file patch {#multi-file-patch}

If you need to create a multi-file patch i.e. patch files for a series
of commits, select those commits in the &lowast;magit-log&lowast; buffer
{{% sidenote %}}
The commit selection process is the same as how you would select
text in any Emacs buffer. For example, if I want to create a series
of 5 patches, I would go to the latest commit in the series, hit
<kbd>C-SPC</kbd> and then <kbd>C-n</kbd> 4 times to select 5 rows of commits.
{{% /sidenote %}} , and then use the same <kbd>W</kbd> <kbd>c</kbd> <kbd>c</kbd> binding.


## More Resources {#more-resources}

Here are the official contribution guides for Emacs and Org mode:

-   [How to contribute to Emacs -- Emacs CONTRIBUTE document](https://git.savannah.gnu.org/cgit/emacs.git/tree/CONTRIBUTE)
-   [How to contribute to Org -- Worg](https://orgmode.org/worg/org-contribute.html)

Here are some more resources that got shared in the _Emacsverse_
recently (within the past year as of writing this):

1.  <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-04-23 Sat&gt; </span></span> [Contributing patches to Org -- Ihor Radchenko](https://lists.gnu.org/r/emacs-orgmode/2022-04/orgYGCOr0hBKH.org)
2.  <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-04-09 Sat&gt; </span></span> [Primer on formatting Git patches with Emacs (Magit) -- Protesilaos Stavrou](https://protesilaos.com/codelog/2022-04-09-simple-guide-git-patches-emacs/)
3.  <span class="timestamp-wrapper"><span class="timestamp">&lt;2021-08-17 Tue&gt; </span></span> [Contributing to Emacs -- Colin Woodbury](https://www.fosskers.ca/en/blog/contributing-to-emacs)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
