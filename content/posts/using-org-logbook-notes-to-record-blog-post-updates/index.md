+++
title = "Using Org Logbook Notes to record blog post updates"
author = ["Kaushal Modi"]
description = """
  Quick introduction to Org mode's `:LOGBOOK:` feature and how I use it
  to record time-stamped notes for blog post updates.
  """
date = 2022-05-16T00:59:00-04:00
tags = ["100DaysToOffload", "logbook"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  org = "release_9.5.3-504-gcdbb1c"
[syndication]
  mastodon = 108309992626484672
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Prior forms of adding post updates](#prior-forms-of-adding-post-updates)
- [Introducing `:LOGBOOK:`](#introducing-logbook)
- [Adding notes to `:LOGBOOK:`](#adding-notes-to-logbook)
- [`:LOGBOOK:` Notes Example](#logbook-notes-example)
- [References](#references)

</div>
<!--endtoc-->


Most of my blog posts are mainly to serve as documentation for my
future self
{{% sidenote %}}
This post will serve to remind me how to get the `:LOGBOOK:` notes
working once again in case I end up with some issue there.
{{% /sidenote %}} . So when I get a chance, I try to fix outdated stuff in
my old blog posts. And along with the act of updating things, adding
brief notes describing those updates comes naturally to me.


## Prior forms of adding post updates {#prior-forms-of-adding-post-updates}

As I author my posts in Org mode, I can easy enter a date stamp using
the `org-time-stamp` command (bound by default to <kbd>C-c</kbd> <kbd>.</kbd> <kbd>RET</kbd>) and
follow that by the update note.

While that worked, that approach bothered me because those notes
didn't have consistent format across multiple posts. For example, I
might type the update as "{{< highlight org "hl_inline=true" >}}*Update (<time stamp>)*: <note>{{< /highlight >}}" in one post, while I might type the same in a
_description list_ form in another: "{{< highlight org "hl_inline=true" >}}- <time stamp>) :: <note>{{< /highlight >}}".

To solve the consistency problem, I came up with this Org macro:

<a id="code-snippet--update-org-macro"></a>
```org
#+macro: update - $1 :: $2
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--update-org-macro">Code Snippet 1</a>:</span>
  <code>{{{update(..)}}}</code> Org Macro
</div>

This worked mostly ... except when the update text needed to be a bit
longer, like a paragraph. It didn't look elegant in that case. Also,
if the text had a _comma_ character in there, it needed to be escaped
with a backslash (`\`).


## Introducing `:LOGBOOK:` {#introducing-logbook}

So when I learned
{{% sidenote %}}
A little bit of history .. I learned about the `:LOGBOOK:` drawer when
Adam Porter mentioned it in [`ox-hugo` Issue # 203](https://github.com/kaushalmodi/ox-hugo/issues/203) back in
September 2018. I wanted to use that feature, but I didn't have time
and/or know-how on how exactly I would parse those Org Drawers in
`ox-hugo` until very recently (May 2022)!
{{% /sidenote %}} about the Org `:LOGBOOK:` drawer, it solved all those problems: (i)
consistency in adding notes (ii) easy to add update notes -- in fact
much easier (iii) easy to type long form notes (iv) no comma escaping
needed.

Org Drawers look like this:

<a id="code-snippet--code-org-drawers"></a>
```org
Content before the drawer
:DRAWERNAME:
Content inside the drawer
:END:
Content after the drawer
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--code-org-drawers">Code Snippet 2</a>:</span>
  Org Drawers
</div>

and they can be inserted anywhere in your Org content using the
`org-insert-drawer` command (bound by default to <kbd>C-c</kbd> <kbd>C-x</kbd> <kbd>d</kbd>).

`:LOGBOOK:` is a special kind of drawer that's auto-inserted by Org
mode when certain actions are detected, like changing the TODO state
of a subtree, or
<mark>adding a note</mark> . That latter action is what this blog post is about.


## Adding notes to `:LOGBOOK:` {#adding-notes-to-logbook}

You need to enable this feature using one of these methods:

1.  Set the `org-log-into-drawer` variable to a non-nil value
    (typically `t`) in your Emacs config, or as a file-local variable,
    or in your project's **`.dir-locals.el`** (:star: my preference).
2.  Set `#+startup: logdrawer` to enable this for the whole Org file.
3.  To enable this feature for only selected subtrees, set the
    `:LOG_INTO_DRAWER: t` property in the subtree (or one of its parent
    subtrees).

Once this is set, call the `org-add-note` command (bound by default to
<kbd>C-c</kbd> <kbd>C-z</kbd>). That will open a window with &lowast;Org Note&lowast; buffer where-in
you will type your post update and then <kbd>C-c</kbd> <kbd>C-c</kbd> to save it to the
subtree's `:LOGBOOK:` drawer. If that drawer didn't exist already, it
will be created directly under the subtree's heading.

The note will get recorded in this format by default under the current
subtree:

<a id="code-snippet--logbook-default-format"></a>
```org
* Subtree title
:LOGBOOK:
- Note taken on <current date and time> \\
  <note text>
:END:
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--logbook-default-format">Code Snippet 3</a>:</span>
  Default format of a <code>:LOGBOOK:</code> note
</div>

As you see, you only type the note text, and the time-stamp is
inserted automatically.


## `:LOGBOOK:` Notes Example {#logbook-notes-example}

Here are the update notes from one of my posts:

<a id="code-snippet--logbook-example"></a>
```org
:LOGBOOK:
- Note taken on <2018-08-26 Sun> \\
  Mention =org-babel-demarcate-block=, tweak the =org-meta-return= advice.
- Note taken on <2018-08-23 Thu> \\
  Use ~M-return~ instead of ~C-return~ for splitting blocks and
  support upper-case blocks (though I [[* Converting Org keywords to lower-case][don't prefer those]]!).
:END:
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--logbook-example">Code Snippet 4</a>:</span>
  Examples of post update notes added to the <code>:LOGBOOK:</code> drawer
</div>

You can see how they rendered at the top of the [Splitting an Org block into two]({{< relref "splitting-an-org-block-into-two" >}}) post.

If you are an `ox-hugo` user following the _subtree-based export
flow_, and would like to export `:LOGBOOK:` notes in a similar
fashion, check out the [`ox-hugo` Manual: Drawers](https://ox-hugo.scripter.co/doc/drawers/) page for details.


## References {#references}

-   [Org Info: Drawers](https://orgmode.org/manual/Drawers.html "Emacs Lisp: (info \"(org) Drawers\")")
-   [Org Info: Tracking TODO state changes](https://orgmode.org/manual/Tracking-TODO-state-changes.html "Emacs Lisp: (info \"(org) Tracking TODO state changes\")")

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
