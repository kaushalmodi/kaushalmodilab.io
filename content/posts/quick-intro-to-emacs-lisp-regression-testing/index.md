+++
title = "Quick Intro to Emacs Lisp Regression Testing"
author = ["Kaushal Modi"]
description = """
  A quick introduction to using ERT (Emacs Lisp Regression Testing) for
  your next _elisp_ library.
  """
date = 2022-03-06T14:40:00-05:00
tags = ["ert", "testing", "regression", "100DaysToOffload", "makefile"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.0.91"
[syndication]
  mastodon = 107911479520750891
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Test directory](#test-directory)
- [Test Files](#test-files)
    - [Feature-specific `t***.el` test files](#feature-test)
    - [Wrapper test file `all-tests.el`](#wrapper-test-file-all-tests-dot-el)
- [Makefile](#makefile)
- [Summary](#summary)
- [References](#references)

</div>
<!--endtoc-->

<abbr aria-label="Emacs Lisp Regression Testing" tabindex=0>ERT</abbr> is a testing library
that comes with Emacs i.e. you do not need to install from GNU Elpa or
Melpa. The aim of this post is to serve as a quick guide on how to
start using ERT with your Emacs Lisp library
{{% sidenote %}}
Here, a "library" could even be a set of custom Emacs Lisp code that
you wrote for your Emacs config.
{{% /sidenote %}} .

The ERT manual
{{% sidenote %}}
You can quickly visit the ERT manual from within Emacs using the
wonderful _Info_ system by typing `C-h i m ert`, you can visit [Emacs
Manual -- ERT](https://www.gnu.org/software/emacs/manual/html_node/ert/) in a web browser.
{{% /sidenote %}} is really well-written and here's how it introduces this testing
library:

> ERT is a tool for automated testing in Emacs Lisp. Its main features
> are facilities for defining tests, running them and reporting the
> results, and for debugging test failures interactively.
>
> ..it
<mark>works well both for [test-driven development](https://en.wikipedia.org/wiki/Test-driven_development)</mark> and for traditional software development methods.

The part about _test-driven development_ is so true! At the moment, I
am developing an Emacs library called [`baser`](https://github.com/kaushalmodi/baser) that does signed number
conversions among base 10, base 2 and base 16 formats. Before adding
new features, I first write the `ert` tests, and then I develop and
refine the functions until they start passing.

In this post, I will use that _baser_ library as an example of how to
set up ERT -- which I believe would be applicable to other Emacs Lisp
projects too.


## Test directory {#test-directory}

Below shows how the `test/` directory is created with respect to the
library being tested.

<a id="code-snippet--test-dir-structure-ert"></a>
```text
baser/                (repo root)
├── baser.el
├── ..
├── test/
│  ├── all-tests.el
│  ├── ..
│  └── tdec-hex.el
└── Makefile
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--test-dir-structure-ert">Code Snippet 1</a>:</span>
  Test directory structure from <a href="https://github.com/kaushalmodi/baser"><b>baser</b></a>
</div>

Here,

1.  `baser.el` is the Emacs Lisp library being tested.
2.  `test/` is the directory containing all the tests.
3.  `Makefile` defines a `test` target so that running tests is as easy
    as `make test`.


## Test Files {#test-files}

In the `test/` directory, I have a main `all-tests.el` file that
requires all other test files named in `t***.el` style.


### Feature-specific `t***.el` test files {#feature-test}

I like to follow the convention of starting all test file names with
`t`.

I break up the test files such that each file tests only a particular
feature. In _baser_, I have `tdec-hex.el` to test the conversions
between decimal and hexidecimal formats. Similarly I have
`tdec-bin.el` and `thex-bin.el`.


#### General structure of each feature test {#general-structure-of-each-feature-test}

The structure of each `tfeature.el` file looks like this:

<a id="code-snippet--tfeature-file-structure"></a>
```emacs-lisp { linenos=true, anchorlinenos=true, lineanchors=org-coderef--7f5520 }
;; tfeature.el                    -*- lexical-binding: t; -*-

;; Require the emacs-lisp library being tested.
;; Example: (require 'baser)

;; Test things that *should* happen.
(ert-deftest addition-test ()
  (should (= (+ 1 2) 3)))

;; Test things that your library *should trigger errors* for.
(ert-deftest div-by-0-test ()
  (should-error (/ 0 0)
                :type 'arith-error))


(provide 'tfeature)
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--tfeature-file-structure">Code Snippet 2</a>:</span>
  Structure of each <code>tfeature.el</code> test file
</div>

-   Lines [7](#org-coderef--7f5520-7) and [11](#org-coderef--7f5520-11) define the _ert_ tests using
    `ert-deftest`.
-   The first `ert_deftest` uses a [`should` macro](https://www.gnu.org/software/emacs/manual/html_node/ert/The-should-Macro.html) that checks if its
    body evaluates to _non-nil_. It is similar to constructs like
    {{< highlight systemverilog "hl_inline=true" >}}assert (something == 1);{{< /highlight >}} or
    {{< highlight nim "hl_inline=true" >}}doAssert something == true{{< /highlight >}} in
<span title="SystemVerilog">other</span> <span title="Nim">programming</span> languages.
-   The [second `ert_deftest`​](#org-coderef--7f5520-11) uses kind the `should-error` macro that
    asserts if the evaluation of its body resulted in an error. Here,
    the test fails if the body does **not** throw an error. The optional
    `:type <error type>` argument makes the test stricter; in addition
    to checking if evaluating that body throws an error, it also checks
    if the thrown error is of that specific type.

<div class="note">

`ert` does not need to be _required_ manually because `ert-deftest`
will autoload it.

</div>


### Wrapper test file `all-tests.el` {#wrapper-test-file-all-tests-dot-el}

A wrapper test file `all-tests.el` is created for convenience so that
loading just this one file loads all the tests. Having such a wrapper
file is helpful as you will see in the _Makefile_ section next.

This file simply requires all the feature test files like the ones
explained [above](#feature-test).

<a id="code-snippet--all-tests-ert"></a>
```emacs-lisp
;; all-tests.el

;; If the directory happens to have both compiled and uncompiled
;; version, prefer to use the newer (typically the uncompiled) version.
(setq load-prefer-newer t)

(require 'tdec-hex)  ;Require the dec<->hex conversion feature test
;; more feature tests ..
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--all-tests-ert">Code Snippet 3</a>:</span>
  Wrapper test file <code>all-tests.el</code> to require all other tests
</div>


## Makefile {#makefile}

Finally, we create a `Makefile` so that we can run `make test` to run
the ERT tests.

1.  Run `make test` to run all tests.
2.  Run `make test MATCH=foo` to run only the tests whose names match
    "foo". The test names are the ones defined by the `ert-deftest`
    macro.

<!--listend-->

<a id="code-snippet--makefile-ert"></a>
```makefile
EMACS ?= emacs

TEST_DIR=$(shell pwd)/test

# Run all tests by default.
MATCH ?=

.PHONY: test

test:
    $(EMACS) --batch -L . -L $(TEST_DIR) -l all-tests.el -eval '(ert-run-tests-batch-and-exit "$(MATCH)")'
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--makefile-ert">Code Snippet 4</a>:</span>
  <code>make test</code> recipe for running ERT tests
</div>

See [ERT -- Running Tests in Batch Mode](https://www.gnu.org/software/emacs/manual/html_node/ert/Running-Tests-in-Batch-Mode.html) for reference.


## Summary {#summary}

You can quickly create a nice test setup for your Emacs Lisp library
or Emacs config by adding a test file with tests defined using
`erf-deftest` with `should` and `should-error` macros, and then
running `make test` with the help of a `Makefile` like the one shown
above.

While this post might help you get started with using ERT quickly, do
go through its manual --- it's short and sweet, and well-written, and
covers a lot more than this post.


## References {#references}

-   [Emacs Manual -- ERT](https://www.gnu.org/software/emacs/manual/html_node/ert/)
-   [`baser` library](https://github.com/kaushalmodi/baser)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
