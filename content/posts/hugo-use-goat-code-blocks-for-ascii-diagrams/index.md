+++
title = "Hugo: Use goat code blocks for ASCII diagrams"
author = ["Kaushal Modi"]
description = """
  Use `goat` code blocks if you have ASCII diagrams with a mix of text
  and box drawing characters.
  """
date = 2022-04-07T23:44:00-04:00
tags = ["ascii", "diagrams", "100DaysToOffload"]
categories = ["hugo"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  hugo = "v0.96.0"
[syndication]
  mastodon = 108094443267512606
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [References](#references)

</div>
<!--endtoc-->

[Hugo **v0.93.0**](https://github.com/gohugoio/hugo/releases/tag/v0.93.0) added some serious features like [Code Block Render
Hooks](https://gohugo.io/templates/render-hooks/#render-hooks-for-code-blocks) and native rendering of ASCII diagrams to SVG using a [GoAT](https://github.com/blampe/goat)
{{% sidenote %}}
Go ASCII Tool
{{% /sidenote %}} library.

<div class="note">

To use this feature, you put ASCII diagrams in a code block with
language set to `goat` .. .. That's it!

</div>

I realized I needed to use this new GoAT rendering feature because if
I had an ASCII diagram with a mix of text and box drawing characters
like ┌ │ ─ ┐, the positional relationship between the text and those
characters wasn't always guaranteed to remain as I intended. The
reason is that based on the user's font setup, the box drawing
characters and the regular text characters could be rendered using
different fonts.

This problem is best shown with this example that I was documenting in
the [`ox-hugo` manual](https://ox-hugo.scripter.co/doc/hugo-section/) [today](https://github.com/kaushalmodi/ox-hugo/commit/88ae5c8c65a4f0d401b450338cf8bb36c47684fc).

<a id="code-snippet--ascii-diagram-in-text-src-block"></a>
```org
#+begin_src text
<HUGO_BASE_DIR>/content/<HUGO_SECTION>/<concatenated values of EXPORT_HUGO_SECTION_FRAG>/
                        └─────────────────────  section path  ─────────────────────────┘
#+end_src
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--ascii-diagram-in-text-src-block">Code Snippet 1</a>:</span>
  Mix of text and box characters in a plain <code>text</code> code block
</div>

Above code block renders to:

```text
<HUGO_BASE_DIR>/content/<HUGO_SECTION>/<concatenated values of EXPORT_HUGO_SECTION_FRAG>/
                        └─────────────────────  section path  ─────────────────────────┘
```

The code block rendered above might look alright on your browser based
on your font setup.

-   The "section path" box drawing should start at `<HUGO_SECTION>/ ..`
    and end at `.. EXPORT_HUGO_SECTION_FRAG>`.

But it can also look like below with a different font setup:

<a id="figure--disproportionate-box-drawing"></a>

{{< figure src="ascii-diagram-rendered-in-plain-text-code-block.png" caption="<span class=\"figure-number\">Figure 1: </span>Disproportionate box drawing characters" >}}

To fix that, we simply change the code block language to `goat`
{{% sidenote %}}
If you author your posts in Markdown, this applies to you too. Just
use the Markdown fenced code block syntax: `` ```goat .. ``` ``.
{{% /sidenote %}} <a id="code-snippet--ascii-diagram-in-goat-src-block"></a>
```org
#+begin_src goat
<HUGO_BASE_DIR>/content/<HUGO_SECTION>/<concatenated values of EXPORT_HUGO_SECTION_FRAG>/
                        └─────────────────────  section path  ─────────────────────────┘
#+end_src
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--ascii-diagram-in-goat-src-block">Code Snippet 2</a>:</span>
  Mix of text and box characters in a <code>goat</code> code block
</div>

Now we will see the below rendering on any browser because you are
looking at the SVG generated from the above code block by that _GoAT_
library.

```goat
<HUGO_BASE_DIR>/content/<HUGO_SECTION>/<concatenated values of EXPORT_HUGO_SECTION_FRAG>/
                        └─────────────────────  section path  ─────────────────────────┘
```

Caveat
: The GoAT library doesn't render the text fonts to my
    liking, but it's still better than completely messing up the
    positional relationship between the text and the box diagram
    characters.

Happy grazing! :goat:


## References {#references}

-   [Hugo Documentation -- Diagrams](https://gohugo.io/content-management/diagrams/)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
