+++
title = """
  Linking and Exporting Org "info:" links
  """
author = ["Kaushal Modi"]
description = """
  Journey of Org links from copying (storing) them, inserting them in
  Org documents and exporting them. The focus of this post is on the
  `info:` type Org links, but the same concept would apply to any Org
  link type.
  """
date = 2022-04-12T13:51:00-04:00
lastmod = 2022-04-15T00:00:00-04:00
tags = ["100DaysToOffload", "info", "ox-hugo"]
categories = ["web", "hugo", "emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  emacs = "28.1.50"
  org = "release_9.5-532-gf6813d"
[syndication]
  mastodon = 108120475028701897
[logbook]
  [logbook._toplevel]
    [[logbook._toplevel.notes]]
      timestamp = 2022-04-15T00:00:00-04:00
      note = """
  This post was re-exported today to get better `info:` link exports as
  described in [a newer post](https://scripter.co/improving-ox-hugo-exported-org-info-links/).
  """
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [<span class="section-num">1</span> Storing links to Info nodes](#storing-links-to-info-nodes)
- [<span class="section-num">2</span> Inserting stored links](#inserting-stored-links)
    - [Org Link library for `info:` links (`ol-info`)](#org-link-library-for-info-links--ol-info)
- [<span class="section-num">3</span> Exporting `[[info:..]]` links using `ox-hugo`](#exporting-info-dot-dot-links-using-ox-hugo)
- [Summary](#summary)

</div>
<!--endtoc-->


This post was inspired out of the exercise of support `info:` Org link
exports in `ox-hugo`.

There are 3 steps:

1.  Storing a link from any buffer that you want to later document in
    an Org file.
2.  Inserting that link that you stored earlier.
3.  Exporting the Org document.

I am focusing on the `info:` Org links in this post, they these steps
apply to [any](https://orgmode.org/manual/External-Links.html "Emacs Lisp: (info \"(org) External Links\")") Org link type.


## <span class="section-num">1</span> Storing links to Info nodes {#storing-links-to-info-nodes}

If you follow the instructions on [Org Info: Activation](https://orgmode.org/manual/Activation.html "Emacs Lisp: (info \"(org) Activation\")"), you'll see that
it is recommended
{{% sidenote %}}
This post assumes that you have actually set this binding. If you
haven't yet, go do it already! `(global-set-key (kbd "C-c l")
#'org-store-link)`
{{% /sidenote %}} to bind the `org-store-link` command to <kbd>C-c</kbd> <kbd>l</kbd> in your Emacs
_global map_.

`org-store-link` stores an Org Mode compatible link based on the
context i.e. type of buffer where the point is.  Org supports a
variety of links as documented in [Org Info: External Links](https://orgmode.org/manual/External-Links.html "Emacs Lisp: (info \"(org) External Links\")").

If the point is in an &lowast;Info&lowast; buffer and `org-store-link` is called, it
stores an `info:` type link that will lead back to the same node where
the point was.

Let's say you have opened the Org Info manual and you hit <kbd>C-c</kbd>
<kbd>l</kbd>. You should then see this in the _Echo Area_:

> Stored: org#Top

The link to Org manual's _Top_ node is _stored_ by that command, but
only temporarily -- it isn't yet saved anywhere.


## <span class="section-num">2</span> Inserting stored links {#inserting-stored-links}

These stored links can now be saved in an Org file. To do that,

-   Visit an Org file or a buffer and call <kbd>M-x</kbd> <kbd>org-insert-link</kbd>
    (bound to <kbd>C-c</kbd> <kbd>C-l</kbd> by default) and follow the prompts.
-   An immediate <kbd>RET</kbd> after the prompt will select the last stored link
    to be inserted.
-   The second prompt allows you to change the link description, and
    then the link will be inserted.

If I insert the `org#Top` Info link that I stored earlier over here,
and accept the default link description, it will paste as
{{< highlight org "hl_inline=true" >}}[[info:org#Top][org#Top]]{{< /highlight >}}.

But .. how did the link description become "`org#Top`" by default?

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The [`ol-info.el`](https://git.savannah.gnu.org/cgit/emacs/org-mode.git/tree/lisp/ol-info.el) library did that.<br />

</div>


### Org Link library for `info:` links (`ol-info`) {#org-link-library-for-info-links--ol-info}

The default description for `info:` type links was set at the time of
storing those links, with the help of the [`org-info-store-link`](https://git.savannah.gnu.org/cgit/emacs/org-mode.git/tree/lisp/ol-info.el?id=f6813dbea9ef0c6be19bf68b4d9227ceb64c9449#n49)
function in `ol-info.el`.  That same description is then suggested at
the time of inserting that link.

This library defines these "actions" for `[[info:..]]` type of links:

:store
: Defines when (in which buffer, major mode, etc.) the
    "store" operation should store to an `info:` type link, and what
    metadata should be stored. Some of the stored metadata are: the Info
    file name, node name, link string and description string.

:follow
: Defines what action to perform when `org-open-at-point`
    (<kbd>C-c</kbd> <kbd>C-o</kbd>) is called with point on the link.  Hitting this
    binding with point on an `info:` link will open or switch to that
    &lowast;Info&lowast; node.

:export
: Defines how the link should be exported for various
    backends. _`ol-info` defines how the `info:` links should be
    exported, but only for `html` and `texinfo` backends._


## <span class="section-num">3</span> Exporting `[[info:..]]` links using `ox-hugo` {#exporting-info-dot-dot-links-using-ox-hugo}

`ol-info.el` defines the `:export` "action" such that `info:` links
get converted to hyperlinks pointing to online Org manual pages, when
exporting using `ox-html`.

But it didn't do the same when exporting using `ox-hugo`. That feature
got added recently in [this `ox-hugo` commit](https://github.com/kaushalmodi/ox-hugo/blob/d3d4c57444f03898e78d2ae11e97fdb94a4655c5/ox-hugo.el#L888-L899). Now `ox-hugo` exports
{{< highlight org "hl_inline=true" >}}[[info:org#Top][org#Top]]{{< /highlight >}} to [org#Top](https://orgmode.org/manual/index.html "Emacs Lisp: (info \"(org) Top\")").

I didn't like the `#` that `org-info-store-link` added to the default
description (notice that previous link). So I added a tiny little
feature to the `info:` export behavior :grin: --- If you leave the
`info:` link descriptions empty when inserting the links, `ox-hugo`
injects its own "auto description" which are better (in my
opinion).

Now when that same Info link is inserted description-less as
{{< highlight org "hl_inline=true" >}}[[info:org#Top]]{{< /highlight >}}, it renders to: [Org Info](https://orgmode.org/manual/index.html "Emacs Lisp: (info \"(org) Top\")").


## Summary {#summary}

1.  Store link (from anywhere in Emacs) : <kbd>C-c</kbd> <kbd>l</kbd>
2.  Insert link (in an Org file) : <kbd>C-c</kbd> <kbd>C-l</kbd>
3.  Export the Org file as usual.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
