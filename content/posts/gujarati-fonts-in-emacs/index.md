+++
title = "Gujarati fonts in Emacs"
author = ["Kaushal Modi"]
description = "Setting a different font for a specific script or language in Emacs."
date = 2022-06-19T01:31:00-04:00
series = ["Gujarati in Emacs"]
tags = ["gujarati", "font", "100DaysToOffload"]
categories = ["emacs"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
series_weight = 4002
[versions]
  emacs = "28.1.50"
[syndication]
  mastodon = 108502548982628792
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Setting a "fontset" font](#setting-a-fontset-font)
- [Gujarati fonts](#gujarati-fonts)

</div>
<!--endtoc-->


All Emacs versions ship with a nifty **HELLO** file that you can quickly
open using `M-x view-hello-file` or its default binding `C-h h`. This
file lists "Hello" written in dozens of languages to demonstrate some
of the character sets supported by Emacs.

<a id="figure--hello-buffer"></a>

{{< figure src="hello.png" caption="<span class=\"figure-number\">Figure 1: </span>\"Hello\" buffer in Emacs" link="hello.png" >}}

Born and raised in the Gujarat state in India, I grew up
speaking the [Gujarati (ગુજરાતી)](https://en.wikipedia.org/wiki/Gujarati_language) language
{{% sidenote %}}
India officially recognizes [22 languages](https://en.wikipedia.org/wiki/Languages_of_India) (as of <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-06-19 Sun&gt;</span></span>) and
Gujarati is one of them.
{{% /sidenote %}} and so it's also the language closest to my heart. So I was pleasantly
surprised to see a representation of Gujarati in the "Hello" buffer!
In the above screenshot, in the "South Asia" section, the script after
the yellow cursor is Gujarati, and it reads _namaste_.


## Setting a "fontset" font {#setting-a-fontset-font}

It was on <span class="timestamp-wrapper"><span class="timestamp">&lt;2018-08-13 Mon&gt; </span></span> that I discovered the presence of Gujarati
script in that "Hello" buffer, and the reason I know that exact date
is because I had [asked a question](https://lists.gnu.org/r/help-gnu-emacs/2018-08/msg00033.html) regarding that on the
_help-gnu-emacs_ mailing list :smiley:.

This was the time when Emacs was using the [m17n](https://www.nongnu.org/m17n/) library for
multi-lingual font rendering by default. The question was regarding a
font rendering issue I was seeing. As I learn later in that thread, it
was because I didn't have the m17n database installed on my machine
{{% sidenote %}}
At least in 2022, the [harfbuzz](https://github.com/harfbuzz/harfbuzz) library is the recommended library for
text shaping and font rendering. Someone please correct me if that's
wrong. In any case, I have switched to using _harfbuzz_ instead of
_m17n_ for a while now and haven't found any font-rendering issues
with non-English scripts.
{{% /sidenote %}} . But it's in that support thread that, thanks to _Andy Moreton_, I
learned that you can change the font for the Gujarati script using
`set-fontset-font`.

This applies in general to any script. You can read more details about
this function in [Emacs Info: Modifying Fontsets](https://www.gnu.org/software/emacs/manual/html_node/emacs/Modifying-Fontsets.html "Emacs Lisp: (info \"(emacs) Modifying Fontsets\")"), but here's the gist:

<a id="code-snippet--gujarati-set-fontset-font"></a>
```emacs-lisp
(set-fontset-font "fontset-default" 'gujarati "<FONT NAME>")
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--gujarati-set-fontset-font">Code Snippet 1</a>:</span>
  Setting default font for Gujarati script using <code>set-fontset-font</code>
</div>

That led me down the path of exploring the available Gujarati fonts
out there ..


## Gujarati fonts {#gujarati-fonts}

After looking around for a bit, I found a wonderful [collection of
Gujarati fonts](https://github.com/samyakbhuta/chhapkaam/wiki/%E0%AA%97%E0%AB%81%E0%AA%9C%E0%AA%B0%E0%AA%BE%E0%AA%A4%E0%AB%80-%E0%AA%AF%E0%AB%81%E0%AA%A8%E0%AA%BF%E0%AA%95%E0%AB%8B%E0%AA%A1-%E0%AA%AB%E0%AB%8B%E0%AA%A8%E0%AB%8D%E0%AA%9F-%E0%AA%B8%E0%AB%82%E0%AA%9A%E0%AB%80---List-of-Gujarati-Unicode-Fonts) in this GitHub repository:
[github.com/samyakbhuta/chhapkaam](https://github.com/samyakbhuta/chhapkaam)
{{% sidenote %}}
The repo name is _chhapkaam_ (I
would have spelled it as _chhaapkaam_) which is the Gujarati word
છાપકામ, meaning "printing".
{{% /sidenote %}} .

Below is my further curated list of fonts from the above list:

<a id="table--gujarati-fonts"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--gujarati-fonts">Table 1</a>:</span>
  Gujarati Fonts
</div>

| Font Name          | _Namaste_                    | Category   | Homepage                                                                      | Download                                              |
|--------------------|------------------------------|------------|-------------------------------------------------------------------------------|-------------------------------------------------------|
| **Shruti**         | ![](namaste_shruti.png)      | serif      |                                                                               | [wfonts.com](https://www.wfonts.com/font/shruti)      |
| **Mukta Vaani**    | ![](namaste_mukta_vaani.png) | serif      | [Ek Type -- Mukta Vaani](https://ektype.in/scripts/gujarati/mukta-vaani.html) | [GitHub](https://github.com/EkType/Mukta/releases)    |
| **Lohit Gujarati** | ![](namaste_lohit.png)       | sans serif | [Pagure -- Lohit](https://pagure.io/lohit)                                    | [pagure releases](https://releases.pagure.org/lohit/) |

To use these fonts, after downloading and installing them on your
system, evaluate [Code Snippet 1](#code-snippet--gujarati-set-fontset-font) above with the
correct "FONT NAME". For example, to set the Gujarati text to use the
_Shruti_ font, evaluate {{< highlight emacs-lisp "hl_inline=true" >}}(set-fontset-font "fontset-default" 'gujarati "Shruti"){{< /highlight >}}.

Thanks for reading (વાંચવા બદલ આભાર) :pray:!

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
