+++
title = "Org: Show only Post subtree headings"
author = ["Kaushal Modi"]
description = """
  How to define a custom `org-global-cycle`-like command that collapses
  only the Org subtrees with specific properties.
  """
date = 2022-06-16T00:21:00-04:00
tags = ["100DaysToOffload", "subtree", "looping", "advice"]
categories = ["emacs", "org", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  org = "release_9.5.4-549-gaa789b"
  emacs = "28.1.50"
[syndication]
  mastodon = 108485468708210928
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Org Global Cycle](#org-global-cycle)
- [Skeleton of only Post headings](#skeleton-of-only-post-headings)
- [The "Collapse All Posts" function](#the-collapse-all-posts-function)
- [Binding with `C-u C-c TAB`](#binding-with-c-u-c-c-tab)
- [Result](#result)

</div>
<!--endtoc-->


I start this post by introducing what the Org mode global cycling
command does, what kind of subtree folding I actually need, and then
share the solution with code snippets.


## Org Global Cycle {#org-global-cycle}

Org mode has a built-in `org-global-cycle` command that you might be
familiar with. It's bound by default to the `S-TAB` key. Each time
this command is called, the Org buffer will cycle through these
states:

1.  Overview: Show only the Level 1 headings and collapse everything
    underneath.
2.  Contents: Show only the Org headings and collapse all the content.
3.  Show All: Expand all the headings and show their contents too.

If a numeric prefix _N_ is used with this command, it will show only
the Org headings up to Level _N_. For example, `C-2 S-TAB` will show
only the headings up to Level 2.

This is a really helpful command, but I needed something different ..


## Skeleton of only Post headings {#skeleton-of-only-post-headings}

I maintain most of this website's content in a single Org file. I have
dozens of blog posts organized in Org subtrees, which I further
organize under "category" headings .. It kind of looks like the below
mock-up:
{{% sidenote %}}
It's amazing how many features PlantUML has. If you are interested in
creating diagrams like these, check out the [PlantUML Salt](https://plantuml.com/salt) syntax.
{{% /sidenote %}}

<a id="figure--post-subtrees-collapsed-mockup"></a>

{{< figure src="post-subtrees.svg" caption="<span class=\"figure-number\">Figure 1: </span>Post Subtrees at arbitrary heading levels" >}}

As we can see,

-   All the post subtrees are not at Level 1 headings.
-   They are also not at a fixed Level _N_.
-   The heading level of the post depends on how many parent categories
    that post has (and that will also change over time).

I needed to basically show everything leading up to a post subtree
heading, and then collapse all the content under that post; even the
sub-headings.


## The "Collapse All Posts" function {#the-collapse-all-posts-function}

The `modi/org-hugo-collapse-all-posts` function defined below meets
the above requirement:

1.  It first widens the whole buffer and expands all the headings.
2.  Then it loops through all the headings and collapses all the _post
    subtrees_ i.e. all the subtrees that have the `EXPORT_FILE_NAME`
    property set. This is where I use the [`org-map-entries`]({{< relref "looping-through-org-mode-headings" >}}) magic.
3.  Finally it looks for Org headings that begin with "Footnotes" or
    "COMMENT" and collapses them as well.

I am using the development version of Org mode (version 9.6, yet to be
released as of <span class="timestamp-wrapper"><span class="timestamp">&lt;2022-06-15 Wed&gt;</span></span>) which has the new `org-fold`
library. This library obsoletes the use of `outline.el` library and
other _code-folding_ related functions in Org mode. So `cl-flet` is
used to create function symbol aliases that use the `org-fold-*`
functions if available, otherwise they fall back to the legacy
functions.

<a id="code-snippet--collapse-all-posts-fn"></a>
```emacs-lisp { linenos=true, linenostart=1, hl_lines=["19","23"] }
(defun modi/org-hugo-collapse-all-posts ()
  "Collapse all post subtrees in the current buffer.
Also collapse the Footnotes subtree and COMMENT subtrees if
present.

A post subtree is one that has the EXPORT_FILE_NAME property
set."
  (interactive)
  (cl-flet ((show-all (if (fboundp 'org-fold-show-all)
                          #'org-fold-show-all
                        #'org-show-all))
            (hide-subtree (if (fboundp 'org-fold-hide-subtree)
                              #'org-fold-hide-subtree
                            #'outline-hide-subtree)))
    (widen)
    (show-all '(headings))
    ;; Collapse all the post subtrees (ones with EXPORT_FILE_NAME
    ;; property set).
    (org-map-entries #'hide-subtree "EXPORT_FILE_NAME<>\"\"" 'file)
    ;; Also hide Footnotes and comments.
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "^\\(\\* Footnotes\\|\\*+ COMMENT\\)"
                                nil :noerror)
        (hide-subtree)))))
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--collapse-all-posts-fn">Code Snippet 1</a>:</span>
  Function that collapses all the post subtrees in the current buffer
</div>


## Binding with `C-u C-c TAB` {#binding-with-c-u-c-c-tab}

The function is ready, but let's now add a bit of convenience to it.

If a point is under a subtree, `C-c TAB` will collapse that subtree
while showing only Level 1 headings, and if a numeric prefix is used,
it will show only those many levels of headings. I decided to bind the
above function to `C-u C-c TAB` because,

1.  The behavior of `modi/org-hugo-collapse-all-posts` falls in the
    same category as that of `C-c TAB`.
2.  The `C-u C-c ..` binding rolls off the fingers pretty
    nicely :smiley:.

This _binding_ is achieved using one of my favorite Emacs features
.. the **advice** system. The `:before-until` [Advice Combinator](https://www.gnu.org/software/emacs/manual/html_node/elisp/Advice-Combinators.html "Emacs Lisp: (info \"(elisp) Advice Combinators\")") is used
here, which means that if the _advising_ function (below) returns a
_nil_, the _advised_ or the original function `org-ctrl-c-tab` is not
called.

The _advising_ function below detects if the `C-u` prefix argument is
used. If it is, the `modi/org-hugo-collapse-all-posts` function is
called, otherwise the original `org-ctrl-c-tab` function is called.

<a id="code-snippet--collapse-all-posts-binding"></a>
```emacs-lisp { linenos=true, linenostart=1, hl_lines=["4"] }
(defun modi/org-ctrl-c-tab-advice (&rest args)
  "Run `modi/org-hugo-collapse-all-posts' when
doing \\[universal-argument] \\[org-ctrl-c-tab]."
  (let ((do-not-run-orig-fn (equal '(4) current-prefix-arg)))
    (when do-not-run-orig-fn
      (modi/org-hugo-collapse-all-posts))
    do-not-run-orig-fn))

(advice-add 'org-ctrl-c-tab :before-until #'modi/org-ctrl-c-tab-advice)
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--collapse-all-posts-binding">Code Snippet 2</a>:</span>
  Bind <code>C-u C-c TAB</code> to call <code>modi/org-hugo-collapse-all-posts</code>
</div>


## Result {#result}

After evaluating the above two snippets, when I do `C-u C-c TAB` in
my "blog posts" Org buffer, I see this:

<a id="figure--post-subtrees-collapsed"></a>

{{< figure src="post-subtrees-collapsed.png" caption="<span class=\"figure-number\">Figure 2: </span>My \"blog posts\" Org buffer showing only the Post subtree headings" link="post-subtrees-collapsed.png" >}}

It matches [that earlier mockup](#figure--post-subtrees-collapsed-mockup) --- Mission accomplished! :100:

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
