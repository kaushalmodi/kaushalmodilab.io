+++
title = "Converting Org keywords to lower-case"
author = ["Kaushal Modi"]
date = 2018-02-02T13:42:00-05:00
aliases = ["/converting-org-keywords-to-lower-case/"]
categories = ["emacs", "org", "elisp"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
+++

I never quite liked the trend to have upper-cased keywords in Org
documents, like `#+TITLE`.

So it was a pleasure to see that trend start changing in this [Org
commit](https://code.orgmode.org/bzg/org-mode/commit/13424336a6f30c50952d291e7a82906c1210daf0).. so that that same keyword would now be written as `#+title`.

<!--more-->

But now I have quite a few Org documents with the ALL-CAPS keywords
and block identifiers. So I came up with this little "lower-case all
the Org keywords and block identifiers in the current document" Elisp
command:

```emacs-lisp
(defun modi/lower-case-org-keywords ()
  "Lower case Org keywords and block identifiers.

Example: \"#+TITLE\" -> \"#+title\"
         \"#+BEGIN_EXAMPLE\" -> \"#+begin_example\"

Inspiration:
https://code.orgmode.org/bzg/org-mode/commit/13424336a6f30c50952d291e7a82906c1210daf0."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let ((case-fold-search nil)
          (count 0))
      ;; Match examples: "#+FOO bar", "#+FOO:", "=#+FOO=", "~#+FOO~",
      ;;                 "‘#+FOO’", "“#+FOO”", ",#+FOO bar",
      ;;                 "#+FOO_bar<eol>", "#+FOO<eol>".
      (while (re-search-forward "\\(?1:#\\+[A-Z_]+\\(?:_[[:alpha:]]+\\)*\\)\\(?:[ :=~’”]\\|$\\)" nil :noerror)
        (setq count (1+ count))
        (replace-match (downcase (match-string-no-properties 1)) :fixedcase nil nil 1))
      (message "Lower-cased %d matches" count))))
```

Here are [few](https://github.com/kaushalmodi/ox-hugo/commit/ad1b513c0847383d19bf37becce6413697d14bd0) [examples](https://github.com/kaushalmodi/eless/commit/f2eee31be46f6f296541d840f74fdd7dc1f5acd2) where <kbd>M-x</kbd> `modi/lower-case-org-keywords` did
hundreds of replacements for me, saving me a **lot** of time
:sunglasses:.

[Source](https://github.com/kaushalmodi/.emacs.d/blob/56a2d3ad42e6640ddae46b5afd3f93044b6d5172/setup-files/setup-org.el#L267-L286)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
