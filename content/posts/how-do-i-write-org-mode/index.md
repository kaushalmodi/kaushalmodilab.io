+++
title = """
  How do I write "Org mode"?
  """
author = ["Kaushal Modi"]
description = """
  You write it just like that in the title --- "Org" with capital "O",
  and then lower-case "mode" separated by a space.
  """
date = 2018-05-21T16:35:00-04:00
tags = ["convention", "consistency", "standard"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[syndication]
  twitter = 998665470323646464
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [A super-quick intro to Org mode](#a-super-quick-intro-to-org-mode)
- [Inconsistency](#inconsistency)
- [The right way](#the-right-way)
- [Reference](#reference)

</div>
<!--endtoc-->



## A super-quick intro to Org mode {#a-super-quick-intro-to-org-mode}

[Org mode](https://orgmode.org) is a fantastic major mode for Emacs, and people use it for
all sorts of things like keeping notes, maintaining TODO lists,
writing documentation, or even blogging---like I am doing here.


## Inconsistency {#inconsistency}

Every now and then, I would see "Org mode" and related phrases written
in the "wild" (like blogs, Reddit posts, tweets) as _Org-mode_,
_org-manual_, _org file_, etc., with a mix-and-match of cases and
hyphens.

So here is an attempt to familiarize more people with the
documentation standard for referring to "Org" stuff. Below I am
quoting the text from the official Org Documentation Standards:

> -   Prefer "Org mode" to "Org-mode" or "org-mode". This is simply
>     because it reflects an existing convention in [The Emacs Manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html) which
>     consistently documents mode names in this form - "Text mode",
>     "Outline mode", "Mail mode", etc.
> -   Likewise refer, if at all possible, to "Org file or "Org buffer"
>     meaning with, great generality, any file or buffer which requires
>     use of some part of Org to edit it properly.
> -   Org uses "org-..." to ring fence a name space for itself in the
>     Emacs code base. This is obviously retained in code snippets.


## The right way {#the-right-way}

<div class="org-center">

**Org mode**, **Org manual**, **Org file**

</div>

Only in the Elisp code, is it called `org-mode`, because it is a
_major mode_, and has to be named so in code by convention, and for
its separate `org-*` name space.

But for blogging, documentation, etc. --

<div class="verse">

&nbsp;&nbsp;&nbsp;&nbsp;**Org foo** it is.<br />

</div>


## Reference {#reference}

-   [Org mode Documentation Standard](https://code.orgmode.org/bzg/org-mode/src/master/doc/Documentation_Standards.org#referencing-systems-packages-modes-and-much-else)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
