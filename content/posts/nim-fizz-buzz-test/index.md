+++
title = "Nim: Fizz-Buzz test"
author = ["Kaushal Modi"]
description = "My attempt at _FizzBuzz_ in Nim."
date = 2018-06-05T11:13:00-04:00
tags = ["nim", "fizz-buzz", "test"]
categories = ["programming", "replies"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  nim = "230692a22f9"
[syndication]
  twitter = 1004021650768252928
+++

<div class="mf2 reply">In reply to: <p><a class="u-in-reply-to h-cite" rel="in-reply-to" href="https://masahiko-ofgp-notebook.blogspot.com/2018/06/fizzbuzz-by-nim-lang.html">https://masahiko-ofgp-notebook.blogspot.com/2018/06/fizzbuzz-by-nim-lang.html</a></p></div>

Today I came across this [FizzBuzz attempt for Nim](https://masahiko-ofgp-notebook.blogspot.com/2018/06/fizzbuzz-by-nim-lang.html), so I thought of
giving it a try too.

Here's how _Fizz buzz_ is [defined on Wikipedia](https://en.wikipedia.org/wiki/Fizz_buzz):

> Fizz buzz is a group word game for children to teach them about
> division. Players take turns to count incrementally, replacing any
> number divisible by three with the word "fizz", and any number
> divisible by five with the word "buzz".

And it's also one of those basic programming problems (labeled as
_interview questions_) that's written in [many languages](https://rosettacode.org/wiki/FizzBuzz). Here's how
Rosetta Code defines this programming task ---

> Write a program that prints the integers from 1 to 100 (inclusive).
>
> But:
>
> -   for multiples of three, print Fizz (instead of the number)
> -   for multiples of five, print Buzz (instead of the number)
> -   for multiples of both three and five, print FizzBuzz (instead of the
>     number)

I am not doing anything radical in this post.. just recording my
attempt at FizzBuzz using Nim :smile:.

```nim
for i in 1 .. 100:
  var str = $i
  if (i mod 3) == 0:
    str = "Fizz"
    if (i mod 5) == 0:
      str.add("Buzz")
  elif (i mod 5) == 0:
    str = "Buzz"
  echo str
```

<details>
<summary>See the output</summary>
<div class="details">

```text
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
Fizz
Buzz
26
Fizz
28
29
FizzBuzz
31
32
Fizz
34
Buzz
Fizz
37
38
Fizz
Buzz
41
Fizz
43
44
FizzBuzz
46
47
Fizz
49
Buzz
Fizz
52
53
Fizz
Buzz
56
Fizz
58
59
FizzBuzz
61
62
Fizz
64
Buzz
Fizz
67
68
Fizz
Buzz
71
Fizz
73
74
FizzBuzz
76
77
Fizz
79
Buzz
Fizz
82
83
Fizz
Buzz
86
Fizz
88
89
FizzBuzz
91
92
Fizz
94
Buzz
Fizz
97
98
Fizz
Buzz
```
</div>
</details>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
