+++
title = "Disarming the 'tar' bomb in 10 seconds"
author = ["Kaushal Modi"]
description = """
  Use `tar -caf <file> <dir>` to create an archive, `tar -xf <file>` to
  extract one, and more.
  """
date = 2022-06-13T01:14:00-04:00
tags = ["tar", "100DaysToOffload"]
categories = ["unix", "replies"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  tar = "1.26"
[syndication]
  mastodon = 108468473116969173
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Mnemonics](#mnemonics)
- [Creating an archive](#creating-an-archive)
- [Extracting an archive](#extracting-an-archive)
- [Listing contents of an archive](#listing-contents-of-an-archive)
- [Summary](#summary)

</div>
<!--endtoc-->


I had come across [this post by user _Garrit_](https://fosstodon.org/@garritfra/108409516362853350) on Mastodon, and that
inspired this post.

<div class="mf2 reply">In reply to: <p><a class="u-in-reply-to h-cite" rel="in-reply-to" href="https://garrit.xyz/posts/2022-06-02-tar-commands">https://garrit.xyz/posts/2022-06-02-tar-commands</a></p></div>

A post on the Unix command [`tar`](https://www.gnu.org/software/tar/manual/html_node/index.html "Emacs Lisp: (info \"(tar) Top\")") cannot leave out the obligatory _xkcd
**tar**_ comic :smile:, so here it is:

<a id="figure--xkcd-1168"></a>

{{< figure src="xkcd-1168.png" title="I don't know what's worse--the fact that after 15 years of using tar I still can't keep the flags straight, or that after 15 years of technological advancement I'm still mucking with tar flags that were 15 years old when I started." attr="xkcd.com" attrlink="https://xkcd.com/1168/" >}}


## Mnemonics {#mnemonics}

These few mnemonics help me remember the basic and my most frequently
used `tar` options:

-   **c** to (c)reate
-   **a** to (a)uto compress the archive based on the file name
-   **x** to e(x)tract
-   **f** for the archive (f)ile name we are dealing with (whether
    creating, listing or extracting an archive)

.. and a few not so frequent options (for me):

-   **t** to lis(t) contents of an archive
-   **v** for (v)erbose output


## Creating an archive {#creating-an-archive}

<div class="org-center">

**tar -caf &lt;file&gt; &lt;dir&gt;**

</div>

A keen user might have noticed that I am using `tar -caf ..` instead
of `tar caf ..` i.e. I am using a hyphen before the `tar`
options. Both approaches work and they look similar, but the approach
with the hyphen is the **newer** [Short Option style](https://www.gnu.org/software/tar/manual/html_node/Short-Options.html "Emacs Lisp: (info \"(tar) Short Options\")") while the other is
the [Old Option style](https://www.gnu.org/software/tar/manual/html_node/Old-Options.html "Emacs Lisp: (info \"(tar) Old Options\")").

<div class="note">

I prefer the _short option style_ because .. well.. the other style is
old.. and also because the _short option style_ is **stricter** e.g. the
`-f` switch has to be followed by the file name.

</div>

Whether you are creating a regular **.tar** archive, or a compressed
archive like **.tar.gz**, always use the auto-compresion switch
**-a**. That relieves you from deciding _if_ you need that switch, or
which compression algorithm switch should be used :smile:.

The **-a** switch makes the decision for you based on the file
extension.  For example, `tar -caf foo.tar foo/` will create a regular
archive, while `tar -caf foo.tar.gz foo/` will create a compressed
archive using `gzip`. You can read more about it in [Creating and
Reading Compressed Archives](https://www.gnu.org/software/tar/manual/html_node/gzip.html "Emacs Lisp: (info \"(tar) gzip\")").


## Extracting an archive {#extracting-an-archive}

<div class="org-center">

**tar -xf &lt;file&gt;**

</div>

At times, it might be useful to add the verbosity switch **-v** to this
command and do `tar -xvf <file>`.

<div class="note">

Just to reiterate, the **-f** switch must be followed by the archive
file name.

</div>


## Listing contents of an archive {#listing-contents-of-an-archive}

<div class="org-center">

**tar -tf &lt;file&gt;**

</div>

Once you are done creating an archive, you might feel the need to
check if the archive contains everything you expect. Or you might want
to check what's inside the archive before you extract it.

This command is often paired with `grep` or [`rg` (ripgrep)](https://github.com/BurntSushi/ripgrep) like so:
`tar -tf foo.tar.xz | rg 'some_file_name_in_archive'`.


## Summary {#summary}

If you simply glossed over the whole article, or didn't read through
the all linked manual pages
{{% sidenote %}}
I know you didn't :wink:
{{% /sidenote %}} , just remember this ---

<div class="org-center">

**tar -caf** to **c**​reate and **tar -xf** to e​**x**​tract

</div>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
