+++
title = "PlantUML version"
author = ["Kaushal Modi"]
description = "Finding PlantUML version."
date = 2018-04-03T17:53:00-04:00
tags = ["plantuml", "version"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.3 + ox-hugo)"
[versions]
  plantuml = "1.2022.1"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Using PlantUML code](#using-plantuml-code)
- [From the command line](#from-the-command-line)
- [Reference](#reference)

</div>
<!--endtoc-->

<span class="timestamp-wrapper"><span class="timestamp">&lt;2022-02-22 Tue&gt;</span></span>
: Add another method to get the PlantUML version, using `version` in the UML code.


## Using PlantUML code {#using-plantuml-code}

<a id="code-snippet--plantuml-version"></a>
```plantuml
@startuml
version
@enduml
```
<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--plantuml-version">Code Snippet 1</a>:</span>
  Print PlantUML version
</div>

<a id="figure--plantuml-version"></a>

{{< figure src="/ox-hugo/plantuml-version.svg" caption="<span class=\"figure-number\">Figure 1: </span>PlantUML Version Output" >}}


## From the command line {#from-the-command-line}

```shell
java -jar /path/to/plantuml.jar -version
```

That will give an output like:

```text
PlantUML version 1.2022.1 (Tue Feb 01 13:19:58 EST 2022)
(GPL source distribution)
Java Runtime: OpenJDK Runtime Environment
JVM: OpenJDK 64-Bit Server VM
Default Encoding: UTF-8
Language: en
Country: US

PLANTUML_LIMIT_SIZE: 4096

Dot version: dot - graphviz version 2.30.1 (20180420.1509)
Installation seems OK. File generation OK
```


## Reference {#reference}

[PlantUML FAQ](https://plantuml.com/faq)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
