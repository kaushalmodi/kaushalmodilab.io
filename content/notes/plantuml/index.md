+++
title = "PlantUML"
author = ["Kaushal Modi"]
description = "Collection of PlantUML snippets that I find useful."
date = 2018-04-06
lastmod = 2022-06-17T14:45:10-04:00
tags = ["plantuml"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.4 + ox-hugo)"
[versions]
  plantuml = "1.2018.14"
+++

<div class="ox-hugo-toc toc">

<div class="heading">Table of Contents</div>

- [Class with tree-structure in body](#class-with-tree-structure-in-body)
- [Class Composition](#class-composition)
    - [Reference](#reference)
- [Nested boxes](#nested-boxes)
- [Forcing vertical ordering of rectangles](#forcing-vertical-ordering-of-rectangles)
- [Simple Flowchart](#simple-flowchart)
    - [Activity Diagram](#activity-diagram)
    - [Activity Diagram -- Beta](#activity-diagram-beta)
- [Hyperlinked SVGs](#hyperlinked-svgs)
- [Salt](#salt)
    - [Tab Bar](#tab-bar)

</div>
<!--endtoc-->

<div class="org-center">

[PlantUML Homepage](https://plantuml.com/) | [Online Playground](https://www.plantuml.com/plantuml/uml/)

</div>


## Class with tree-structure in body {#class-with-tree-structure-in-body}

```plantuml
class Foo {
**Bar(Model)**
|_ prop
|_ **Bom(Model)**
  |_ prop2
  |_ prop3
    |_ prop3.1
|_ prop4
--
}
```

{{< figure src="class-body-tree.svg" caption="<span class=\"figure-number\">Figure 1: </span>Class with tree structure in body" >}}

[Source](http://forum.plantuml.net/3448/how-to-display-nested-tree-like-structure-in-a-class-body?show=3455#a3455)


## Class Composition {#class-composition}

Diagram to represent a class containing children classes.

```plantuml
class h-entry << (H,white) >> {
  Post Entry
  ==
  ""<div class="h-entry">..</div>""
}

class p-name << (P,orchid) >> {
  Title of the post
  ==
  ""<h1 class="p-name">..</h1>""
}

class u-url << (U,#7EC0EE) >> {
  Permalink of the post
  ==
  ""<a href=".." class="u-url">..</a>""
}

class u-syndication << (U,#7EC0EE) >> {
  Link where the post is syndicated
  ==
  ""<a href=".." class="u-syndication" rel="syndication">..</a>""
}

class dt-published << (D,green) >> {
  Publish date of the post
  ..
  Go Date/time format: "2006-01-02T15:04:05-0700"
  ==
  ""<time datetime=".." class="dt-published">..</time>""
}

class p-summary << (P,orchid) >> {
  Summary/description of the post
  ==
  ""<div class="p-summary">..</div>""
}

class u-author << (U,#7EC0EE) >> {
  Post author
  ==
  ""<a href="HOMEPAGE" class="u-author">..</a>""
}

class e-content << (E,red) >> {
  Post content
  ==
  ""<div class="e-content">..</div>""
}

class p-category << (P,orchid) >> {
  Tag, category, series name
  ==
  ""<a href="TAGPAGE" class="p-category">TAG</a>""
}

"p-name" -up-* "h-entry"
"u-url" -up-* "h-entry"
"u-syndication" -left-* "h-entry"
"h-entry" *-right- "p-summary"
"h-entry" *-right- "u-author"
"h-entry" *-down- "dt-published"
"h-entry" *-down- "e-content"
"h-entry" *-down- "p-category"
```

{{< figure src="class-composition.svg" caption="<span class=\"figure-number\">Figure 2: </span>The `h-entry` object is shown to **contain** all other objects shown in the diagram" >}}


### Reference {#reference}

-   <http://plantuml.com/class-diagram>


## Nested boxes {#nested-boxes}

> I'd like to document the nested HTML tags for my web page, and
> represent them as a diagram with nested blocks.
>
> While searching for an existing representation like that online, I
> came across [this](https://mdn.mozillademos.org/files/14075/bubbling-capturing.png).
>
> Is there a way to draw something like the nested blocks in the center
> of that figure using PlantUML?

```plantuml
rectangle "<html>, <body>, etc." as a  {
  rectangle "<div>..." as b #antiquewhite {
    rectangle "<video>...\n\n\n" as c
  }
}
```

{{< figure src="nested-boxes.svg" caption="<span class=\"figure-number\">Figure 3: </span>Depicting nesting of objects in PlantUML" >}}

[Source](http://forum.plantuml.net/7530/how-to-create-a-diagram-with-unconnected-nested-blocks?show=7531#a7531)


## Forcing vertical ordering of rectangles {#forcing-vertical-ordering-of-rectangles}

Trying to nest _rectangles_ within _rectangles_ will give this:

```plantuml
rectangle a {
  rectangle b
  rectangle c
  rectangle d
}
```

{{< figure src="vertically-ordered-rectangles-not-working.svg" caption="<span class=\"figure-number\">Figure 4: </span>Nested rectangles showing up in arbitrary horizontal/vertical arrangement" >}}

To force the vertical order and alignment of _rectangles_, use
**hidden** arrows (`b -[hidden]-> c`).

<div class="note">

Note that this is a hack, and must not be abused.

</div>

```plantuml
rectangle a {
  rectangle b
  rectangle c
  rectangle d
  b -[hidden]-> c
  c -[hidden]-> d
}
```

{{< figure src="vertically-ordered-rectangles.svg" caption="<span class=\"figure-number\">Figure 5: </span>Forcing the nested rectangles to show up in vertical order using `-[hidden]->` hack" >}}

[Source](http://forum.plantuml.net/7537/specify-nested-rectangles-to-be-created-in-vertical-order?show=7545#a7545)


## Simple Flowchart {#simple-flowchart}


### Activity Diagram {#activity-diagram}

[Activity Diagram](http://plantuml.com/activity-diagram)

```plantuml
(*) -> "First Activity"
-> "Second Activity"
-> "Third Activity"
```

{{< figure src="flowchart-activity-diagram.svg" caption="<span class=\"figure-number\">Figure 6: </span>Simple Flowchart 1" >}}

I'd like to have the above diagram without that "start" dot. But if I
remove it, it generates a _sequence_ diagram:

```plantuml
"First Activity" -> "Second Activity"
-> "Third Activity"
```

{{< figure src="sequence-diagram.svg" caption="<span class=\"figure-number\">Figure 7: </span>Sequence Diagram" >}}


### Activity Diagram -- Beta {#activity-diagram-beta}

[Activity Diagram -- Beta](http://plantuml.com/activity-diagram-beta)

The activity diagram **beta** syntax does almost what I want --- a
horizontal flow chart --- just that it is vertical. The good thing is
that the "start" dot can be omitted using this syntax.

```plantuml
:First Activity;
:Second Activity;
:Third Activity;
```

{{< figure src="flowchart-activity-diagram-beta.svg" caption="<span class=\"figure-number\">Figure 8: </span>Simple Flowchart 2" >}}


## Hyperlinked SVGs {#hyperlinked-svgs}

-   [SVGs with hyperlinks](http://plantuml.com/svg)
-   [Hyperlinks in PlantUML](http://plantuml.com/link)

<!--listend-->

```plantuml
skinparam svgLinkTarget _parent
start
:[[http://plantuml.com PlantUML Homepage]];
stop
```

<figure>
<svg
xmlns="http://www.w3.org/2000/svg"
xmlns:xlink="http://www.w3.org/1999/xlink" contentStyleType="text/css"
height="136px" preserveAspectRatio="none"
style="width:171px;height:136px;background:#FFFFFF;" version="1.1"
viewBox="0 0 171 136" width="171px"
zoomAndPan="magnify"><defs/><g><ellipse cx="85.5" cy="20"
fill="#222222" rx="10" ry="10"
style="stroke:#222222;stroke-width:1.0;"/><rect fill="#F1F1F1"
height="33.9688" rx="12.5" ry="12.5"
style="stroke:#181818;stroke-width:0.5;" width="149" x="11" y="50"/><a
href="http://plantuml.com" target="_parent"
title="http://plantuml.com" xlink:actuate="onRequest"
xlink:href="http://plantuml.com" xlink:show="new"
xlink:title="http://plantuml.com" xlink:type="simple"><text
fill="#0000FF" font-family="sans-serif" font-size="12"
lengthAdjust="spacing" text-decoration="underline" textLength="129"
x="21" y="71.1387">PlantUML Homepage</text></a><ellipse cx="85.5"
cy="114.9688" rx="11" ry="11"
style="stroke:#222222;stroke-width:1.0;fill:none;"/><ellipse cx="85.5"
cy="114.9688" fill="#222222" rx="6" ry="6"
style="stroke:#111111;stroke-width:1.0;"/><line
style="stroke:#181818;stroke-width:1.0;" x1="85.5" x2="85.5" y1="30"
y2="50"/><polygon fill="#181818"
points="81.5,40,85.5,50,89.5,40,85.5,44"
style="stroke:#181818;stroke-width:1.0;"/><line
style="stroke:#181818;stroke-width:1.0;" x1="85.5" x2="85.5"
y1="83.9688" y2="103.9688"/><polygon fill="#181818"
points="81.5,93.9688,85.5,103.9688,89.5,93.9688,85.5,97.9688"
style="stroke:#181818;stroke-width:1.0;"/></g></svg>
<figcaption>

  <span class="figure-number">Figure 9: </span>An inlined SVG with hyperlink
</figcaption>
</figure>

<div class="note">

For the hyperlinks in SVGs to work, the SVGs need to be inlined, and
not embedded in the `<img>` HTML tags --- [Reference](https://alligator.io/svg/hyperlinks-svg/).

</div>


## Salt {#salt}

_Salt_ is a subproject included in _PlantUML_ that can be useful to
create graphical interface mockups.

The _Salt_ syntax is wrapped inside `@startsalt` and `@endsalt`
keywords.

```plantuml
@startsalt
{
  <Salt syntax here>
}
@endsalt
```

Reference
: <https://plantuml.com/salt>


### Tab Bar {#tab-bar}

A tab bar is added using the `{/ .. }` notation and each tab is
separate using the `|` character.

The [Creole](https://plantuml.com/creole) markup for formatting works inside _Slat_ syntax too.

```plantuml
@startsalt
{
  {/ <b>General | <back:orange>Fullscreen | Behavior | Saving }
}
@endsalt
```

{{< figure src="tabbar.svg" caption="<span class=\"figure-number\">Figure 10: </span>A tab bar example" >}}

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
