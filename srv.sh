#!/usr/bin/env bash
# Usage:
#   ./srv.sh      # Serves on port 3333 by default
#   ./srv.sh 1337 # Serves on port 1337

set -euo pipefail # http://redsymbol.net/articles/unofficial-bash-strict-mode
IFS=$'\n\t'

port="${1:-3333}" # default

mod_base_dir=".."
mods_to_be_replaced=("gitlab.com/kaushalmodi/hugo-theme-refined"
                     "github.com/kaushalmodi/hugo-search-fuse-js"
                     "github.com/kaushalmodi/hugo-atom-feed"
                     "github.com/kaushalmodi/hugo-jf2"
                    )

sed -i '/replace .* =>/d' go.mod
for mod in "${mods_to_be_replaced[@]}"
do
    echo "replace ${mod} => ${mod_base_dir}/$(basename "${mod}")" >> go.mod
done

echo "Hugo Module Graph:"
hugo mod graph

hugo server \
     --buildDrafts \
     --buildFuture \
     --navigateToChanged \
     --port "${port}"
